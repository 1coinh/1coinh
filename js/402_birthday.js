
function OnStart()
{
  var birthday = window.localStorage.getItem("myBirthday");
  if (birthday == null) {birthday = "";}
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  if (birthday.length == 10)
  {
    s1 = birthday.substring(0, 2);
    s2 = birthday.substring(3, 5);
    s3 = birthday.substring(6, 10);
  }
  document.getElementById("day").value = s1;
  document.getElementById("month").value = s2;
  document.getElementById("year").value = s3;
  document.getElementById("day").focus();
  birthdayTyped();  
}


function birthdayTyped()
{
  var ss = "";
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  var t1 = 0;
  var t2 = 0;
  var t3 = 0;
  var d = 0;
  var m = 0;
  var y = 0;
  var err = false;
  s1 = document.getElementById("day").value;
  s2 = document.getElementById("month").value;
  s3 = document.getElementById("year").value;
  d = parseInt(s1.match(/\d+/));
  d = d || 0;
  m = parseInt(s2.match(/\d+/));
  m = m || 0;
  y = parseInt(s3.match(/\d+/));
  y = y || 0;
  if ((d > 0) && (m > 0) && (y > 0))
  {
    err = ((d > 31) ||
                 ((d > 30) && ((m ==2) ||  (m ==4) || (m ==6) || (m ==9) || (m ==11))) ||
                 ((d > 29) && (m == 2)) ||
                 ((d > 28) && (m == 2) && ((y/4) != (Math.trunc(y/4)))) ||
                 (m > 12) ||
                 (y < 1905));
    // chech if birthday is in the future
    var today = new Date();
    var dd = String(today.getDate());
    var mm = String(today.getMonth() + 1); //January is 0!
    var yy = today.getFullYear();
    t1=Number(dd);
    t2=Number(mm);
    t3=Number(yy);
    if ((y > t3) || ((y == t3) && ((m > t2) || ((m == t2) && (d > t3))))) //MyBirth is in the future
    {
      err = true;
    }
  }
  else
  {
    err = true;
  }
  if (d == 0)
  {
    document.getElementById("day").value = "";
  }
  else
  {
    document.getElementById("day").value = String(d);
  }
  if (m == 0)
  {
    document.getElementById("month").value = "";
  }
  else
  {
    document.getElementById("month").value = String(m);
  }
  if (y == 0)
  {
    document.getElementById("year").value = "";
  }
  else
  {
    document.getElementById("year").value = String(y);
  }
  document.getElementById("birthdayContinue").classList.remove(document.getElementById("birthdayContinue").classList.item(0));
  if (err)
  {
    document.getElementById("mySpan1").style.color = "red";
    document.getElementById("mySpan1").innerHTML = "Date is invalid.";
    document.getElementById("birthdayContinue").disabled = true;
    document.getElementById("birthdayContinue").classList.add("disabled_button");
    // }
  }
  else
  {
    document.getElementById("mySpan1").style.color = "#00bf8e";
    document.getElementById("mySpan1").innerHTML = "Date is valid.";
    document.getElementById("birthdayContinue").disabled = false;
    document.getElementById("birthdayContinue").classList.add("main_button");
  }
}


function birthdayEnter()
{
  var ss = "";
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  s1 = document.getElementById("day").value;
  s2 = document.getElementById("month").value;
  s3 = document.getElementById("year").value;
  if (s1.length == 1) {s1 = "0" + s1;}
  if (s2.length == 1) {s2 = "0" + s2;}
  ss = s1 + "-" + s2 + "-" + s3;
  window.localStorage.setItem("myBirthday", ss);
  window.location.href = "400_idcard.html";
}