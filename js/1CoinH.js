/**
 * Function to run when loading a HTML document. The function should be called with:
 *   `<body onload="OnStart();">`
 */
function OnStart() { // eslint-disable-line no-unused-vars
  if (!lsTest()) {
    alert("ERROR!  This browser doesn't support 'LocalStorage' and is therefore not capable to run the 1CoinH app. Try installing another Internet-Browser on your system.");
  }
}


/**
 * Try to add an item to localStorage and report the availability of the
 * localStorage function
 * 
 * @returns {boolean} whether the browser is capable of localStorage or not
 */
function lsTest() {
  var test = "test";
  try {
    localStorage.setItem(test, test);
    localStorage.removeItem(test);
    return true;
  } catch (e) {
    return false;
  }
}
