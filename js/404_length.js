
const format = (num, decimals) => num.toLocaleString("en-US", {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});



function OnStart()
{
  var long = window.localStorage.getItem("myLength");
  if (long == null) {long = "";}
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  var t1 = 0;
  var t2 = 0;
  var t3 = 0;
  if (long.length > 0)
  {
    t1 = long.indexOf("m");
    t2 = long.indexOf("ft");
    t3 = long.indexOf("in");
    //180cm
    if (t1>-1)
    {
      s1=long.substring(0,t1);
      t1 = Math.round(100*Number(s1));
    }
    //4ft12in
    //0123456
    if (t2>-1)
    {
      s2=long.substring(0,t2);
    }
    if (t3>-1)
    {
      s3=long.substring(t2+2,t3);
    }
    if (t1 > -1)
    {
      document.getElementById("Metric").checked = true;
      document.getElementById("length1").value = s1;
    }
    else
    {
      document.getElementById("Imperial").checked = true;
      document.getElementById("length2").value = s2;
      document.getElementById("length3").value = s3;
    }
    lengthSelect();
  }
}


function lengthSelect()
{
  if (document.getElementById("Metric").checked)
  {
    document.getElementById("length1").disabled = false;
    document.getElementById("length2").disabled = true;
    document.getElementById("length3").disabled = true;
    document.getElementById("length1").classList.remove(document.getElementById("length1").classList.item(0));
    document.getElementById("length2").classList.remove(document.getElementById("length2").classList.item(0));
    document.getElementById("length3").classList.remove(document.getElementById("length3").classList.item(0));
    document.getElementById("length1").classList.add("TextInputContainer");
    document.getElementById("length2").classList.add("TextInputDisabledContainer");
    document.getElementById("length3").classList.add("TextInputDisabledContainer");
  }
  else
  {
    document.getElementById("length1").disabled = true;
    document.getElementById("length2").disabled = false;
    document.getElementById("length3").disabled = false;
    document.getElementById("length1").classList.remove(document.getElementById("length1").classList.item(0));
    document.getElementById("length2").classList.remove(document.getElementById("length2").classList.item(0));
    document.getElementById("length3").classList.remove(document.getElementById("length3").classList.item(0));
    document.getElementById("length1").classList.add("TextInputDisabledContainer");
    document.getElementById("length2").classList.add("TextInputContainer");
    document.getElementById("length3").classList.add("TextInputContainer");
  }
  lengthTyped();
  document.getElementById("length1").focus();
}


function lengthTyped()
{
  var err = false;
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  var t1 = 0;
  var t2 = 0;
  var t3 = 0;
  if (document.getElementById("Metric").checked)
  {
    s1 = document.getElementById("length1").value;
    s1 = s1.replace(/\D/g, "");
    t1 = Number(s1);
    if (t1>270) {t1 = 270;}
    s1 = String(t1);
    if (s1 == "0") {s1 = "";}
    document.getElementById("length1").value = s1;
    err = (t1<45);
  }
  else
  {
    s2 = document.getElementById("length2").value;
    s2 = s2.replace(/\D/g, "");
    t2 = Number(s2);
    if (t2>8) {t2 = 8;}
    s2 = String(t2);
    if (s2 == "0") {s2 = "";}
    document.getElementById("length2").value = s2;
    s3 = document.getElementById("length3").value;
    s3 = s3.replace(/\D/g, "");
    t3 = Number(s3);
    if (t3>11) {t3 = 11;}
    s3 = String(t3);
    if (s3 == "0") {s3 = "";}
    document.getElementById("length3").value = s3;
    err = ((t2<1) || ((t2 == 1) && (t3<6)));
  }
  document.getElementById("lengthContinue").classList.remove(document.getElementById("lengthContinue").classList.item(0));
  if (err)
  {
    document.getElementById("mySpan1").style.color = "red";
    document.getElementById("mySpan1").innerHTML = "Length is invalid.";
    document.getElementById("lengthContinue").disabled = true;
    document.getElementById("lengthContinue").classList.add("disabled_button");
  }
  else
  {
    document.getElementById("mySpan1").style.color = "#00bf8e";
    document.getElementById("mySpan1").innerHTML = "Length is valid.";
    document.getElementById("lengthContinue").disabled = false;
    document.getElementById("lengthContinue").classList.add("main_button");
  }
}


function lengthEnter()
{
  var ss = "";
  var s1 =" ";
  var s2 = "";
  var s3 = "";
  if (document.getElementById("Metric").checked)
  {
    s1 = document.getElementById("length1").value;
    ss = format((Number(s1)/100))+"m";
  }
  else
  {
    s1 = document.getElementById("length2").value;
    s2 = document.getElementById("length3").value;
    ss = s1+"ft";
    if (s2 != "0")
    {
      ss = ss + " "+s2+"in";
    }
  }
  window.localStorage.setItem("myLength", ss);
  window.location.href = "400_idcard.html";
}