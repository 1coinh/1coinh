
function OnStart()
{
  var features = window.localStorage.getItem("myFeatures");
  if (features == null) {features = "";}
  document.getElementById("features").value = features;
  document.getElementById("features").focus();
  featuresTyped();
}


function featuresTyped()
{
  var ss = "";
  var i = 0;
  var tt = 0;
  var t1 = 0;
  var t2 = 0;
  var t3 = 0;
  var s1 = "";
  var err = false; //between 4 and 32 char
  var features = document.getElementById("features").value;
  //alert(features)
  if (features.length>0)
  {
    /////////////////////////////////////////
    /// remove all spaces from the start  ///
    /////////////////////////////////////////
    while ((features.length>0) && (features.charCodeAt(0)==32))
    {
      if (features.length>1)
      {
        features = features.substring(1, features.length);
      }
      else
      {
        features = "";
      }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///  now we check the length (trimmed from spaces) on the back (spaces on the front are already removed)  ///
    ///  because we don't want to destroy the last space (the person is still typing) we use the ss var       ///
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ss = features.trim();
    tt = ss.length;
    if ((tt<4) || (tt>32))
    {
      err = true;
    }
    /////////////////////////////////////////////
    /// now we check on "illegal" characters  ///
    /////////////////////////////////////////////
    tt = features.length;
    if (tt>0)
    {
      if (tt>1)
      {
        ///////////////////////////////////////////////////////////////////
        ///  all procedures here have a features.length > 1             ///
        ///////////////////////////////////////////////////////////////////
        /// not allowed are:                                            ///
        /// 0-31 and 127                                                ///
        ///////////////////////////////////////////////////////////////////
        /// first we replace all not allowed char with "_"              ///
        ///////////////////////////////////////////////////////////////////
        for(i = 0; i < tt; i++) 
        {
          code = features.charCodeAt(i);
          if ((code<32) || (code == 127))
          {
            ss="";
            if (i>0)
            {
              ss = features.substring(0, i);
            }
            ss = ss + "_";
            if (i<(tt-1))
            {
              ss = ss + features.substr(i+1);
            }
            features = ss;
          }
        }
        ///////////////////////////////////////////////////////
        ///  now we check if 2 following chars are allowed  ///
        ///  double spaces, double --, __, ++ == || and     ///
        ///  are deleted                                    ///
        ///////////////////////////////////////////////////////
        if ((tt>3) || (tt<33))
        {
          features = features.replace("<", "[");
          features = features.replace(">", "]");
          features = features.replace("  ", " ");
          features = features.replace("~~", "~");
          features = features.replace("``", "`");
          features = features.replace("!!", "!");
          features = features.replace("@@", "@");
          features = features.replace("##", "#");
          features = features.replace("$$", "$");
          features = features.replace("%%", "%");
          features = features.replace("^^", "^");
          features = features.replace("&&", "&");
          features = features.replace("**", "*");
          features = features.replace("((", "(");
          features = features.replace("))", ")");
          features = features.replace("__", "_");
          features = features.replace("  ", " ");
          features = features.replace("--", "-");
          features = features.replace("++", "+");
          features = features.replace("==", "=");
          features = features.replace("{{", "{");
          features = features.replace("}}", "}");
          features = features.replace("[[", "[");
          features = features.replace("]]", "]");
          features = features.replace("||", "|");
          features = features.replace("\\", "|");
          features = features.replace("::", ":");
          features = features.replace(";;", ";");
          features = features.replace("''", "'");
          features = features.replace("<<", "[");
          features = features.replace(">>", "]");
          features = features.replace(",,", ",");
          features = features.replace("..", ".");
          features = features.replace("??", "?");
          features = features.replace("//", "/");
        }
        ss = features.trim();
        tt = ss.length;
        if (tt<4)
        {
          err = true;
        }
      }
      else
      {  //features.length = 1
        err = true;  //not between 4 and 32 char
      }
    }
    else
    {  //features.length = 0
      err = true; //not between 4 and 32 char
    }
  }
  else
  { // features.length = 0
    err = true; //not between 4 and 32 char
  }
  tt = features.length;
  document.getElementById("mySpan1").innerHTML = "between 4 and 32 characters: ["+String(tt)+"]";
  document.getElementById("features").value = features;
  document.getElementById("mySpan1").style.color = "green";
  if (err)
  {
    document.getElementById("mySpan1").style.color = "red";
  }
  document.getElementById("featuresContinue").classList.remove(document.getElementById("featuresContinue").classList.item(0));
  if (err)
  {
    document.getElementById("featuresContinue").disabled = true;
    document.getElementById("featuresContinue").classList.add("disabled_button");
  }
  else
  {
    document.getElementById("featuresContinue").disabled = false;
    document.getElementById("featuresContinue").classList.add("main_button");
  }
}


function featuresEnter()
{
  var ss="";
  var features = document.getElementById("features").value;
  ss = features.toLowerCase();
  if (ss == "none") {features = "";}
  window.localStorage.setItem("myFeatures", features);
  window.location.href = "400_idcard.html";
}