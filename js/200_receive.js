/**
 * requires format and SHA256 from ./hash.js,
 * this file should be included in the calling html file
 */

/* global SHA256 */

var fileContent = "";
var blockchain = "";
var IDCardBase64 = "";
var payer = "";
var myBlockchainTXT = "";


function handleFile(e) {
  var fileSize = 0;
  var theFile = document.getElementById("loadtext");
  if (theFile.value.indexOf("payment_id.txt") > -1) {
    if (typeof (FileReader) != "undefined") {
      //get table element
      var table = document.getElementById("myTable");
      var headerLine = "";
      //create html5 file reader object
      var myReader = new FileReader();
      // call filereader. onload function
      myReader.onload = function (e) {
        fileContent = myReader.result;
        processPayInfo();
      };
      //call file reader onload
      myReader.readAsText(theFile.files[0]);
    }
    else {
      alert("This browser does not support HTML5.");
    }
  }
  else {
    alert("Please upload a valid payment_id.txt file.");
  }
  return false;
}


function loadIt() {
  document.getElementById("loadtext").click();
}


function OnStart() {
  document.getElementById("loadtext").addEventListener("change", handleFile, false);
  //////////////////////////////
  ///  read your blockchain  ///
  //////////////////////////////
  var myFiles = "";  //to get the filename of your blockchain
  var read = ""; //to read your blockchain
  var s1 = window.localStorage.getItem("myFiles");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    read = "";
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
  }
  else {
    var myBlockchain = s1 + ".txt";
    var s1 = window.localStorage.getItem(myBlockchain);
    if (s1 == null) { s1 = ""; }
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
      myBlockchainTXT = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
    }
    else {
      myBlockchainTXT = s1;
      ////////////////////////////////////////////////////////////////////////////
      ///  if payment info is already loaded but not finalised and deleted we  ///
      ///  skip this part of the process so we dont interfere with the non-    ///
      ///  processed data of the previous payment                              ///
      ////////////////////////////////////////////////////////////////////////////
      var s1 = localStorage.getItem("payer");
      if (s1 == null) { s1 = ""; }
      if ((s1 != s1) || (s1 == "")) //catch a NaN issue
      {
        //do nothing
      }
      else {
        window.open("201_receive1.html", "_self");
        //alert("jump 1")
      }
    }
  }
}


function processPayInfo() {
  var s1 = "";
  var s2 = "";
  var ss = "";
  //alert(fileContent.substring(0,100))
  document.getElementById("uploadPayInfo").style.display = "none";
  document.getElementById("uploadText").style.display = "none";
  document.getElementById("IDCard").style.display = "block";
  document.getElementById("hashCheck").style.display = "block";
  /////////////////////////////////////////////////////////
  ///  load the IDCard Base 64 data and the Blockchain  ///
  ///  so they can be checked                           ///
  /////////////////////////////////////////////////////////
  var t1 = fileContent.indexOf(">");
  IDCardBase64 = fileContent.substring(0, t1);
  blockchain = fileContent.substring(t1 + 1);
  s1 = blockchain.substring(0, 64);
  s2 = myBlockchainTXT.substring(0, 64);
  if (s1 == s2) {
    alert("You can't read your own payment_id.txt file. This file can only be used by others!");
    window.open("index.html", "_self");
  }
  else {
    ////////////////////////////////////////////////////////////////////////////////
    ///  first count the number of previous IDCards, when a person has multiple  ///
    ///  IDCards they are all included in the payinfo file, at the end,          ///
    ///  starting with the "%" character                                         ///
    ////////////////////////////////////////////////////////////////////////////////
    var numcards = (fileContent.match(/%/g) || []).length + 1;
    var sha = SHA256(IDCardBase64);
    var sha2 = blockchain.substring(0, 64);
    ///////////////////////////////
    ///  check hash blockchain  ///
    ///////////////////////////////
    document.getElementById("acceptPay").classList.remove(document.getElementById("acceptPay").classList.item(0));
    if (sha != sha2)  //hash is not okay
    {
      document.getElementById("acceptPay").disabled = true;
      document.getElementById("acceptPay").classList.add("disabled_button");
      document.getElementById("hashCheck").innerHTML = "Hash IDCard is wrong!<br><br><br>";
      document.getElementById("hashCheck").style.color = "#ff0000";
      document.getElementById("checkTitle").style.display = "none";
      document.getElementById("checkName").style.display = "none";
      document.getElementById("IDCard").src = "img/PhotoClear.png";
      blockchain = "";
    }
    else {
      document.getElementById("acceptPay").disabled = false;
      document.getElementById("acceptPay").classList.add("main_button");
      document.getElementById("hashCheck").innerHTML = "Hash IDCard is okay!";
      document.getElementById("hashCheck").style.color = "#00bf8e";
      document.getElementById("checkTitle").style.display = "block";
      document.getElementById("checkName").style.display = "block";
      document.getElementById("IDCard").src = IDCardBase64;
      ///////////////////////////////////////////////////////////////////////////
      ///  Get the name and cardnumber and ask user to check them             ///
      ///  86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219             ///
      ///  a35b0c9982|30 Jun 2022|15:23:13|Gloria Moses|04 Sep 1998|-204479<  ///
      ///////////////////////////////////////////////////////////////////////////
      s1 = String(numcards);
      if (numcards < 10) { s1 = "0" + s1; }
      s2 = blockchain;
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //30 Jun 2022|15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(0, t1); //Gloria Moses
      document.getElementById("checkName").innerHTML = s2 + " | IDCard" + s1;
      ss = s2.replace(/\s/g, "_"); //replace spaces in my Name
      payer = ss + "-" + s1 + "," + blockchain.substring(0, 64);
    }
  }
}



function acceptPayment() {
  /////////////////////////////////////////////////////////////////////////////////////////////
  ///  Now we store the blockchain and IDCard (all of them TODO) in the localstorage        ///
  ///  the blockchain gets .txt, the IDCard .png and "payer" stores the part before the "." ///  
  ///  The format is this: Teun_van_Sambeek-01,4ca2a4859b705a78ce07.png                     ///
  ///         name with _ + "-" + cardnr + "," + first 20char of hash of the IDCard + .png  ///
  /////////////////////////////////////////////////////////////////////////////////////////////
  var s1 = blockchain.substring(0, 64) + ".txt";
  var s2 = blockchain.substring(0, 64) + ".png";
  localStorage.setItem("payer", payer);
  localStorage.setItem(s1, blockchain);
  localStorage.setItem(s2, IDCardBase64);
  /////////////////////////////////////////
  window.open("201_receive1.html", "_self");
}
