/**
 * requires SHA256 from ./hash.js,
 * this file should be included in the calling html file
 */

/* global SHA256 */

var securityHash = "";
var payment = "";

function getDate() {
  ///////////////////////////////////////////////////////
  // based on the vars hcd,hcm,hcy and hch             //
  // the hour count in hc is calculated                //
  ///////////////////////////////////////////////////////

  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((hcy - 2021) / 4);    //in the year 2025 we need to start with one jump day
  if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2)) { t1++; } //if feb has pars in a jump year we also need to add one jump day
  t1 = hcd + t1 + (365 * (hcy - 2022)); //then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
  if (hcm > 1) { t1 = t1 + 31; } //then we add the days of all past months in this year vvv
  if (hcm > 2) { t1 = t1 + 28; }
  if (hcm > 3) { t1 = t1 + 31; }
  if (hcm > 4) { t1 = t1 + 30; }
  if (hcm > 5) { t1 = t1 + 31; }
  if (hcm > 6) { t1 = t1 + 30; }
  if (hcm > 7) { t1 = t1 + 31; }
  if (hcm > 8) { t1 = t1 + 31; }
  if (hcm > 9) { t1 = t1 + 30; }
  if (hcm > 10) { t1 = t1 + 31; }
  if (hcm > 11) { t1 = t1 + 30; }
  hc = (24 * (t1 - 1)) + hch; //because the day has not passed we reduce one day, multiply with 24 and add the current hour.
  //var ss = String(hc);
  //document.getElementById("ta1").innerHTML ="Hour Count: " + ss;
}


function handleFile(e) {
  var fileSize = 0;
  var theFile = document.getElementById("loadtext");
  if (typeof (FileReader) != "undefined") {
    //get table element
    var table = document.getElementById("myTable");
    var headerLine = "";
    //create html5 file reader object
    var myReader = new FileReader();
    // call filereader. onload function
    myReader.onload = function (e) {
      fileContent = myReader.result;
      processSecurityHash();
    };
    //call file reader onload
    myReader.readAsText(theFile.files[0]);
  }
  else {
    alert("This browser does not support HTML5.");
  }
  return false;
}


function loadIt() {
  document.getElementById("loadtext").click();
}


function delSecurityImage() {
  var s1 = "Do you want to remove the hash  of the security image: " + securityHash + " ?";
  var s2 = "The hash " + securityHash + " is removed from your local storage.";
  if (window.confirm(s1)) {
    window.localStorage.removeItem("securityHash");
    securityHash = "";
    s1 = "<small>After uploading the security image, the app will hash it and add the security hash to your payment proposal.</small>";
    document.getElementById("uploadtext").innerHTML = s1;
    s1 = "<small>No image selected<br>&nbsp;</small>";
    document.getElementById("hash").innerHTML = s1;
    document.getElementById("delSecurityImage").classList.remove(document.getElementById("delSecurityImage").classList.item(0));
    document.getElementById("delSecurityImage").disabled = true;
    document.getElementById("delSecurityImage").classList.add("disabled_button");
    alert(s2);
  }
}



function processSecurityHash() {
  var s1 = "";
  var s2 = "";
  var ss = "";
  s1 = "<small>This is the hash of the Security Image you just uploaded.<br>" +
    "It will be added to your payment proposal.</small>";
  document.getElementById("uploadtext").innerHTML = s1;
  ////////////////////////////////////////////////////////////////////////////////
  ///  first count the number of previous IDCards, when a person has multiple  ///
  ///  IDCards they are all included in the payinfo file, at the end,          ///
  ///  starting with the "%" character                                         ///
  ////////////////////////////////////////////////////////////////////////////////
  //console.log(fileContent);
  securityHash = SHA256(fileContent);
  window.localStorage.setItem("securityHash", securityHash);
  s1 = "<small>" + securityHash.substring(0, 32) + "<br>" + securityHash.substring(32, 64) + "</small>";
  document.getElementById("hash").innerHTML = s1;
  document.getElementById("delSecurityImage").classList.remove(document.getElementById("delSecurityImage").classList.item(0));
  document.getElementById("delSecurityImage").disabled = false;
  document.getElementById("delSecurityImage").classList.add("main_button");
}



function OnStart() {
  document.getElementById("loadtext").addEventListener("change", handleFile, false);
  payment = "";
  var s1 = window.localStorage.getItem("payment");
  if (s1 == null) { s1 = ""; }
  ////////////////////////////////////////////////////////////////////////////////
  ///  The payment info should always be present, so this should never happen  ///
  ////////////////////////////////////////////////////////////////////////////////
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    alert("no payment info found. Please redo the receive process.");
    window.localStorage.removeItem("securityHash");
    window.localStorage.removeItem("payer");
    window.localStorage.removeItem("payment");
    //alert(s2)
    window.open("200_receive.html", "_self");
  }
  else {
    payment = s1;
  }
  securityHash = "";
  var s1 = window.localStorage.getItem("securityHash");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    s1 = "<small>After uploading the security image, the app will hash it and add the security hash to your payment proposal.</small>";
    document.getElementById("uploadtext").innerHTML = s1;
    s1 = "<small>No image selected<br>&nbsp;</small>";
    document.getElementById("hash").innerHTML = s1;
    document.getElementById("delSecurityImage").classList.remove(document.getElementById("delSecurityImage").classList.item(0));
    document.getElementById("delSecurityImage").disabled = true;
    document.getElementById("delSecurityImage").classList.add("disabled_button");
    s1 = "";
  }
  securityHash = s1;
  var ss = payment + securityHash;
  var s1 = window.localStorage.getItem("downloadProposal");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    ////////////////////////////////////
    ///  Disable the Receive button  ///
    ////////////////////////////////////
    document.getElementById("receive").disabled = true;
    document.getElementById("receive").classList.add("disabled_button");
  }
  else {
    document.getElementById("receive").classList.remove(document.getElementById("receive").classList.item(0));
    if (s1.indexOf(ss) > -1) {
      ///////////////////////////////////
      ///  Enable the Receive button  ///
      ///////////////////////////////////
      document.getElementById("receive").disabled = false;
      document.getElementById("receive").classList.add("main_button");
    }
    else {
      ////////////////////////////////////
      ///  Disable the Receive button  ///
      ////////////////////////////////////
      document.getElementById("receive").disabled = true;
      document.getElementById("receive").classList.add("disabled_button");
    }
  }
  if (securityHash == "") {
    s1 = "<small><small>There is no Security Image found. " +
      "No Security hash will be added to your payment proposal.</small></small>";
    document.getElementById("uploadtext").innerHTML = s1;
    s1 = "<small><small>[none]<br>&nbsp;</small></small>";
    document.getElementById("hash").innerHTML = s1;
    document.getElementById("delSecurityImage").classList.remove(document.getElementById("delSecurityImage").classList.item(0));
    document.getElementById("delSecurityImage").disabled = true;
    document.getElementById("delSecurityImage").classList.add("disabled_button");
  }
  else {
    s1 = "<small><small>This is the hash of the Security Image you just uploaded. " +
      "It will be added to your payment proposal.</small></small>";
    document.getElementById("uploadtext").innerHTML = s1;
    s1 = "<small><small>" + securityHash.substring(0, 32) + "<br>" + securityHash.substring(32, 64) + "</small></small>";
    document.getElementById("hash").innerHTML = s1;
    document.getElementById("delSecurityImage").classList.remove(document.getElementById("delSecurityImage").classList.item(0));
    document.getElementById("delSecurityImage").disabled = false;
    document.getElementById("delSecurityImage").classList.add("main_button");
  }
}


function downloadProposal() {
  if (payment == "") {
    alert("No payment data found, please use 'Delete Payment Proposal' button!");
  }
  else {
    var myFiles = "";  //to get the filename of your blockchain
    var read = ""; //to read your blockchain (person that receives)
    var read2 = ""; //to read your IDCard
    var sx1 = window.localStorage.getItem("myFiles");
    s1 = sx1;
    if (s1 == null) { s1 = ""; }
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
    {
      read = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
      window.localStorage.removeItem("securityHash");
      window.localStorage.removeItem("payer");
      window.localStorage.removeItem("payment");
      window.localStorage.removeItem("downloadProposal");
      window.open("400_idcard.html", "_self");
    }
    else {
      var myBlockchain = s1 + ".txt";
      s1 = window.localStorage.getItem(myBlockchain);
      if (s1 == null) { s1 = ""; }
      if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
      {
        read = "";
        alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
        window.localStorage.removeItem("securityHash");
        window.localStorage.removeItem("payer");
        window.localStorage.removeItem("payment");
        window.localStorage.removeItem("downloadProposal");
        window.open("400_idcard.html", "_self");
      }
      else {
        read = s1.trim();  //blockchainstring
        var myIDCard = sx1 + ".png";
        s1 = window.localStorage.getItem(myIDCard);
        if (s1 == null) { s1 = ""; }
        if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
        {
          read2 = "";
          alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
          window.localStorage.removeItem("securityHash");
          window.localStorage.removeItem("payer");
          window.localStorage.removeItem("payment");
          window.localStorage.removeItem("downloadProposal");
          window.open("400_idcard.html", "_self");
        }
        else {
          read2 = s1.trim();  //IDCard string
          ///////////////////////
          ///  Set timestamp  ///
          ///////////////////////
          var today = new Date();
          hcd = Number(String(today.getDate()));
          hcm = Number(String(today.getMonth() + 1)); //January is 0!
          hcy = Number(today.getFullYear());
          hch = Number(String(today.getHours()));
          hcmin = Number(String(today.getMinutes()));
          hcsec = Number(String(today.getSeconds()));
          hcmsec = Number(String(today.getMilliseconds()));
          getDate(); //this will set hc which is the hours passed since 1-1-2022 0:00
          var ss1 = String(hcmin);
          if (hcmin < 10) { ss1 = "0" + ss1; }
          var ss2 = String(hcsec);
          if (hcsec < 10) { ss2 = "0" + ss2; }
          var ss3 = String(hcmsec);
          if (hcmsec < 10) { ss3 = "0" + ss3; }
          if (hcmsec < 100) { ss3 = "0" + ss3; }
          var timestamp = String(hc) + ":" + ss1 + ss2 + ss3;
          ///////////////////////////////
          ///  create downloadstring  ///
          ///////////////////////////////////////////////////////////////////////
          ///  if a person has multiple IDCards, they need to be added after  ///
          ///  the proposal.txt string,                                       ///
          ///  each one starting with a "%" character  TODO!!!                ///
          ///////////////////////////////////////////////////////////////////////
          var ss = payment + securityHash + ">" + timestamp + "<";
          window.localStorage.setItem("downloadProposal", ss);
          /////////////////////////////////////////////////////////////////
          ///  for the receiver of the proposal (the person that pays)  ///
          ///  we need to add my IDCard and Blockchain so the person    ///
          ///  that pays can update your most recent data + payment     ///
          /////////////////////////////////////////////////////////////////
          ss = ss + read + "~" + read2;
          //////////////////////////////////////////////////
          ///  Download the string as.proposal.txt file  ///
          //////////////////////////////////////////////////
          // OLD!!
          // var element = document.createElement('a');
          // element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(ss));
          // element.setAttribute('download', "proposal.txt");
          // element.style.display = 'none';
          // document.body.appendChild(element);
          // element.click();
          // document.body.removeChild(element);
          ///////////////////////////////////////////////////////////////////////
          // https://stackoverflow.com/questions/3665115/how-to-create-a-file-in-memory-for-user-to-download-but-not-through-server
          ///////////////////////////////////////////////////////////////////////
          const blob = new Blob([ss], { type: "text" });
          if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, "proposal.txt");
          }
          else {
            const elem = window.document.createElement("a");
            elem.href = window.URL.createObjectURL(blob);
            elem.download = "proposal.txt";
            document.body.appendChild(elem);
            elem.click();
            document.body.removeChild(elem);
          }
          ///////////////////////////////////
          ///  Enable the Receive button  ///
          ///////////////////////////////////
          document.getElementById("receive").classList.remove(document.getElementById("receive").classList.item(0));
          document.getElementById("receive").disabled = false;
          document.getElementById("receive").classList.add("main_button");
        }
      }
    }
  }
}


function deleteProposal() {
  var s1 = "Do you want to delete your payment proposal?";
  var s2 = "The payment process is cleared. If you want to continue with the person" +
    " that pays, please reload the payinfo.txt again.";
  if (window.confirm(s1)) {
    window.localStorage.removeItem("securityHash");
    window.localStorage.removeItem("payer");
    window.localStorage.removeItem("payment");
    window.localStorage.removeItem("downloadProposal");
    alert(s2);
    window.open("200_receive.html", "_self");
  }
}



function receive() {
  var s1 = "!! IMPORTANT !!\rReceiving coins without the explicit acceptance of your payment proposal " +
    "by the person that pays, is fraud and will very likely result the coins that you receive " +
    "- plus the coins you create yourself - becoming worthless or very difficult to use!" +
    " Besides that it might also have legal consequences depending on your local laws." +
    "\r\rDo you want to continue?";
  if (window.confirm(s1)) {
    var myFiles = "";  //to get the filename of your blockchain
    var read = ""; //to read your blockchain (person that receives)
    var read2 = ""; //to read blockchain of person that pays
    s1 = window.localStorage.getItem("myFiles");
    if (s1 == null) { s1 = ""; }
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
    {
      read = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
      window.localStorage.removeItem("securityHash");
      window.localStorage.removeItem("payer");
      window.localStorage.removeItem("payment");
      window.localStorage.removeItem("downloadProposal");
      window.open("400_idcard.html", "_self");
    }
    else {
      var myBlockchain = s1 + ".txt";
      s1 = window.localStorage.getItem(myBlockchain);
      if (s1 == null) { s1 = ""; }
      if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
      {
        read = "";
        alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
        window.localStorage.removeItem("securityHash");
        window.localStorage.removeItem("payer");
        window.localStorage.removeItem("payment");
        window.localStorage.removeItem("downloadProposal");
        window.open("400_idcard.html", "_self");
      }
      else {
        read = s1.trim();
        var s1 = window.localStorage.getItem("payer");  //Gloria_Moses-01,86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982
        if (s1 == null) { s1 = ""; }
        if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
        {
          read2 = "";
          alert("You didn't set the payment properly. You will be asked to set it again.");
          window.localStorage.removeItem("securityHash");
          window.localStorage.removeItem("payer");
          window.localStorage.removeItem("payment");
          window.localStorage.removeItem("downloadProposal");
          window.open("200_receive.html", "_self");
        }
        else {
          var t2 = s1.indexOf(",");
          var senderIDCode = s1.substring(t2 + 1); //86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982
          ss = senderIDCode + ".txt"; //86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982.txt
          var s1 = window.localStorage.getItem(ss);  //Gloria_Moses-01,86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982
          if (s1 == null) { s1 = ""; }
          if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
          {
            read2 = "";
            alert("Can't find the blockchain of sender. Please enter all payment data again.");
            window.localStorage.removeItem("securityHash");
            window.localStorage.removeItem("payer");
            window.localStorage.removeItem("payment");
            window.localStorage.removeItem("downloadProposal");
            window.open("200_receive.html", "_self");
          }
          else {
            read2 = s1.trim();  //blockchain of sender
            var downloadProposal = "";
            s1 = window.localStorage.getItem("downloadProposal");
            if (s1 == null) { s1 = ""; }
            if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
            {
              downloadProposal = ""; //this shouldnt happen
              alert("You didn't download your proposal properly. Please do that first.");
            }
            else {
              //////////////////////////
              ///  downloadProposal  ///
              /////////////////////////////////////////////////////////////////////////////
              ///  first get the last 78 char of the proposal (securityhash+timestamp)  ///
              /////////////////////////////////////////////////////////////////////////////
              downloadProposal = s1.trim();
              var xs = downloadProposal.substring((downloadProposal.length - 15), (downloadProposal.length - 13));
              if (xs == "|>") {
                //securityHash = empty!
                var ss = downloadProposal.substring((downloadProposal.length - 14));
                securityHash = "";
                var timestamp = ss.substring(1, 13);
                var sss = downloadProposal.substring(0, downloadProposal.length - 15);
              }
              else {
                var ss = downloadProposal.substring((downloadProposal.length - 78));
                securityHash = ss.substring(0, 64);
                var timestamp = ss.substring(65, 77);
                var sss = downloadProposal.substring(0, downloadProposal.length - 79);
              }
              console.log(downloadProposal.substring(0, 400));
              /////////////////////////////////////
              ///  read the proposal in detail  /// 0|123|Gloria Moses|86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982|
              /////////////////////////////////////
              t0 = sss.indexOf("|");
              var others = Number(sss.substring(0, t0));     //number of others
              /////////////////////////////////////
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
              var xr0 = Number(ssr); //amount payment person0
              /////////////////////////////////////
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              var name0 = sss.substring(0, t0); //name0 of the person that pays
              /////////////////////////////////////
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              var hash0 = sss.substring(0, t0); //hash0 of the person that pays
              /////////////////////////////////////
              if (others > 0) {
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
                var xr1 = Number(ssr); //amount payment person1
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var name1 = sss.substring(0, t0); //name1 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 < 0) {
                  hash1 = sss;
                }
                else {
                  var hash1 = sss.substring(0, t0); //hash1 of the person that pays
                }
                /////////////////////////////////////
              }
              /////////////////////////////////////
              if (others > 1) {
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
                var xr2 = Number(ssr); //amount payment person2
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var name2 = sss.substring(0, t0); //name2 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 < 0) {
                  hash2 = sss;
                }
                else {
                  var hash2 = sss.substring(0, t0); //hash1 of the person that pays
                }
                /////////////////////////////////////
              }
              /////////////////////////////////////
              if (others > 2) {
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
                var xr3 = Number(ssr); //amount payment person3
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var name3 = sss.substring(0, t0); //name3 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 < 0) {
                  hash3 = sss;
                }
                else {
                  var hash3 = sss.substring(0, t0); //hash1 of the person that pays
                }
                /////////////////////////////////////
              }
              /////////////////////////////////////
              if (others > 3) {
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
                var xr4 = Number(ssr); //amount payment person4
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var name4 = sss.substring(0, t0); //name4 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 < 0) {
                  hash4 = sss;
                }
                else {
                  var hash4 = sss.substring(0, t0); //hash1 of the person that pays
                }
                /////////////////////////////////////
              }
              /////////////////////////////////////
              if (others > 4) {
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var ssr = sss.substring(0, t0); //payment amount of personal coins of the person that pays
                var xr5 = Number(ssr); //amount payment person5
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                var name5 = sss.substring(0, t0); //name5 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 < 0) {
                  hash5 = sss;
                }
                else {
                  var hash5 = sss.substring(0, t0); //hash1 of the person that pays
                }
                /////////////////////////////////////
              }

              //////////////////////////////////////////
              /// change blockchain of receiver (me) ///
              //////////////////////////////////////////
              var ss = "R" + senderIDCode + "|" + timestamp + "|" + securityHash + "|" + String(xr0);
              if (others > 0) { ss = ss + "|" + hash1 + "|" + String(xr1); }
              if (others > 1) { ss = ss + "|" + hash2 + "|" + String(xr2); }
              if (others > 2) { ss = ss + "|" + hash3 + "|" + String(xr3); }
              if (others > 3) { ss = ss + "|" + hash4 + "|" + String(xr4); }
              if (others > 4) { ss = ss + "|" + hash5 + "|" + String(xr5); }
              ss = ss + "|";
              read = read + ss;
              var endhash = SHA256(read);
              read = read + endhash + "<";
              window.localStorage.setItem(myBlockchain, read);
              ///////////////////////////////////////////////////
              /// change my copy of the blockchain of sender  ///
              ///////////////////////////////////////////////////
              //var acceptfile = blockchains+"/accept/"+senderIDCard.substring(0,(senderIDCard.length-4)) + ".txt"    
              //var read2 = app.ReadFile(acceptfile) 
              var myCurrentHash = window.localStorage.getItem("myCurrentHash");
              var ss = "S" + myCurrentHash + "|" + timestamp + "|" + securityHash + "|" + String(xr0);
              if (others > 0) { ss = ss + "|" + hash1 + "|" + String(xr1); }
              if (others > 1) { ss = ss + "|" + hash2 + "|" + String(xr2); }
              if (others > 2) { ss = ss + "|" + hash3 + "|" + String(xr3); }
              if (others > 3) { ss = ss + "|" + hash4 + "|" + String(xr4); }
              if (others > 4) { ss = ss + "|" + hash5 + "|" + String(xr5); }
              ss = ss + "|";
              read2 = read2 + ss;
              var endhash = SHA256(read2);
              read2 = read2 + endhash + "<";
              ss = senderIDCode + ".txt";
              window.localStorage.setItem(ss, read2);
              //////////////////////////////
              ///  remove proposal data  ///
              //////////////////////////////
              window.localStorage.removeItem("securityHash");
              window.localStorage.removeItem("payer");
              window.localStorage.removeItem("payment");
              window.localStorage.removeItem("downloadProposal");
              s2 = "The payment process is finalized and cleared.";
              alert(s2);
              window.open("300_cashflow.html", "_self");
            }
          }
        }
      }
    }
  }
}
