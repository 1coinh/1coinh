/**
 * requires SHA256 from ./hash.js,
 * this file should be included in the calling html file
 */
/* global SHA256 */


/**
 * Function to run when loading a HTML document. The function should be called with:
 *   `<body onload="OnStart();">`
 *
 */
function OnStart() { // eslint-disable-line no-unused-vars
  // find the hash of the person between 3rd and 4th "|"
  var s1 = window.localStorage.getItem("proposal");  //s1 = 0|0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
  var previdnum = (s1.match(/%/g) || []).length + 1;
  var s2 = String(previdnum);
  if (previdnum < 10) { s2 = "0" + s2; }
  var t0 = s1.indexOf("|");
  s1 = s1.substring(t0 + 1);  //s1 = 0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
  t0 = s1.indexOf("|");
  s1 = s1.substring(t0 + 1);  //s1 = Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
  t0 = s1.indexOf("|");
  var s3 = s1.substring(0, t0);
  s1 = s1.substring(t0 + 1, t0 + 65);  //s1 = 2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4

  /** @todo here you could compare possible earlier versions of blockchains you have of the person  */
  var fname = s1 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = s1;

  // check hash blockchain
  document.getElementById("acceptPay").classList.remove(document.getElementById("acceptPay").classList.item(0));
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("acceptPay").disabled = true;
    document.getElementById("acceptPay").classList.add("disabled_button");
    document.getElementById("hashCheck").innerHTML = "Hash IDCard is wrong!<br><br><br>";
    document.getElementById("hashCheck").style.color = "#ff0000";
    document.getElementById("checkTitle").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  } else {
    document.getElementById("acceptPay").disabled = false;
    document.getElementById("acceptPay").classList.add("main_button");
    document.getElementById("hashCheck").innerHTML = "Hash IDCard is okay!";
    document.getElementById("hashCheck").style.color = "#00bf8e";
    document.getElementById("checkTitle").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = s3 + " | IDCard" + s2;
  }
}


/**
 * If the receiver is accepted,
 * open the file 101_pay1.html
 */
function acceptReceiver() { // eslint-disable-line no-unused-vars   
  window.open("101_pay1.html", "_self");
}


/**
 * If the receiver is refused,
 * remove the proposal object from 
 * localStorage and open the main page (index.html)
 */
function refuseReceiver() { // eslint-disable-line no-unused-vars
  window.localStorage.removeItem("proposal");
  window.open("index.html", "_self");
}


