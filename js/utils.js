/**
 * Check if a value is set and if so, enable the button {HTMLElement} with the given id
 *
 * @param {*} value 
 * @param {string} id
 */
function setButtons(value, id) { // eslint-disable-line no-unused-vars
  document.getElementById(id).classList.remove(document.getElementById(id).classList.item(0));
  if (!value) {
    document.getElementById(id).disabled = true;
    document.getElementById(id).classList.add("disabled_button");
  } else {
    document.getElementById(id).disabled = false;
    document.getElementById(id).classList.add("main_button");
  }
}


/**
 * Check if a value is set and if so, enable the button {HTMLElement} with the given id
 *
 * @param {string} item_id the item to retrieve from localStorage
 * @param {string} title the title to set when item cannot be retrieved
 * @param {string} id the id of the HTMLElement
 * @param {string} icon the icon to show
 */
function setIDCardButton(item_id, title, id, icon) { // eslint-disable-line no-unused-vars
  var item = window.localStorage.getItem(item_id);
  document.getElementById(id).classList.remove(document.getElementById(id).classList.item(0));
  if (!item) {
    document.getElementById(id).innerHTML = "<span class='" + icon + "'></span>[" + title + "]";
    document.getElementById(id).classList.add("main_button");
  }
  else {
    document.getElementById(id).innerHTML = "<span class='" + icon + "'></span>"
      + item.length === 0 ? "[None]" : item;
    document.getElementById(id).classList.add("done_button");
  }
}