
var trantt = 1;
var trantot = 1;
var showtrans = "";
var blockchain = "";
var currencyChar = "ᕫ";
var gname = "";

var hc = 0;   //hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time). The counter does not need to look at daylight saving time or time zones
var hcd = 0; //day
var hcm = 0; //month
var hcy = 0; //year
var hch = 0; //hour
var hcmin = 0; //minutes
var hcsec = 0; //seconds
var hcmsec = 0; //milliseconds
var bdd = 0;   //birthday day
var bmm = 0;    //birthday month
var byy = 0; //birthday year
var bhh = 0;    //birthday hour
var MyBhh = 0;  //my birthday hour
var timer = ""; //string to represent the progress in time in the DATASET


const format = (num, decimals) => num.toLocaleString("en-US", {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});


function setDate()
{
  ///////////////////////////////////////////////////////
  // based on hc, the vars hcd,hcm,hcy and hch are set //
  // the date in hc_date is also set                   //
  ///////////////////////////////////////////////////////
  var t1 = 0;
  var t5 = 0;
  hcd = Math.trunc(hc/24);                          //hcd = daynumber since 01-01-2022 where 01-01-2022 is day 0 here
  hch = hc - (24*hcd);                              //hch = number of hours on day t1
  t1 = Math.trunc(hcd / 1461);                      //t1 = number of clusters of 4 years (=1461 days) that are past
  hcd = 1 + (hcd - (1461 * t1));                    //hcd = remaining number of days in remaining cluster, add one day to make 01-01-2022 day 1
  t5 = 0;                                           //t5 = number of years that has past in the remaining cluster 
  if (hcd > 365) {t5++;hcd=hcd-365;}               //t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 365) && (t5 == 1)) {t5++;hcd=hcd-365;}//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 366)  && (t5 == 2)) {t5++;hcd=hcd-366;}//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  hcy = 2022 + t5 + (4*t1);
  //hcd is now the day number in year hcy
  hcm = 1;
  if (hcd > 31) {hcm=2;hcd=hcd-31;}
  if (t5 == 2)
  {
    if ((hcd > 29) && (hcm>1)) {hcm=3;hcd=hcd-29;}
  } else
  {    
    if ((hcd > 28) && (hcm>1)) {hcm=3;hcd=hcd-28;}
  }
  if ((hcd > 31) && (hcm>2)) {hcm=4;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>3)) {hcm=5;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>4)) {hcm=6;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>5)) {hcm=7;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>6)) {hcm=8;hcd=hcd-31;}
  if ((hcd > 31) && (hcm>7)) {hcm=9;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>8)) {hcm=10;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>9)) {hcm=11;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>10)) {hcm=12;hcd=hcd-30;}
  var mnd = "Jan";
  switch(hcm) 
  {
  case 1:  {var mnd = "Jan";} break;        
  case 2:  {var mnd = "Feb";} break;        
  case 3:  {var mnd = "Mar";} break;        
  case 4:  {var mnd = "Apr";} break;        
  case 5:  {var mnd = "May";} break;        
  case 6:  {var mnd = "Jun";} break;        
  case 7:  {var mnd = "Jul";} break;        
  case 8:  {var mnd = "Aug";} break;        
  case 9:  {var mnd = "Sep";} break;        
  case 10:  {var mnd = "Oct";} break;        
  case 11:  {var mnd = "Nov";} break;        
  case 12:  {var mnd = "Dec";} break;        
  }
  day = (hcd < 10 ? "0" : "") + hcd;
  hour = (hch < 10 ? "0" : "") + hch;
  timer = day + mnd + hcy+"_"+hour+"u";
}



function getDate()
{
  ///////////////////////////////////////////////////////
  // based on the vars hcd,hcm,hcy and hch             //
  // the hour count in hc is calculated                //
  ///////////////////////////////////////////////////////
    
  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((hcy - 2021) / 4);    //in the year 2025 we need to start with one jump day
  if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2))  {t1++;} //if feb has pars in a jump year we also need to add one jump day
  t1 = hcd + t1 + (365*(hcy-2022)); //then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
  if (hcm > 1) {t1 = t1+31;} //then we add the days of all past months in this year vvv
  if (hcm > 2) {t1 = t1+28;}
  if (hcm > 3) {t1 = t1+31;}
  if (hcm > 4) {t1 = t1+30;}
  if (hcm > 5) {t1 = t1+31;}
  if (hcm > 6) {t1 = t1+30;}
  if (hcm > 7) {t1 = t1+31;}
  if (hcm > 8) {t1 = t1+31;}
  if (hcm > 9) {t1 = t1+30;}
  if (hcm > 10) {t1 = t1+31;}
  if (hcm > 11) {t1 = t1+30;}
  hc = (24*(t1-1)) + hch; //because the day has not passed we reduce one day, multiply with 24 and add the current hour.
  //var ss = String(hc);
  //document.getElementById("ta1").innerHTML ="Hour Count: " + ss;
}


function OnStart()
{
  ////////////////////////////////////
  ///  arrange currency character  ///
  ////////////////////////////////////
  s1 = window.localStorage.getItem("myCurrencyChar");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    currencyChar = "ᕫ";
  }
  else
  {
    currencyChar = s1;
  }
  ////////////////////////////////
  ///  get transaction number  ///
  ////////////////////////////////
  s1 = window.localStorage.getItem("transaction");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    trantt = -1;
  }
  else
  {
    trantt = Number(s1);
  }
  var myFiles = "";  //to get the filename of your blockchain
  blockchain = ""; //to read your own blockchain
  s1 = window.localStorage.getItem("myFiles");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
  {
    blockchain = "";
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
    window.localStorage.removeItem("securityHash");
    window.localStorage.removeItem("proposal");
    window.open("400_idcard.html","_self");
  }
  else
  {
    var myBlockchain = s1 + ".txt";
    s1 = window.localStorage.getItem(myBlockchain);
    if (s1 == null) {s1 = "";}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
    {
      blockchain = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
      window.localStorage.removeItem("securityHash");
      window.localStorage.removeItem("proposal");
      window.open("400_idcard.html","_self");
    }
    else
    {
      blockchain = s1.trim();  //my entire blockchain
      /////////////////////////////////////////
      ///  remove first part of blockchain  ///
      /////////////////////////////////////////
      var t1 = blockchain.indexOf("<") + 1;
      blockchain = blockchain.substring(t1);
      //alert(blockchain)
      //////////////////////////////////////////
      ///  count the number of transactions  ///
      ////////////////////////////////////////// 
      trantot = (blockchain.match(/</g) || []).length;
      if (trantt == -1)
      {
        trantt = trantot;
      }
      ss = blockchain;
      for(var i = 0; i < trantt; i++) 
      {
        var t1 = ss.indexOf("<") + 1;
        showtrans = ss.substring(0,t1-1);
        if (i < (trantt-1))
        {
          ss = ss.substring(t1);
        }
        else
        {
          ss = "";
        }
      }
      FillTransaction();
    }
  //rem
  }
}


function details()
{
  var ss = blockchain;
  for(var i = 0; i < trantt; i++) 
  {
    var t1 = ss.indexOf("<") + 1;
    showtrans = ss.substring(0,t1-1);
    if (i < (trantt-1))
    {
      ss = ss.substring(t1);
    }
    else
    {
      ss = "";
    }
  }
  window.localStorage.setItem("transaction",String(trantt));
  window.localStorage.setItem("details",showtrans);
  window.open("302_details.html","_self");
}


function older()
{
  trantt--;
  if (trantt<1)
  {
    trantt = 1;
  }
  var ss = blockchain;
  for(var i = 0; i < trantt; i++) 
  {
    var t1 = ss.indexOf("<") + 1;
    showtrans = ss.substring(0,t1-1);
    if (i < (trantt-1))
    {
      ss = ss.substring(t1);
    }
    else
    {
      ss = "";
    }
  }
  FillTransaction();
}


function newer()
{
  trantt++;
  if (trantt>trantot)
  {
    trantt = trantot;
  }
  var ss = blockchain;
  for(var i = 0; i < trantt; i++) 
  {
    var t1 = ss.indexOf("<") + 1;
    showtrans = ss.substring(0,t1-1);
    if (i < (trantt-1))
    {
      ss = ss.substring(t1);
    }
    else
    {
      ss = "";
    }
  }
  FillTransaction();
}


function GetName()
{
  ////////////////////////////////////////////////////////////////
  ///  we need to find the name of the person with hash:gname  ///
  ////////////////////////////////////////////////////////////////
  var ssl = gname + ".txt";
  //alert("ssl:"+ssl)
  s1 = window.localStorage.getItem(ssl);
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    alert("Blockchain: "+ssl+" not found!");
    gname = "Name not found";
  }
  else
  {
    var t1 = s1.indexOf("|");
    s1 = s1.substring(t1+1);
    var t1 = s1.indexOf("|");
    s1 = s1.substring(t1+1);
    var t1 = s1.indexOf("|");
    s1 = s1.substring(t1+1);
    var t1 = s1.indexOf("|");
    gname = s1.substring(0,t1);
  }
}



function FillTransaction()
{
  var readx = "";
  var s0 = "";
  var s1 = "";
  var s2 = "";
  var s3 = "";
  var s4 = "";
  var sss = "";
  var ss1 = "";
  var ss2 = "";
  var ss3 = "";
  var ss4 = "";
  var ss5 = "";
  var ssr = "";
  var xr0 = 0;
  var xr1 = 0;
  var xr2 = 0;
  var xr3 = 0;
  var xr4 = 0;
  var xr5 = 0;
  var tx = 0;
  var t0 = 0;
  var delta = 0;
  var fact = 0;
  var others2 = 0;
  var endhash = "";
  document.getElementById("older").classList.remove(document.getElementById("older").classList.item(0));
  if (trantt == 1)
  {
    document.getElementById("older").disabled = true;
    document.getElementById("older").classList.add("disabled_button");
  }
  else
  {
    document.getElementById("older").disabled = false;
    document.getElementById("older").classList.add("main_button");
  }    
  document.getElementById("newer").classList.remove(document.getElementById("newer").classList.item(0));
  if (trantt == trantot)
  {
    document.getElementById("newer").disabled = true;
    document.getElementById("newer").classList.add("disabled_button");
  }
  else
  {
    document.getElementById("newer").disabled = false;
    document.getElementById("newer").classList.add("main_button");
  }    
  //alert(trantt+" - "+showtrans)
  today = new Date();
  //app.ShowPopup(today)
  hcd = Number(String(today.getDate()));
  hcm = Number(String(today.getMonth() + 1)); //January is 0!
  hcy = Number(today.getFullYear());
  hch = Number(String(today.getHours()));
  getDate();
  var hcend = hc; 
  readx = showtrans;
  s0 = readx.substring(0,1);     //can be "R" be "S"
  s1 = readx.substring(1,65);    //hash person that paid to me or received the coin from me
  gname = s1;
  GetName();
  var ssname0 = gname;
  sss = readx.substring(66); 
  t0 = sss.indexOf("|");
  s2 = sss.substring(0,t0);          //timestamp
  sss = sss.substring(t0+1);      
  t0 = s2.indexOf(":");
  hc = Number(s2.substring(0,t0));
  setDate();
  var st1 = timer.substring(0,2)+"-"+timer.substring(2,5)+"-"+timer.substring(5,9)+", "+timer.substring(10,12)+":00";
  t0 = sss.indexOf("|");
  s3 = sss.substring(0,t0);          //securityhash
  sss = sss.substring(t0+1);      
  t0 = sss.indexOf("|");
  ssr = sss.substring(0,t0);          //rr0
  xr0 = Number(ssr);
  others2 = 0;
  sss = sss.substring(t0+1);      
  t0 = sss.indexOf("|");
  endhash = "";
  ssr = "";
  if (t0 > -1)
  {
    others2 = 1;
    ss1 = sss.substring(0,t0); //hash1
    gname = ss1;
    GetName();
    var ssname1 = gname;
    sss = sss.substring(t0+1);      
    t0 = sss.indexOf("|");
    ssr = sss.substring(0,t0); //rr1
    xr1 = Number(ssr);
    sss = sss.substring(t0+1); 
    t0 = sss.indexOf("|");
    if (t0 > -1)
    {
      others2 = 2;
      ss2 = sss.substring(0,t0); //hash2
      gname = ss2;
      GetName();
      var ssname2 = gname;
      sss = sss.substring(t0+1);      
      t0 = sss.indexOf("|");
      ssr = sss.substring(0,t0); //rr2
      xr2 = Number(ssr);
      sss = sss.substring(t0+1); 
      t0 = sss.indexOf("|");
      if (t0 > -1)
      {
        others2 = 3;
        var ss3 = sss.substring(0,t0); //hash3
        gname = ss3;
        GetName();
        var ssname3 = gname;
        sss = sss.substring(t0+1);      
        t0 = sss.indexOf("|");
        ssr = sss.substring(0,t0); //rr3
        xr3 = Number(ssr);
        sss = sss.substring(t0+1); 
        t0 = sss.indexOf("|");
        if (t0 > -1)
        {
          others2 = 4;
          var ss4 = sss.substring(0,t0); //hash4
          gname = ss4;
          GetName();
          var ssname4 = gname;
          sss = sss.substring(t0+1);      
          t0 = sss.indexOf("|");
          ssr = sss.substring(0,t0); //rr4
          xr4 = Number(ssr);
          sss = sss.substring(t0+1); 
          t0 = sss.indexOf("|");
          if (t0 > -1)
          {
            others2 = 5;
            var ss5 = sss.substring(0,t0); //hash5
            gname = ss5;
            GetName();
            var ssname5 = gname;
            sss = sss.substring(t0+1);      
            t0 = sss.indexOf("|");
            ssr = sss.substring(0,t0); //rr5
            xr5 = Number(ssr);
            sss = sss.substring(t0+1); 
            t0 = sss.indexOf("|");
          }
        }
      }
    }
  }
  t0 = sss.indexOf("<");
  endhash = sss.substring(t0-64,t0);          //must be endhash
  //////////////////////
  ///  Process data  ///
  //////////////////////
  t0 = s2.indexOf(":");
  tx = Number(s2.substring(0,t0)); //hour of payment
  delta = (hcend - tx);
  fact = Math.pow(0.6, (delta/8766));
  if (s0 == "R")
  {
    // received coins
    var rrr = xr0 + xr1 + xr2 + xr3 + xr4 + xr5;
    var st0 = format(xr0);
    var st2 = "<font color=#F7E273><big><big><i><b>" + format(rrr)+" "+currencyChar + "</b></i></big></big></font>";
    var st01 = format(xr1)+" "+currencyChar;
    var st02 = format(xr2)+" "+currencyChar;
    var st03 = format(xr3)+" "+currencyChar;
    var st04 = format(xr4)+" "+currencyChar;
    var st05 = format(xr5)+" "+currencyChar;
    rrr = (fact * rrr);
    var st3 = format(rrr)+" "+currencyChar;
    st1 = st2 + "<br><big><i>Present value: <b><font color=#AD8F00>" + st3 + "</font></b></i></big><br>Received on: " + st1 + "<br>";
    if (others2 > 0)
    {
      st1 = st1 + "&nbsp;&nbsp;" + st0 + " " + currencyChar + " Paid by: " + ssname0 + "<br>";
      st1 = st1 + "Additional received coins:";
    }
    else
    {
      st1 = st1 + "Paid by: " + ssname0 + "<br>";
    }
    if (others2 > 0) { st1 = st1 + "<br>&nbsp;&nbsp;" + st01 + " Created by: " + ssname1; }
    if (others2 > 1) { st1 = st1 + "<br>&nbsp;&nbsp;" + st02 + " Created by: " + ssname2; }
    if (others2 > 2) { st1 = st1 + "<br>&nbsp;&nbsp;" + st03 + " Created by: " + ssname3; }
    if (others2 > 3) { st1 = st1 + "<br>&nbsp;&nbsp;" + st04 + " Created by: " + ssname4; }
    if (others2 > 4) { st1 = st1 + "<br>&nbsp;&nbsp;" + st05 + " Created by: " + ssname5; }
  }
  else
  {
    // spent coins
    var rrr = xr0 + xr1 + xr2 + xr3 + xr4 + xr5;
    var st0 = format(xr0);
    var st2 = "<font color=#ff0000><big><big><i><b>" + format(rrr)+" "+currencyChar + "</b></i></big></big></font>";
    var st01 = format(xr1)+" "+currencyChar;
    var st02 = format(xr2)+" "+currencyChar;
    var st03 = format(xr3)+" "+currencyChar;
    var st04 = format(xr4)+" "+currencyChar;
    var st05 = format(xr5)+" "+currencyChar;
    rrr = (fact * rrr);
    var st3 = format(rrr)+" "+currencyChar;
    st1 = st2 + "<br><big><i>Present value: <b><font color=#990000>" + st3 + "</font></b></i></big><br>Paid on: " + st1 + "<br>";
    st1 = st1 + "Paid to: " + ssname0 + "<br>";
    if (others2 > 0)
    {
      st1 = st1 + "&nbsp;&nbsp;" + st0 + " " + currencyChar + " Paid self originated coins<br>";
      st1 = st1 + "Additional paid coins:";
    }
    if (others2 > 0) { st1 = st1 + "<br>&nbsp;&nbsp;" + st01 + " Created by: " + ssname1; }
    if (others2 > 1) { st1 = st1 + "<br>&nbsp;&nbsp;" + st02 + " Created by: " + ssname2; }
    if (others2 > 2) { st1 = st1 + "<br>&nbsp;&nbsp;" + st03 + " Created by: " + ssname3; }
    if (others2 > 3) { st1 = st1 + "<br>&nbsp;&nbsp;" + st04 + " Created by: " + ssname4; }
    if (others2 > 4) { st1 = st1 + "<br>&nbsp;&nbsp;" + st05 + " Created by: " + ssname5; }
  }
  showtext = "<p>"+
        "<font color=#cccccc><br><small><hr>"+
        "<br>"+st1+"<br>"+
        "<hr><br></small></font>"+
        "</p>";

  document.getElementById("transaction").innerHTML = showtext;
  //add possibility to see IDCards of people that pay, to see Security Image todo
}