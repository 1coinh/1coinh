/**
 * requires format from ./hash.js,
 * this file should be included in the calling html file
 */

/* global format */
var myCurrentHash = "";
var senderIDCard = "";
var senderIDCode = "";
var senderName = "";
var senderBlockchain = "";
var senderCard = "";
var myFile = "";
var fileContent;
var currencyChar = "ᕫ";
var name0 = "";
var name1 = "";
var name2 = "";
var name3 = "";
var name4 = "";
var name5 = "";
var total = 0;
var perc = 50;
var sortbut = 1;
var hash0 = "";
var hash1 = "";
var hash2 = "";
var hash3 = "";
var hash4 = "";
var hash5 = "";
var amountTot = 0;
var amount0 = 0;
var amount1 = 0;
var amount2 = 0;
var amount3 = 0;
var amount4 = 0;
var amount5 = 0;
var xamount1 = 0; //for manual function
var xamount2 = 0; //for manual function
var xamount3 = 0; //for manual function
var xamount4 = 0; //for manual function
var xamount5 = 0; //for manual function
var rr0 = 0;
var rr1 = 0;
var rr2 = 0;
var rr3 = 0;
var rr4 = 0;
var rr5 = 0;
var arname = Array(5000).fill("");
var arhash = Array(5000).fill("");
var armyhash = Array(5000).fill("");
var aramount = Array(5000).fill(0);
var arcount = 0;
var armycount = 0;
var others = 0;
var allOthers = 0;
var senderPerc = 0.5;
var orig = ""; //is used to copy data of originators to clipboard
var securityhash = ""; //is the security hash variable
var hc = 0;   //hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time). The counter does not need to look at daylight saving time or time zones
var hcd = 13; //day
var hcm = 2; //month
var hcy = 2035; //year
var hch = 11; //hour
var hcmin = 0; //minutes
var hcsec = 0; //seconds
var hcmsec = 0; //milliseconds
var bdd = 13;   //birthday day
var bmm = 2;    //birthday month
var byy = 1965; //birthday year
var bhh = 0;    //birthday hour
//var MyBhh = 0;  //my birthday hour
var timer = ""; //string to represent the progress in time in the DATASET


const slide_button = document.getElementById("myRange");


function setDate() {
  ///////////////////////////////////////////////////////
  // based on hc, the vars hcd,hcm,hcy and hch are set //
  // the date in hc_date is also set                   //
  ///////////////////////////////////////////////////////
  var t1 = 0;
  var t5 = 0;
  hcd = Math.trunc(hc / 24);                          //hcd = daynumber since 01-01-2022 where 01-01-2022 is day 0 here
  hch = hc - (24 * hcd);                              //hch = number of hours on day t1
  t1 = Math.trunc(hcd / 1461);                      //t1 = number of clusters of 4 years (=1461 days) that are past
  hcd = 1 + (hcd - (1461 * t1));                    //hcd = remaining number of days in remaining cluster, add one day to make 01-01-2022 day 1
  t5 = 0;                                           //t5 = number of years that has past in the remaining cluster 
  if (hcd > 365) { t5++; hcd = hcd - 365; }               //t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 365) && (t5 == 1)) { t5++; hcd = hcd - 365; }//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 366) && (t5 == 2)) { t5++; hcd = hcd - 366; }//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  hcy = 2022 + t5 + (4 * t1);
  //hcd is now the day number in year hcy
  hcm = 1;
  if (hcd > 31) { hcm = 2; hcd = hcd - 31; }
  if (t5 == 2) {
    if ((hcd > 29) && (hcm > 1)) { hcm = 3; hcd = hcd - 29; }
  } else {
    if ((hcd > 28) && (hcm > 1)) { hcm = 3; hcd = hcd - 28; }
  }
  if ((hcd > 31) && (hcm > 2)) { hcm = 4; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 3)) { hcm = 5; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 4)) { hcm = 6; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 5)) { hcm = 7; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 6)) { hcm = 8; hcd = hcd - 31; }
  if ((hcd > 31) && (hcm > 7)) { hcm = 9; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 8)) { hcm = 10; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 9)) { hcm = 11; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 10)) { hcm = 12; hcd = hcd - 30; }
  var mnd = "Jan";
  switch (hcm) {
  case 1: { var mnd = "Jan"; } break;
  case 2: { var mnd = "Feb"; } break;
  case 3: { var mnd = "Mar"; } break;
  case 4: { var mnd = "Apr"; } break;
  case 5: { var mnd = "May"; } break;
  case 6: { var mnd = "Jun"; } break;
  case 7: { var mnd = "Jul"; } break;
  case 8: { var mnd = "Aug"; } break;
  case 9: { var mnd = "Sep"; } break;
  case 10: { var mnd = "Oct"; } break;
  case 11: { var mnd = "Nov"; } break;
  case 12: { var mnd = "Dec"; } break;
  }
  day = (hcd < 10 ? "0" : "") + hcd;
  hour = (hch < 10 ? "0" : "") + hch;
  timer = day + mnd + hcy + "_" + hour + "u";
}


function getDate() {
  ///////////////////////////////////////////////////////
  // based on the vars hcd,hcm,hcy and hch             //
  // the hour count in hc is calculated                //
  ///////////////////////////////////////////////////////

  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((hcy - 2021) / 4);    //in the year 2025 we need to start with one jump day
  if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2)) { t1++; } //if feb has pars in a jump year we also need to add one jump day
  t1 = hcd + t1 + (365 * (hcy - 2022)); //then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
  if (hcm > 1) { t1 = t1 + 31; } //then we add the days of all past months in this year vvv
  if (hcm > 2) { t1 = t1 + 28; }
  if (hcm > 3) { t1 = t1 + 31; }
  if (hcm > 4) { t1 = t1 + 30; }
  if (hcm > 5) { t1 = t1 + 31; }
  if (hcm > 6) { t1 = t1 + 30; }
  if (hcm > 7) { t1 = t1 + 31; }
  if (hcm > 8) { t1 = t1 + 31; }
  if (hcm > 9) { t1 = t1 + 30; }
  if (hcm > 10) { t1 = t1 + 31; }
  if (hcm > 11) { t1 = t1 + 30; }
  hc = (24 * (t1 - 1)) + hch; //because the day has not passed we reduce one day, multiply with 24 and add the current hour.
  //var ss = String(hc);
  //document.getElementById("ta1").innerHTML ="Hour Count: " + ss;
}



function OnStart() {
  ////////////////////////////////////////////////////////////////////////////
  ///  if payment info is already loaded but not finalised and deleted we  ///
  ///  skip this part of the process so we dont interfere with the non-    ///
  ///  processed data of the previous payment                              ///
  ////////////////////////////////////////////////////////////////////////////
  var s1 = localStorage.getItem("payment");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    //do nothing
  }
  else {
    window.open("202_receive2.html", "_self");
  }

  ////////////////////////////////////
  ///  arrange currency character  ///
  ////////////////////////////////////
  var s1 = window.localStorage.getItem("myCurrencyChar");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    document.getElementById("curchar").innerHTML = "ᕫ";
    window.localStorage.setItem("myCurrencyChar", "ᕫ");
    currencyChar = "ᕫ";
  }
  else {
    document.getElementById("curchar").innerHTML = s1;
    currencyChar = s1;
  }
  ///////////////////////////
  ///  set myCurrentHash  ///
  ///////////////////////////

  myCurrentHash = "";
  var s1 = window.localStorage.getItem("myCurrentHash");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    alert("You don't have a proper IDCard. You are directed to 'IDCard' to set your IDCard.");
    window.localStorage.removeItem("securityHash");
    window.localStorage.removeItem("payer");
    window.localStorage.removeItem("payment");
    window.localStorage.removeItem("downloadProposal");
    window.open("400_idcard.html", "_self");
  }
  else {
    myCurrentHash = s1;
    /////////////////////////////////////////////////
    ///  set name0, mamount0 of person that pays  ///
    /////////////////////////////////////////////////
    var ss = window.localStorage.getItem("payer");  //Gloria_Moses-01,86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982
    var t2 = ss.indexOf(",");
    senderIDCode = ss.substring(t2 + 1); //86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982
    senderIDCard = senderIDCode + ".png"; //including .png = 86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982.png
    t2 = ss.indexOf("-");
    senderCard = ss.substring(t2 + 1, t2 + 3); //01
    senderName = ss.substring(0, t2);  //Gloria_Moses
    senderName = senderName.replace(/_/g, " ");  //Gloria Moses
    name0 = senderName; // name0 = "Gloria Moses"
    ///////////////////////////////////////////////////////
    ///  read full hash of person that sends the coins  ///
    ///////////////////////////////////////////////////////
    senderBlockchain = senderIDCode + ".txt"; //  = 86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982.txt
    var readz = window.localStorage.getItem(senderBlockchain);
    senderIDCode = readz.substring(0, 64);
    hash0 = senderIDCode; //because we will also do the max 5 others
    ///////////////////////////////////////////////////////////////////////////////////
    ///  First I need to make a list of others that are in my own blockchain to be  /// 
    ///  able to compare them with the others in Senders' blockchain                ///
    ///////////////////////////////////////////////////////////////////////////////////
    myfile = window.localStorage.getItem("myFiles") + ".txt";
    var isthere = false;
    var isthere2 = false;
    var arpoint = -1;
    ////////////////////////////////
    ///  read my own blockchain  ///
    ////////////////////////////////
    var read = window.localStorage.getItem(myfile);
    // remove the initial string from your blockchain
    var t1 = read.indexOf("<") + 1;
    read = read.substring(t1);
    //////////////////////////////////////////
    ///  count the number of transactions  ///
    //////////////////////////////////////////
    t2 = (read.match(/</g) || []).length;
    // arname = Array(5000).fill("")
    // arhash = Array(5000).fill("")
    // aramount = Array(5000).fill(0)
    armycount = 0;
    armyhash[armycount] = myCurrentHash;
    armycount++;
    var isthere = false;
    var readx = "";
    var s0 = "";
    var s2 = "";
    var s3 = "";
    var s4 = "";
    var sss = "";
    var ss1 = "";
    var ss2 = "";
    var ss3 = "";
    var ss4 = "";
    var ss5 = "";
    var ssr = "";
    var tx = 0;
    var t0 = 0;
    var delta = 0;
    var fact = 0;
    var others2 = 0;
    var endhash = "";
    var j = 0;
    //app.ShowPopup(t2)
    if (t2 > 0) {
      for (var i = 0; i < t2; i++) {
        ss1 = "";
        ss2 = "";
        ss3 = "";
        ss4 = "";
        ss5 = "";
        t1 = read.indexOf("<");
        readx = read.substring(0, t1 + 1); //including "<"
        //////////////////////////////////////////
        ///  get remaining part of blockchain  ///
        ///  to be used in the next run        ///
        //////////////////////////////////////////
        if (read.length > (t1 + 3)) //string is not finished
        {
          read = read.substring(t1 + 1); //to be used in next for loop cycle
        }
        //////////////////////////////////////////
        readx = readx.trim();
        s0 = readx.substring(0, 1);     //can be "R" be "S"
        s1 = readx.substring(1, 65);    //must be hash of person that receives (when it is S / sent by me) or pays the coin (and I R Receive them)
        sss = readx.substring(66);
        t0 = sss.indexOf("|");
        s2 = sss.substring(0, t0);          //timestamp
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        s3 = sss.substring(0, t0);          //securityhash
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        ssr = sss.substring(0, t0);          //rr0
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        endhash = "";
        ssr = "";
        if (t0 > -1) {
          ss1 = sss.substring(0, t0); //hash1
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          ssr = sss.substring(0, t0); //rr1
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          if (t0 > -1) {
            ss2 = sss.substring(0, t0); //hash2
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
            ssr = sss.substring(0, t0); //rr2
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
            if (t0 > -1) {
              var ss3 = sss.substring(0, t0); //hash3
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              ssr = sss.substring(0, t0); //rr3
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              if (t0 > -1) {
                var ss4 = sss.substring(0, t0); //hash4
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                ssr = sss.substring(0, t0); //rr4
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 > -1) {
                  var ss5 = sss.substring(0, t0); //hash5
                  sss = sss.substring(t0 + 1);
                  t0 = sss.indexOf("|");
                  ssr = sss.substring(0, t0); //rr5
                  sss = sss.substring(t0 + 1);
                  t0 = sss.indexOf("|");
                }
              }
            }
          }
        }
        //t0 = sss.indexOf("<")
        // end
        //haand check ID-Card 
        // numberand check ID-Card 
        // numberand check ID-Card numbersh = 
        //sss.substring(t0-64,t0)          //must be endhash //check what happened here? todo

        t0 = sss.indexOf("<");
        endhash = sss.substring(t0 - 64, t0);

        ////////////////////
        ///  process s1  ///
        ////////////////////
        if (s1 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == s1) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = s1;
            armycount++;
          }
        }
        /////////////////////
        ///  process ss1  ///
        /////////////////////
        if (ss1 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss1) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = ss1;
            armycount++;
          }
        }
        /////////////////////
        ///  process ss3  ///
        /////////////////////
        if (ss2 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss2) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = ss2;
            armycount++;
          }
        }
        /////////////////////
        ///  process ss3  ///
        /////////////////////
        if (ss3 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss3) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = ss3;
            armycount++;
          }
        }
        /////////////////////
        ///  process ss4  ///
        /////////////////////
        if (ss4 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss4) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = ss4;
            armycount++;
          }
        }
        /////////////////////
        ///  process ss5  ///
        /////////////////////
        if (ss5 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss5) { isthere = true; }
            }
          }
          if (!isthere) {
            armyhash[armycount] = ss5;
            armycount++;
          }
        }
      }
    }

    // alert("armycount:"+armycount)
    // alert("armyhash[0]:"+armyhash[0])




    ///////////////////////////////////////////////////////////////////////////////////
    ///  When your blockchain doesnt have transactions yet, the armycount is still  ///
    ///  set to one to add the IDCard (that is in /1CoinH/accept)                   ///
    ///  this is necessary to add payer to your list of possible coins you get      ///
    ///////////////////////////////////////////////////////////////////////////////////
    if (armycount == 0) { armycount = 1; }
    ///////////////////////////////////////////////////////////////////
    ///  When I did previous transactions there cound be an overlap ///
    ///  for these people I need to find out how many coins sender  ///
    ///  has in his posession so that he might use them             ///
    ///////////////////////////////////////////////////////////////////
    // var arname = Array(5000).fill("")
    // var arhash = Array(5000).fill("")
    // var aramount = Array(5000).fill(0)
    // multiple ID-Cards of a person todo
    amount0 = 0;
    amount1 = 0;
    amount2 = 0;
    amount3 = 0;
    amount4 = 0;
    amount5 = 0;
    /////////////////////////////////////////////////////////////////////////////
    ///  Then check coins of Sender (person that pays) at the current moment  ///
    /////////////////////////////////////////////////////////////////////////////
    today = new Date();
    //app.ShowPopup(today)
    hcd = Number(String(today.getDate()));
    hcm = Number(String(today.getMonth() + 1)); //January is 0!
    hcy = Number(today.getFullYear());
    hch = Number(String(today.getHours()));
    var sdate = "(Your coins per " + String(hcd);
    if (hcm == 1) { sdate = sdate + " Jan "; }
    if (hcm == 2) { sdate = sdate + " Feb "; }
    if (hcm == 3) { sdate = sdate + " Mar "; }
    if (hcm == 4) { sdate = sdate + " Apr "; }
    if (hcm == 5) { sdate = sdate + " May "; }
    if (hcm == 6) { sdate = sdate + " Jun "; }
    if (hcm == 7) { sdate = sdate + " Jul "; }
    if (hcm == 8) { sdate = sdate + " Aug "; }
    if (hcm == 9) { sdate = sdate + " Sep "; }
    if (hcm == 10) { sdate = sdate + " Okt "; }
    if (hcm == 11) { sdate = sdate + " Nov "; }
    if (hcm == 12) { sdate = sdate + " Dec "; }
    var s1 = String(hch);
    if (hch < 10) { s1 = "0" + s1; }
    //app.ShowPopup(hch)
    getDate(); //this calculates hc (current hour after 1-1-2022)
    var hcend = hc;
    var ss = hcd + "-" + hcm + "-" + hcy;
    //app.ShowPopup(ss)
    var originated = 0;
    var coins = 0;
    for (hc = 1; hc < hcend; hc++) {
      setDate();
      if (bhh < hc) //person must be born
      {
        originated = (originated * 0.999941728183863) + 1;
        coins = coins + 1;
      }
    }
    aramount[0] = originated;
    arname[0] = senderName;
    arhash[0] = senderIDCode;
    arcount = 1;
    ////////////////////////////////
    ///  read blockchain Sender  ///
    ////////////////////////////////
    read = readz; //= blockchain sender file
    // remove the initial string from your blockchain
    t1 = read.indexOf("<") + 1;
    read = read.substring(t1);
    //////////////////////////////////////////
    ///  count the number of transactions  ///
    //////////////////////////////////////////
    t2 = (read.match(/</g) || []).length;
    // arname = Array(5000).fill("")
    // arhash = Array(5000).fill("")
    // aramount = Array(5000).fill(0)

    readx = "";
    s0 = "";
    s2 = "";
    s3 = "";
    s4 = "";
    sss = "";
    ss1 = "";
    ss2 = "";
    ss3 = "";
    ss4 = "";
    ss5 = "";
    ssr = "";
    xr0 = 0;
    xr1 = 0;
    xr2 = 0;
    xr3 = 0;
    xr4 = 0;
    xr5 = 0;
    tx = 0;
    t0 = 0;
    delta = 0;
    fact = 0;
    others2 = 0;
    endhash = "";
    //app.ShowPopup(t2)
    if (t2 > 0) {
      for (var i = 0; i < t2; i++) {
        ss1 = "";
        ss2 = "";
        ss3 = "";
        ss4 = "";
        ss5 = "";
        t1 = read.indexOf("<");
        readx = read.substring(0, t1 + 1); //including "<"
        //////////////////////////////////////////
        ///  get remaining part of blockchain  ///
        ///  to be used in the next run        ///
        //////////////////////////////////////////
        if (read.length > (t1 + 3)) //string is not finished
        {
          read = read.substring(t1 + 1); //to be used in next for loop cycle
        }
        /////////////////////////////////////////
        readx = readx.trim();
        s0 = readx.substring(0, 1);     //can be "R" be "S"
        s1 = readx.substring(1, 65);    //must be person that receives (when it is S / sent by me) or pays the coin (and I R Receive them)
        ////////////////////////////////////////////////////////////////////////////////
        ///  s1 is the hash0 in the transaction in Glorias blockchain). When Gloria  ///
        ///  receives a payment of me (R) my hash is the hash0 in the transaction    ///
        ///  when she sends her coins to me, my hash is also the hash0. It however   ///
        ///  are her coins that are paid to me (not mine coins coming back, when     ///
        ///  she pays my own coins back, that can only be done referring to me as an ///
        ///  other person (which should cause no issues) Glorias coins paid to me    ///
        ///  must however be corrected here.                                         ///
        ////////////////////////////////////////////////////////////////////////////////
        if (s0 == "S") {
          ////////////////////////////////////////////////////////////////////////////////
          ///  s1 must be changed to glorias hash                                      ///
          ///  all transations where Gloria pays (S) the person she pays to            ///
          ///  must be changed to glorias hash, bcause it are her coins going out      ///
          ///  it doesnt matter who receives them. Here we look only at Glorias coins  ///
          ////////////////////////////////////////////////////////////////////////////////
          //alert("senderIDCode:"+senderIDCode)
          s1 = senderIDCode;
        }
        sss = readx.substring(66);
        t0 = sss.indexOf("|");
        s2 = sss.substring(0, t0);          //timestamp
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        s3 = sss.substring(0, t0);          //securityhash
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        ssr = sss.substring(0, t0);          //rr0
        xr0 = Number(ssr);
        others2 = 0;
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        endhash = "";
        ssr = "";
        if (t0 > -1) {
          others2 = 1;
          ss1 = sss.substring(0, t0); //hash1
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          ssr = sss.substring(0, t0); //rr1
          xr1 = Number(ssr);
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          if (t0 > -1) {
            others2 = 2;
            ss2 = sss.substring(0, t0); //hash2
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
            ssr = sss.substring(0, t0); //rr2
            xr2 = Number(ssr);
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
            if (t0 > -1) {
              others2 = 3;
              var ss3 = sss.substring(0, t0); //hash3
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              ssr = sss.substring(0, t0); //rr3
              xr3 = Number(ssr);
              sss = sss.substring(t0 + 1);
              t0 = sss.indexOf("|");
              if (t0 > -1) {
                others2 = 4;
                var ss4 = sss.substring(0, t0); //hash4
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                ssr = sss.substring(0, t0); //rr4
                xr4 = Number(ssr);
                sss = sss.substring(t0 + 1);
                t0 = sss.indexOf("|");
                if (t0 > -1) {
                  others2 = 5;
                  var ss5 = sss.substring(0, t0); //hash5
                  sss = sss.substring(t0 + 1);
                  t0 = sss.indexOf("|");
                  ssr = sss.substring(0, t0); //rr5
                  xr5 = Number(ssr);
                  sss = sss.substring(t0 + 1);
                  t0 = sss.indexOf("|");
                }
              }
            }
          }
        }
        t0 = sss.indexOf("<");
        endhash = sss.substring(t0 - 64, t0);          //must be endhash
        //////////////////////
        ///  Process data  ///
        //////////////////////
        t0 = s2.indexOf(":");
        tx = Number(s2.substring(0, t0)); //hour of payment
        delta = (hcend - tx);
        fact = Math.pow(0.6, (delta / 8766));
        //////////////////////////////////////////////////////////////////////////
        ///  Process data: see which are mutual and count the coins of Sender  ///
        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////
        ///  armyhash and armycount = used to establish all my relations               /// 
        ///  these are only used in the start procedure                                ///
        ///  var armyhash = Array(5000).fill("") = all hashes others in my blockchain  ///
        ///  var armycount = 0 : number of hashes of others in my blockchain           ///
        //////////////////////////////////////////////////////////////////////////////////
        ////////////////////
        ///  process s1  ///
        ////////////////////
        if (s1 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == s1) { isthere = true; } //s1 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == s1) { isthere2 = true; arpoint = j; } //s1 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = s1;
              //arname[arcount] = senderName // is already done
              arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              aramount[arpoint] = aramount[arpoint] + (fact * xr0);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr0);
            }
          }
        }
        /////////////////////
        ///  process ss1  ///
        /////////////////////
        if (ss1 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss1) { isthere = true; } //ss1 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == ss1) { isthere2 = true; arpoint = j; } //ss1 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = ss1;
              arname[arcount] =
                arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              aramount[arpoint] = aramount[arpoint] + (fact * xr1);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr1);
            }
          }
        }
        /////////////////////
        ///  process ss2  ///
        /////////////////////
        if (ss2 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss2) { isthere = true; } //ss2 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == ss2) { isthere2 = true; arpoint = j; } //ss2 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = ss2;
              arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              aramount[arpoint] = aramount[arpoint] + (fact * xr2);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr2);
            }
          }
        }
        /////////////////////
        ///  process ss3  ///
        /////////////////////
        if (ss3 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss3) { isthere = true; } //ss3 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == ss3) { isthere2 = true; arpoint = j; } //ss3 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = ss3;
              arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              aramount[arpoint] = aramount[arpoint] + (fact * xr3);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr3);
            }
          }
        }
        /////////////////////
        ///  process ss4  ///
        /////////////////////
        if (ss4 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss4) { isthere = true; } //ss4 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == ss4) { isthere2 = true; arpoint = j; } //ss4 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = ss4;
              arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              aramount[arpoint] = aramount[arpoint] + (fact * xr4);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr4);
            }
          }
        }
        /////////////////////
        ///  process ss5  ///
        /////////////////////
        if (ss5 != "") {
          isthere = false;
          if (armycount > 0) {
            for (j = 0; j < armycount; j++) {
              if (armyhash[j] == ss5) { isthere = true; } //ss4 = mutual
            }
          }
          if (isthere) {
            isthere2 = false;
            if (arcount > 0) {
              for (j = 0; j < arcount; j++) {
                if (arhash[j] == ss5) { isthere2 = true; arpoint = j; } //ss4 = mutual
              }
            }
            if (!isthere2) {
              arhash[arcount] = ss5;
              arpoint = arcount;
              arcount++;
            }
            ///////////////////////////////////////////////////////////////
            ///  now we need to add the value xr0 to aramount[arpoint]  ///
            ///////////////////////////////////////////////////////////////

            if (s0 == "R") {
              //I can receive coins of sender of from another incl my own coins)
              aramount[arpoint] = aramount[arpoint] + (fact * xr5);
            }
            else {
              aramount[arpoint] = aramount[arpoint] - (fact * xr5);
            }
          }
        }
      }  //end loop
    }
    ///////////////////////////////////////////////////////////////////////////
    ///  arnamr/arhash/aramount/arcount                                     ///
    ///  var arname = Array(5000).fill("")                                  ///
    ///  var arhash = Array(5000).fill("")                                  ///
    ///  var aramount = Array(5000).fill(0)                                 ///
    ///  var arcount = 0   = all mutual relations including yourself        ///
    ///  var others = 0    = all mutual relations excluding yourself max 5  ///
    ///  var allOthers = 0 = all mutual relations excluding yourself        ///
    ///////////////////////////////////////////////////////////////////////////
    ///  first we need to eliminate all persons with a negative amount      ///
    ///  or amount that =0 because you can't use their coins anymore        ///
    ///////////////////////////////////////////////////////////////////////////
    if (arcount > 1) {
      var t = arcount;
      for (j = 1; j < t; j++) {
        if (aramount[j] < 0.01) {
          if (j < (t - 1)) {
            for (i = j + 1; i < t; i++) {
              arname[i - 1] = arname[i];
              arhash[i - 1] = arhash[i];
              aramount[i - 1] = aramount[i];
            }
          }
          arcount--;
        }
      }
    }
    sortDesc();
  }
}



function sliderChange() {
  senderPerc = (Math.round((document.getElementById("myRange").value / 100) * 100) / 100);
  setPercBar();
  calculateOthers();
}


function setPercBar() {
  var ss = format(senderPerc * 100);
  var tt = ss.length;
  ss = ss.substring(0, tt - 3);
  ss = ss + "% Payer";
  document.getElementById("payerperc").innerHTML = ss;
  //app.ShowPopup( "Value = " + value );
}


function amountTyped() {
  var s1 = "";
  var s2 = "";
  var rr = 0;
  var t1 = 0;
  s1 = document.getElementById("amount").value;
  s1 = s1.replace(/[^0-9\.]/g, "");
  if (s1 == "") {
    amountTot = 0;
  }
  else {
    //////////////////////////////////
    ///  count the number of dots. ///
    //////////////////////////////////
    t1 = (s1.match(/\./g) || []).length;
    if (t1 > 1) {
      //////////////////////////////
      ///  max 1 dot is allowed  ///
      ///  only the 1st remains  ///
      //////////////////////////////
      t1 = s1.indexOf(".");
      s2 = s1.substring(t1 + 1);
      s1 = s1.substring(0, t1 + 1) + s2.replace(/\./g, "");
    }
    ////////////////////////////////
    ///  change .123 into 0.123  ///
    ////////////////////////////////
    if (s1.indexOf(".") == 0) {
      s1 = "0" + s1;
    }
    ////////////////////////////////////////
    ///  Don't allow more than 2 digits  ///   123.567   3 < 7-3
    ////////////////////////////////////////
    if ((s1.indexOf(".") > -1) && (s1.indexOf(".") < (s1.length - 3))) {
      s1 = s1.substring(0, s1.length - 1);
    }
    ////////////////////////////////////////////////////////////////////
    ///  don't allow s1 to be bigger than available coins of sender  ///
    ////////////////////////////////////////////////////////////////////
    rr = Number(s1);
    if (rr > amount0) {
      rr = amount0;
      s1 = format(rr).replace(/\,/g, "");
    }
    amountTot = rr;
  }
  document.getElementById("amount").value = s1;
  calculateOthers();
}


function continueButton() {
  var myName = "";
  var s1 = window.localStorage.getItem("myName");
  if (s1 == null) { s1 = ""; }
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    alert("You don't have a proper IDCard. You are directed to 'IDCard' to set your IDCard.");
    window.localStorage.removeItem("securityHash");
    window.localStorage.removeItem("payer");
    window.localStorage.removeItem("payment");
    window.localStorage.removeItem("downloadProposal");
    window.open("400_idcard.html", "_self");
  }
  else {
    myName = s1;
    myCurrentHash = "";
    var s1 = window.localStorage.getItem("myCurrentHash");
    if (s1 == null) { s1 = ""; }
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
      alert("You don't have a proper IDCard. You are directed to 'IDCard' to set your IDCard.");
      window.localStorage.removeItem("securityHash");
      window.localStorage.removeItem("payer");
      window.localStorage.removeItem("payment");
      window.localStorage.removeItem("downloadProposal");
      window.open("400_idcard.html", "_self");
    }
    else {
      myCurrentHash = s1;
      var ss = "";
      var s1 = String(others);
      var ss01 = String(rr0);
      //////////////////////////////////////////////////////////////////////////////////////////
      ///  if I create a proposal how the person that pays should pay, I don't need to send  ///
      ///  the name of the person that pays back to the person that pays, because he/she     ///
      ///  already knows that he needs to pay. Instead, the person that pays, needs to be    ///
      ///  "reminded who he is paying to. So instead of sending him/her his/her own name we  ///
      ///  send here the name and hash of myself, so the app of the person that needs to     ///
      ///  pay I certain who it is going to pay                                              ///
      //////////////////////////////////////////////////////////////////////////////////////////
      ///  1|51.66|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef
      ///  0ede4||71.34|John Doe|d1574a245fd10f8cfaba7a15fd532f2bd2fb1351b62c1658d4d59cee93e70d32|
      var ss02 = myName;
      var ss03 = myCurrentHash;
      var ss11 = String(rr1);
      var ss12 = name1;
      var ss13 = hash1;
      var ss21 = String(rr2);
      var ss22 = name2;
      var ss23 = hash2;
      var ss31 = String(rr3);
      var ss32 = name3;
      var ss33 = hash3;
      var ss41 = String(rr4);
      var ss42 = name4;
      var ss43 = hash4;
      var ss51 = String(rr5);
      var ss52 = name5;
      var ss53 = hash5;
      ss = s1 + "|" + ss01 + "|" + ss02 + "|" + ss03 + "|";
      if (others > 0) { ss = ss + ss11 + "|" + ss12 + "|" + ss13 + "|"; }
      if (others > 1) { ss = ss + ss21 + "|" + ss22 + "|" + ss23 + "|"; }
      if (others > 2) { ss = ss + ss31 + "|" + ss32 + "|" + ss33 + "|"; }
      if (others > 3) { ss = ss + ss41 + "|" + ss42 + "|" + ss43 + "|"; }
      if (others > 4) { ss = ss + ss51 + "|" + ss52 + "|" + ss53 + "|"; }
      localStorage.setItem("payment", ss);
      window.open("202_receive2.html", "_self");
    }
  }
}


function calculateOthers() {
  rr0 = 0;
  rr1 = 0;
  rr2 = 0;
  rr3 = 0;
  rr4 = 0;
  rr5 = 0;
  var block1 = false;
  var block2 = false;
  var block3 = false;
  var block4 = false;
  var block5 = false;
  var blocked = 0;
  var blocked2 = 0;
  var blocked3 = 0;
  var blocked4 = 0;
  var blocked5 = 0;
  ///////////////////////////////////
  ///  Calculate sender + others  ///
  ///////////////////////////////////
  /////////////////////////////////////////////////////////////////
  ///  first only enable the Continue button if the amount > 0  ///
  /////////////////////////////////////////////////////////////////
  document.getElementById("continue").classList.remove(document.getElementById("continue").classList.item(0));
  if (amountTot > 0) {
    document.getElementById("continue").disabled = false;
    document.getElementById("continue").classList.add("main_button");
  }
  else {
    document.getElementById("continue").disabled = true;
    document.getElementById("continue").classList.add("disabled_button");
  }
  //////////////////////////////////////////////////////////////////////////////////////////
  ///  Calculate the remaining portion  that must be distributed over the mut.relations  ///
  //////////////////////////////////////////////////////////////////////////////////////////
  rr0 = amountTot * senderPerc;
  if (rr0 > amount0) {
    rr0 = amount0; //sender cant pay more than he owns
  }
  var remain = amountTot - rr0; //this needs to be distributed over the others
  //////////////////////////////////////////////////////////////////////////////////////////
  var rrx = 0;  //is used to calculate portions
  ///////////////////////////////////////////////////////
  if (others == 1) //others = 1 todo
  {
    rrx = remain;
    if (amount1 < (rrx * 1.5)) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked++; }
    if (blocked > 0) //at least 1 is blocked
    {
      remain = (rr0 + rr1) - amountTot;
      if (remain > 0) {
        ///////////////////////////////////////////////////////////////////////////////////////
        ///  because the 1.5 rule (increase the amount when rrx is close to using the full  /// 
        ///  amount) it is possible that the sum is too much and can be lowered             ///
        ///////////////////////////////////////////////////////////////////////////////////////
        if (rr1 > (remain) * 2) { rr1 = rr1 - remain; remain = 0; }
        remain = (rr0 + rr1) - amountTot;
        if (remain > 0) {
          //if it still not works then we try the smaller amounts                        
          if (rr1 > (remain) * 1.1) { rr1 = rr1 - remain; remain = 0; }
        }
      }
      else {
        if (!block1) { rr1 = rrx; }
      }
    }
    else {  //none is blocked
      rr1 = rrx;
    }
    if (((Math.round((rr0 + rr1) * 100000)) / 100000) != (Math.round(amountTot * 100000) / 100000)) {
      alert("Combination of Sender and these others have not enough funds to pay the amount!");
    }
  }
  ////////////////////////////////////////////////////////////////////////////
  if (others == 2) {
    rrx = remain / 2;
    if (amount1 < (rrx * 1.5)) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked++; }
    if (amount2 < (rrx * 1.5)) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked++; }
    if (blocked > 0) //at least 1 is blocked
    {
      //blocked means that the person gets a different rrx because he cant pay rrx or fills all his coins because of the 1.5 rule
      if (blocked < 2) {
        rrx = remain / (2 - blocked);
        if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked2++; }
        if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked2++; }
      }
      if ((blocked + blocked2) == 2)  //they are all blocked
      {
        remain = (rr0 + rr1 + rr2) - amountTot;
        if (remain > 0) {
          ///////////////////////////////////////////////////////////////////////////////////////
          ///  because the 1.5 rule (increase the amount when rrx is close to using the full  /// 
          ///  amount) it is possible that the sum is too much and can be lowered             ///
          ///////////////////////////////////////////////////////////////////////////////////////
          if (rr1 > (remain) * 2) { rr1 = rr1 - remain; remain = 0; }
          if (rr2 > (remain) * 2) { rr2 = rr2 - remain; remain = 0; }
          remain = (rr0 + rr1 + rr2) - amountTot;
          if (remain > 0) {
            //if it still not works then we try the smaller amounts                        
            if (rr1 > (remain) * 1.1) { rr1 = rr1 - remain; remain = 0; }
            if (rr2 > (remain) * 1.1) { rr2 = rr2 - remain; remain = 0; }
          }
          remain = (rr0 + rr1 + rr2) - amountTot;
          if (remain > 0) {
            if (rr1 > (remain) * 0.5) { rr1 = rr1 - (remain * 0.5); remain = remain * 0.5; }
          }
        }
      }
      else {
        if (!block1) { rr1 = rrx; }
        if (!block2) { rr2 = rrx; }
      }
    }
    else {  //none is blocked
      rr1 = rrx;
      rr2 = rrx;
    }
    if (((Math.round((rr0 + rr1 + rr2) * 100000)) / 100000) != (Math.round(amountTot * 100000) / 100000)) {
      alert("Combination of Sender and these others have not enough funds to pay the amount!");
    }
  }
  ///////////////////////////////////////////////////////
  if (others == 3) {
    rrx = remain / 3;
    if (amount1 < (rrx * 1.5)) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked++; }
    if (amount2 < (rrx * 1.5)) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked++; }
    if (amount3 < (rrx * 1.5)) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked++; }
    if (blocked > 0) //at least 1 is blocked
    {
      //blocked means that the person gets a different rrx because he cant pay rrx or fills all his coins because of the 1.5 rule
      if (blocked < 3) {
        rrx = remain / (3 - blocked);
        if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked2++; }
        if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked2++; }
        if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked2++; }
        if ((blocked2 > 0) && ((blocked + blocked2) < 3)) //at least 1 new person is blocked, but not all...
        {
          rrx = remain / (3 - (blocked + blocked2));
          if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked3++; }
          if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked3++; }
          if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked3++; }
        }
      }
      if ((blocked + blocked2 + blocked3) == 3)  //they are all blocked
      {
        remain = (rr0 + rr1 + rr2 + rr3) - amountTot;
        if (remain > 0) {
          ///////////////////////////////////////////////////////////////////////////////////////
          ///  because the 1.5 rule (increase the amount when rrx is close to using the full  /// 
          ///  amount) it is possible that the sum is too much and can be lowered             ///
          ///////////////////////////////////////////////////////////////////////////////////////
          if (rr1 > (remain) * 2) { rr1 = rr1 - remain; remain = 0; }
          if (rr2 > (remain) * 2) { rr2 = rr2 - remain; remain = 0; }
          if (rr3 > (remain) * 2) { rr3 = rr3 - remain; remain = 0; }
          remain = (rr0 + rr1 + rr2 + rr3) - amountTot;
          if (remain > 0) {
            //if it still not works then we try the smaller amounts                        
            if (rr1 > (remain) * 1.1) { rr1 = rr1 - remain; remain = 0; }
            if (rr2 > (remain) * 1.1) { rr2 = rr2 - remain; remain = 0; }
            if (rr3 > (remain) * 1.1) { rr3 = rr3 - remain; remain = 0; }
          }
          remain = (rr0 + rr1 + rr2 + rr3) - amountTot;
          if (remain > 0) {
            if (rr1 > (remain) * 0.5) { rr1 = rr1 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3) - amountTot;
          if (remain > 0) {
            if (rr3 > (remain) * 0.5) { rr3 = rr3 - (remain * 0.5); remain = remain * 0.5; }
          }
        }
      }
      else {
        if (!block1) { rr1 = rrx; }
        if (!block2) { rr2 = rrx; }
        if (!block3) { rr3 = rrx; }
      }
    }
    else {  //none is blocked
      rr1 = rrx;
      rr2 = rrx;
      rr3 = rrx;
    }
    if (((Math.round((rr0 + rr1 + rr2 + rr3) * 100000)) / 100000) != (Math.round(amountTot * 100000) / 100000)) {
      alert("Combination of Sender and these others have not enough funds to pay the amount!");
    }
  }
  ///////////////////////////////////////////////////////
  if (others == 4) {
    rrx = remain / 4;
    if (amount1 < (rrx * 1.5)) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked++; }
    if (amount2 < (rrx * 1.5)) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked++; }
    if (amount3 < (rrx * 1.5)) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked++; }
    if (amount4 < (rrx * 1.5)) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked++; }
    if (blocked > 0) //at least 1 is blocked
    {
      //blocked means that the person gets a different rrx because he cant pay rrx or fills all his coins because of the 1.5 rule
      if (blocked < 4) {
        rrx = remain / (4 - blocked);
        if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked2++; }
        if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked2++; }
        if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked2++; }
        if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked2++; }
        if ((blocked2 > 0) && ((blocked + blocked2) < 4)) //at least 1 new person is blocked, but not all...
        {
          rrx = remain / (4 - (blocked + blocked2));
          if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked3++; }
          if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked3++; }
          if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked3++; }
          if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked3++; }
          if ((blocked3 > 0) && ((blocked + blocked2 + blocked3) < 4)) //at least 1 new person is blocked, but not all...
          {
            rrx = remain / (4 - (blocked + blocked2 + blocked3));
            if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked4++; }
            if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked4++; }
            if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked4++; }
            if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked4++; }
          }
        }
      }
      if ((blocked + blocked2 + blocked3 + blocked4) == 4)  //they are all blocked
      {
        remain = (rr0 + rr1 + rr2 + rr3 + rr4) - amountTot;
        if (remain > 0) {
          ///////////////////////////////////////////////////////////////////////////////////////
          ///  because the 1.5 rule (increase the amount when rrx is close to using the full  /// 
          ///  amount) it is possible that the sum is too much and can be lowered             ///
          ///////////////////////////////////////////////////////////////////////////////////////
          if (rr1 > (remain) * 2) { rr1 = rr1 - remain; remain = 0; }
          if (rr2 > (remain) * 2) { rr2 = rr2 - remain; remain = 0; }
          if (rr3 > (remain) * 2) { rr3 = rr3 - remain; remain = 0; }
          if (rr4 > (remain) * 2) { rr4 = rr4 - remain; remain = 0; }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4) - amountTot;
          if (remain > 0) {
            //if it still not works then we try the smaller amounts                        
            if (rr1 > (remain) * 1.1) { rr1 = rr1 - remain; remain = 0; }
            if (rr2 > (remain) * 1.1) { rr2 = rr2 - remain; remain = 0; }
            if (rr3 > (remain) * 1.1) { rr3 = rr3 - remain; remain = 0; }
            if (rr4 > (remain) * 1.1) { rr4 = rr4 - remain; remain = 0; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4) - amountTot;
          if (remain > 0) {
            if (rr1 > (remain) * 0.5) { rr1 = rr1 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4) - amountTot;
          if (remain > 0) {
            if (rr3 > (remain) * 0.5) { rr3 = rr3 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4) - amountTot;
          if (remain > 0) {
            if (rr4 > (remain) * 0.5) { rr4 = rr4 - (remain * 0.5); remain = remain * 0.5; }
          }
        }
      }
      else {
        if (!block1) { rr1 = rrx; }
        if (!block2) { rr2 = rrx; }
        if (!block3) { rr3 = rrx; }
        if (!block4) { rr4 = rrx; }
      }
    }
    else {  //none is blocked
      rr1 = rrx;
      rr2 = rrx;
      rr3 = rrx;
      rr4 = rrx;
    }
    if (((Math.round((rr0 + rr1 + rr2 + rr3 + rr4) * 100000)) / 100000) != (Math.round(amountTot * 100000) / 100000)) {
      alert("Combination of Sender and these others have not enough funds to pay the amount!");
    }
  }
  ///////////////////////////////////////////////////////////////////////////
  if (others == 5) {
    rrx = remain / 5;
    if (amount1 < (rrx * 1.5)) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked++; }
    if (amount2 < (rrx * 1.5)) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked++; }
    if (amount3 < (rrx * 1.5)) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked++; }
    if (amount4 < (rrx * 1.5)) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked++; }
    if (amount5 < (rrx * 1.5)) { rr5 = amount5; block5 = true; remain = remain - rr5; blocked++; }
    if (blocked > 0) //at least 1 is blocked
    {
      //blocked means that the person gets a different rrx because he cant pay rrx or fills all his coins because of the 1.5 rule
      if (blocked < 5) {
        rrx = remain / (5 - blocked);
        if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked2++; }
        if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked2++; }
        if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked2++; }
        if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked2++; }
        if ((!block5) && (amount5 < (rrx * 1.5))) { rr5 = amount5; block5 = true; remain = remain - rr5; blocked2++; }
        if ((blocked2 > 0) && ((blocked + blocked2) < 5)) //at least 1 new person is blocked, but not all...
        {
          rrx = remain / (5 - (blocked + blocked2));
          if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked3++; }
          if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked3++; }
          if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked3++; }
          if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked3++; }
          if ((!block5) && (amount5 < (rrx * 1.5))) { rr5 = amount5; block5 = true; remain = remain - rr5; blocked3++; }
          if ((blocked3 > 0) && ((blocked + blocked2 + blocked3) < 5)) //at least 1 new person is blocked, but not all...
          {
            rrx = remain / (5 - (blocked + blocked2 + blocked3));
            if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked4++; }
            if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked4++; }
            if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked4++; }
            if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked4++; }
            if ((!block5) && (amount5 < (rrx * 1.5))) { rr5 = amount5; block5 = true; remain = remain - rr5; blocked4++; }
            if ((blocked4 > 0) && ((blocked + blocked2 + blocked3 + blocked4) < 5)) //at least 1 new person is blocked, but not all...
            {
              rrx = remain / (5 - (blocked + blocked2 + blocked3 + blocked4));
              if ((!block1) && (amount1 < (rrx * 1.5))) { rr1 = amount1; block1 = true; remain = remain - rr1; blocked5++; }
              if ((!block2) && (amount2 < (rrx * 1.5))) { rr2 = amount2; block2 = true; remain = remain - rr2; blocked5++; }
              if ((!block3) && (amount3 < (rrx * 1.5))) { rr3 = amount3; block3 = true; remain = remain - rr3; blocked5++; }
              if ((!block4) && (amount4 < (rrx * 1.5))) { rr4 = amount4; block4 = true; remain = remain - rr4; blocked5++; }
              if ((!block5) && (amount5 < (rrx * 1.5))) { rr5 = amount5; block5 = true; remain = remain - rr5; blocked5++; }
            }
          }
        }
      }
      if ((blocked + blocked2 + blocked3 + blocked4 + blocked5) == 5)  //they are all blocked
      {
        remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
        if (remain > 0) {
          ///////////////////////////////////////////////////////////////////////////////////////
          ///  because the 1.5 rule (increase the amount when rrx is close to using the full  /// 
          ///  amount) it is possible that the sum is too much and can be lowered             ///
          ///////////////////////////////////////////////////////////////////////////////////////
          //first we try the very big amounts
          // var err2 = "Debug"
          // var err1=String(block1)+"-"+String(block2)+"-"+String(block3)+"-"+String(block4)+"-"+String(block5)+" | "+
          //      format(rr1)+" - "+format(rr2)+" - "+format(rr3)+" - "+format(rr4)+" - "+format(rr5)+" - "+format(remain)
          // app.Alert( err1, err2 )
          if (rr1 > (remain) * 2) { rr1 = rr1 - remain; remain = 0; }
          if (rr2 > (remain) * 2) { rr2 = rr2 - remain; remain = 0; }
          if (rr3 > (remain) * 2) { rr3 = rr3 - remain; remain = 0; }
          if (rr4 > (remain) * 2) { rr4 = rr4 - remain; remain = 0; }
          if (rr5 > (remain) * 2) { rr5 = rr5 - remain; remain = 0; }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
          if (remain > 0) {
            //if it still not works then we try the smaller amounts                        
            if (rr1 > (remain) * 1.1) { rr1 = rr1 - remain; remain = 0; }
            if (rr2 > (remain) * 1.1) { rr2 = rr2 - remain; remain = 0; }
            if (rr3 > (remain) * 1.1) { rr3 = rr3 - remain; remain = 0; }
            if (rr4 > (remain) * 1.1) { rr4 = rr4 - remain; remain = 0; }
            if (rr5 > (remain) * 1.1) { rr5 = rr5 - remain; remain = 0; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
          if (remain > 0) {
            if (rr1 > (remain) * 0.5) { rr1 = rr1 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
          if (remain > 0) {
            if (rr3 > (remain) * 0.5) { rr3 = rr3 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
          if (remain > 0) {
            if (rr4 > (remain) * 0.5) { rr4 = rr4 - (remain * 0.5); remain = remain * 0.5; }
          }
          remain = (rr0 + rr1 + rr2 + rr3 + rr4 + rr5) - amountTot;
          if (remain > 0) {
            if (rr5 > (remain) * 0.5) { rr5 = rr5 - (remain * 0.5); remain = remain * 0.5; }
          }
        }
      }
      else {
        if (!block1) { rr1 = rrx; }
        if (!block2) { rr2 = rrx; }
        if (!block3) { rr3 = rrx; }
        if (!block4) { rr4 = rrx; }
        if (!block5) { rr5 = rrx; }
      }
    }
    else {  //none is blocked
      rr1 = rrx;
      rr2 = rrx;
      rr3 = rrx;
      rr4 = rrx;
      rr5 = rrx;
    }
    if (((Math.round((rr0 + rr1 + rr2 + rr3 + rr4 + rr5) * 100000)) / 100000) != (Math.round(amountTot * 100000) / 100000)) {
      alert("Combination of Sender and these others have not enough funds to pay the amount!");
    }
  }
  ///////////////////////////////////////////////////////////////////////////
  var xname0 = name0;
  var xname1 = name1;
  var xname2 = name2;
  var xname3 = name3;
  var xname4 = name4;
  var xname5 = name5;
  if (xname0.length > 17) { xname0 = xname0.substring(0, 17) + "..."; }
  if (xname1.length > 17) { xname1 = xname1.substring(0, 17) + "..."; }
  if (xname2.length > 17) { xname2 = xname2.substring(0, 17) + "..."; }
  if (xname3.length > 17) { xname3 = xname3.substring(0, 17) + "..."; }
  if (xname4.length > 17) { xname4 = xname4.substring(0, 17) + "..."; }
  if (xname5.length > 17) { xname5 = xname5.substring(0, 17) + "..."; }
  var ss0 = format(rr0);
  var ss1 = format(rr1);
  var ss2 = format(rr2);
  var ss3 = format(rr3);
  var ss4 = format(rr4);
  var ss5 = format(rr5);
  ss0 = ss0 + " " + currencyChar;
  ss1 = ss1 + " " + currencyChar;
  ss2 = ss2 + " " + currencyChar;
  ss3 = ss3 + " " + currencyChar;
  ss4 = ss4 + " " + currencyChar;
  ss5 = ss5 + " " + currencyChar;

  document.getElementById("name0").innerHTML = xname0;
  document.getElementById("amount0").innerHTML = ss0;
  if (others == 0) {
    document.getElementById("0perc").style.visibility = "hidden";
    document.getElementById("payerperc").style.visibility = "hidden";
    document.getElementById("100perc").style.visibility = "hidden";
    document.getElementById("myRange").style.visibility = "hidden";
    document.getElementById("mutual").style.visibility = "hidden";
    document.getElementById("name1").style.visibility = "hidden";
    document.getElementById("amount1").style.visibility = "hidden";
    document.getElementById("name2").style.visibility = "hidden";
    document.getElementById("amount2").style.visibility = "hidden";
    document.getElementById("name3").style.visibility = "hidden";
    document.getElementById("amount3").style.visibility = "hidden";
    document.getElementById("name4").style.visibility = "hidden";
    document.getElementById("amount4").style.visibility = "hidden";
    document.getElementById("name5").style.visibility = "hidden";
    document.getElementById("amount5").style.visibility = "hidden";
    document.getElementById("stripe0").style.visibility = "hidden";
    document.getElementById("stripe1").style.visibility = "hidden";
    document.getElementById("total").style.visibility = "hidden";
    document.getElementById("amount6").style.visibility = "hidden";
  }
  if (others == 1) {
    document.getElementById("0perc").style.visibility = "visible";
    document.getElementById("payerperc").style.visibility = "visible";
    document.getElementById("100perc").style.visibility = "visible";
    document.getElementById("myRange").style.visibility = "visible";
    document.getElementById("mutual").style.visibility = "visible";
    document.getElementById("name1").style.visibility = "visible";
    document.getElementById("amount1").style.visibility = "visible";
    document.getElementById("name2").style.visibility = "visible";
    document.getElementById("amount2").style.visibility = "visible";
    document.getElementById("name3").style.visibility = "visible";
    document.getElementById("amount3").style.visibility = "visible";
    document.getElementById("name4").style.visibility = "hidden";
    document.getElementById("amount4").style.visibility = "hidden";
    document.getElementById("name5").style.visibility = "hidden";
    document.getElementById("amount5").style.visibility = "hidden";
    document.getElementById("stripe0").style.visibility = "hidden";
    document.getElementById("stripe1").style.visibility = "hidden";
    document.getElementById("total").style.visibility = "hidden";
    document.getElementById("amount6").style.visibility = "hidden";
    document.getElementById("name2").style.color = "#cccccc";
    document.getElementById("amount2").style.color = "#cccccc";
    document.getElementById("name3").style.color = "#F7E273";
    document.getElementById("amount3").style.color = "#F7E273";
    document.getElementById("name1").innerHTML = xname1;
    document.getElementById("amount1").innerHTML = ss1;
    document.getElementById("name2").innerHTML = "";
    document.getElementById("amount2").innerHTML = "<small>════════════</small>";
    document.getElementById("name3").innerHTML = "Total";
    ss6 = format((rr0 + rr1)) + " " + currencyChar;
    document.getElementById("amount3").innerHTML = ss6;
  }
  if (others == 2) {
    document.getElementById("0perc").style.visibility = "visible";
    document.getElementById("payerperc").style.visibility = "visible";
    document.getElementById("100perc").style.visibility = "visible";
    document.getElementById("myRange").style.visibility = "visible";
    document.getElementById("mutual").style.visibility = "visible";
    document.getElementById("name1").style.visibility = "visible";
    document.getElementById("amount1").style.visibility = "visible";
    document.getElementById("name2").style.visibility = "visible";
    document.getElementById("amount2").style.visibility = "visible";
    document.getElementById("name3").style.visibility = "visible";
    document.getElementById("amount3").style.visibility = "visible";
    document.getElementById("name4").style.visibility = "visible";
    document.getElementById("amount4").style.visibility = "visible";
    document.getElementById("name5").style.visibility = "hidden";
    document.getElementById("amount5").style.visibility = "hidden";
    document.getElementById("stripe0").style.visibility = "hidden";
    document.getElementById("stripe1").style.visibility = "hidden";
    document.getElementById("total").style.visibility = "hidden";
    document.getElementById("amount6").style.visibility = "hidden";
    document.getElementById("name2").style.color = "#cccccc";
    document.getElementById("amount2").style.color = "#cccccc";
    document.getElementById("name3").style.color = "#cccccc";
    document.getElementById("amount3").style.color = "#cccccc";
    document.getElementById("name4").style.color = "#F7E273";
    document.getElementById("amount4").style.color = "#F7E273";
    document.getElementById("name1").innerHTML = xname1;
    document.getElementById("amount1").innerHTML = ss1;
    document.getElementById("name2").innerHTML = xname2;
    document.getElementById("amount2").innerHTML = ss2;
    document.getElementById("name3").innerHTML = "";
    document.getElementById("amount3").innerHTML = "<small>════════════</small>";
    document.getElementById("name4").innerHTML = "Total";
    ss6 = format((rr0 + rr1 + rr2)) + " " + currencyChar;
    document.getElementById("amount4").innerHTML = ss6;
  }
  if (others == 3) {
    document.getElementById("0perc").style.visibility = "visible";
    document.getElementById("payerperc").style.visibility = "visible";
    document.getElementById("100perc").style.visibility = "visible";
    document.getElementById("myRange").style.visibility = "visible";
    document.getElementById("mutual").style.visibility = "visible";
    document.getElementById("name1").style.visibility = "visible";
    document.getElementById("amount1").style.visibility = "visible";
    document.getElementById("name2").style.visibility = "visible";
    document.getElementById("amount2").style.visibility = "visible";
    document.getElementById("name3").style.visibility = "visible";
    document.getElementById("amount3").style.visibility = "visible";
    document.getElementById("name4").style.visibility = "visible";
    document.getElementById("amount4").style.visibility = "visible";
    document.getElementById("name5").style.visibility = "visible";
    document.getElementById("amount5").style.visibility = "visible";
    document.getElementById("stripe0").style.visibility = "hidden";
    document.getElementById("stripe1").style.visibility = "hidden";
    document.getElementById("total").style.visibility = "hidden";
    document.getElementById("amount6").style.visibility = "hidden";
    document.getElementById("name2").style.color = "#cccccc";
    document.getElementById("amount2").style.color = "#cccccc";
    document.getElementById("name3").style.color = "#cccccc";
    document.getElementById("amount3").style.color = "#cccccc";
    document.getElementById("name4").style.color = "#cccccc";
    document.getElementById("amount4").style.color = "#ccccc";
    document.getElementById("name5").style.color = "#F7E273";
    document.getElementById("amount5").style.color = "#F7E273";
    document.getElementById("name1").innerHTML = xname1;
    document.getElementById("amount1").innerHTML = ss1;
    document.getElementById("name2").innerHTML = xname2;
    document.getElementById("amount2").innerHTML = ss2;
    document.getElementById("name3").innerHTML = xname3;
    document.getElementById("amount3").innerHTML = ss3;
    document.getElementById("name4").innerHTML = "";
    document.getElementById("amount4").innerHTML = "<small>════════════</small>";
    document.getElementById("name5").innerHTML = "Total";
    ss6 = format((rr0 + rr1 + rr2 + rr3)) + " " + currencyChar;
    document.getElementById("amount5").innerHTML = ss6;
  }
  if (others == 4) {
    document.getElementById("0perc").style.visibility = "visible";
    document.getElementById("payerperc").style.visibility = "visible";
    document.getElementById("100perc").style.visibility = "visible";
    document.getElementById("myRange").style.visibility = "visible";
    document.getElementById("mutual").style.visibility = "visible";
    document.getElementById("name1").style.visibility = "visible";
    document.getElementById("amount1").style.visibility = "visible";
    document.getElementById("name2").style.visibility = "visible";
    document.getElementById("amount2").style.visibility = "visible";
    document.getElementById("name3").style.visibility = "visible";
    document.getElementById("amount3").style.visibility = "visible";
    document.getElementById("name4").style.visibility = "visible";
    document.getElementById("amount4").style.visibility = "visible";
    document.getElementById("name5").style.visibility = "visible";
    document.getElementById("amount5").style.visibility = "visible";
    document.getElementById("stripe0").style.visibility = "visible";
    document.getElementById("stripe1").style.visibility = "visible";
    document.getElementById("total").style.visibility = "hidden";
    document.getElementById("amount6").style.visibility = "hidden";
    document.getElementById("name2").style.color = "#cccccc";
    document.getElementById("amount2").style.color = "#cccccc";
    document.getElementById("name3").style.color = "#cccccc";
    document.getElementById("amount3").style.color = "#cccccc";
    document.getElementById("name4").style.color = "#cccccc";
    document.getElementById("amount4").style.color = "#cccccc";
    document.getElementById("name5").style.color = "#cccccc";
    document.getElementById("amount5").style.color = "#cccccc";
    document.getElementById("stripe0").style.color = "#F7E273";
    document.getElementById("stripe1").style.color = "#F7E273";
    document.getElementById("name1").innerHTML = xname1;
    document.getElementById("amount1").innerHTML = ss1;
    document.getElementById("name2").innerHTML = xname2;
    document.getElementById("amount2").innerHTML = ss2;
    document.getElementById("name3").innerHTML = xname3;
    document.getElementById("amount3").innerHTML = ss3;
    document.getElementById("name4").innerHTML = xname4;
    document.getElementById("amount4").innerHTML = ss4;
    document.getElementById("name5").innerHTML = "";
    document.getElementById("amount5").innerHTML = "<small>════════════</small>";
    document.getElementById("stripe0").innerHTML = "Total";
    ss6 = format((rr0 + rr1 + rr2 + rr3 + rr4)) + " " + currencyChar;
    document.getElementById("stripe1").innerHTML = ss6;
  }
  if (others == 5) {
    document.getElementById("0perc").style.visibility = "visible";
    document.getElementById("payerperc").style.visibility = "visible";
    document.getElementById("100perc").style.visibility = "visible";
    document.getElementById("myRange").style.visibility = "visible";
    document.getElementById("mutual").style.visibility = "visible";
    document.getElementById("name1").style.visibility = "visible";
    document.getElementById("amount1").style.visibility = "visible";
    document.getElementById("name2").style.visibility = "visible";
    document.getElementById("amount2").style.visibility = "visible";
    document.getElementById("name3").style.visibility = "visible";
    document.getElementById("amount3").style.visibility = "visible";
    document.getElementById("name4").style.visibility = "visible";
    document.getElementById("amount4").style.visibility = "visible";
    document.getElementById("name5").style.visibility = "visible";
    document.getElementById("amount5").style.visibility = "visible";
    document.getElementById("stripe0").style.visibility = "visible";
    document.getElementById("stripe1").style.visibility = "visible";
    document.getElementById("total").style.visibility = "visible";
    document.getElementById("amount6").style.visibility = "visible";
    document.getElementById("name2").style.color = "#cccccc";
    document.getElementById("amount2").style.color = "#cccccc";
    document.getElementById("name3").style.color = "#cccccc";
    document.getElementById("amount3").style.color = "#cccccc";
    document.getElementById("name4").style.color = "#cccccc";
    document.getElementById("amount4").style.color = "#cccccc";
    document.getElementById("name5").style.color = "#cccccc";
    document.getElementById("amount5").style.color = "#cccccc";
    document.getElementById("stripe0").style.color = "#cccccc";
    document.getElementById("stripe1").style.color = "#cccccc";
    document.getElementById("total").style.color = "#F7E273";
    document.getElementById("amount6").style.color = "#F7E273";

    document.getElementById("name1").innerHTML = xname1;
    document.getElementById("amount1").innerHTML = ss1;
    document.getElementById("name2").innerHTML = xname2;
    document.getElementById("amount2").innerHTML = ss2;
    document.getElementById("name3").innerHTML = xname3;
    document.getElementById("amount3").innerHTML = ss3;
    document.getElementById("name4").innerHTML = xname4;
    document.getElementById("amount4").innerHTML = ss4;
    document.getElementById("name5").innerHTML = xname5;
    document.getElementById("amount5").innerHTML = ss5;
    ss6 = format((rr0 + rr1 + rr2 + rr3 + rr4 + rr5)) + " " + currencyChar;
    document.getElementById("amount6").innerHTML = ss6;
  }

  if (others < 2) {
    document.getElementById("btnstring").style.visibility = "hidden";
    document.getElementById("desc").style.visibility = "hidden";
    document.getElementById("asc").style.visibility = "hidden";
    document.getElementById("random").style.visibility = "hidden";
    document.getElementById("manual").style.visibility = "hidden";
  }
  else {
    document.getElementById("btnstring").style.visibility = "visible";
    document.getElementById("desc").style.visibility = "visible";
    document.getElementById("asc").style.visibility = "visible";
    document.getElementById("random").style.visibility = "visible";
    document.getElementById("manual").style.visibility = "visible";
  }
}


function abortButton() {
  var s3 = senderName;
  var s1 = "Do you want to remove the payment data of " + s3 + "?";
  var s2 = "The payment data of " + s3 + " is removed from your local storage.";
  if (window.confirm(s1)) {
    //localStorage.clear();
    window.localStorage.removeItem("payer");
    window.localStorage.removeItem("payment"); //if this is there
    // the blockchain and IDCard of the person are never deleted. only updated.
    alert(s2);
  }
  window.open("index.html", "_self");
}


function sortDesc() {
  //////////////////////////////////////////////////////////////
  ///  The number of mutual people is known now (arcount-1)  ///
  ///  Number zero is sender so we can                       ///
  ///  correct the originate with the adjustments and        ///
  ///  also sort the others.                                 ///
  ///////////////////////////////////////////////////////////////////////////
  ///  arnamr/arhash/aramount/arcount                                     ///
  ///  var arname = Array(5000).fill("")                                  ///
  ///  var arhash = Array(5000).fill("")                                  ///
  ///  var aramount = Array(5000).fill(0)                                 ///
  ///  var arcount = 0   = all mutual relations including yourself        ///
  ///  var others = 0    = all mutual relations excluding yourself max 5  ///
  ///  var allOthers = 0 = all mutual relations excluding yourself        ///
  ///////////////////////////////////////////////////////////////////////////
  var s1 = "";
  var s2 = "";
  var t1 = 0;
  /////////////////////////////////////////////
  ///  sort the descending (biggest first)  ///
  /////////////////////////////////////////////
  if (arcount > 1) {
    var t = arcount;
    for (j = 1; j < t - 1; j++) {
      for (i = j + 1; i < t; i++) {
        if (aramount[j] < aramount[i]) {
          s1 = arname[j];
          s2 = arhash[j];
          t1 = aramount[j];
          arname[j] = arname[i];
          arhash[j] = arhash[i];
          aramount[j] = aramount[i];
          arname[i] = s1;
          arhash[i] = s2;
          aramount[i] = t1;
        }
      }
    }
  }
  /////////////////////////////////////////////
  allOthers = arcount - 1;
  others = allOthers;
  if (others > 5) { others = 5; }
  senderPerc = 0.5;
  if (others == 0) { senderPerc = 1; }
  amount0 = aramount[0];
  if (others > 0) { amount1 = aramount[1]; }
  if (others > 1) { amount2 = aramount[2]; }
  if (others > 2) { amount3 = aramount[3]; }
  if (others > 3) { amount4 = aramount[4]; }
  if (others > 4) { amount5 = aramount[5]; }
  hash0 = arhash[0];
  if (others > 0) { hash1 = arhash[1]; }
  if (others > 1) { hash2 = arhash[2]; }
  if (others > 2) { hash3 = arhash[3]; }
  if (others > 3) { hash4 = arhash[4]; }
  if (others > 4) { hash5 = arhash[5]; }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///  Get names others: Because all the "others" are always mutual, this means you must  ///
  ///  have all the ID-Cards of the "others" in your possession and are able to           ///
  ///  find the names by looking at these ID-Cards in your localStorage                   ///
  ///////////////////////////////////////////////////////////////////////////////////////////
  var ss = "";
  if (others > 0) {
    for (i = 0; i < others; i++) {
      ss = arhash[i + 1] + ".txt";
      s2 = window.localStorage.getItem(ss);
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //30 Jun 2022|15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      arname[i + 1] = s2.substring(0, t1); //Gloria Moses
    }
  }
  if (others > 0) { name1 = arname[1]; }
  if (others > 1) { name2 = arname[2]; }
  if (others > 2) { name3 = arname[3]; }
  if (others > 3) { name4 = arname[4]; }
  if (others > 4) { name5 = arname[5]; }
  ////////////////////////////////////////////////////////
  ///  the array of all mutual relations is filled.    ///
  ///  Based on the sorting keys and manual setting    ///
  ///  max 5 are selected and presented. This happens  ///
  ///  after the percentage slider is processed.       ///
  ////////////////////////////////////////////////////////
  document.getElementById("myRange").value = (senderPerc * 100);
  setPercBar();
  calculateOthers();
}


function sortAsc() {
  //////////////////////////////////////////////////////////////
  ///  The number of mutual people is known now (arcount-1)  ///
  ///  Number zero is sender so we can                       ///
  ///  correct the originate with the adjustments and        ///
  ///  also sort the others.                                 ///
  ///////////////////////////////////////////////////////////////////////////
  ///  arnamr/arhash/aramount/arcount                                     ///
  ///  var arname = Array(5000).fill("")                                  ///
  ///  var arhash = Array(5000).fill("")                                  ///
  ///  var aramount = Array(5000).fill(0)                                 ///
  ///  var arcount = 0   = all mutual relations including yourself        ///
  ///  var others = 0    = all mutual relations excluding yourself max 5  ///
  ///  var allOthers = 0 = all mutual relations excluding yourself        ///
  ///////////////////////////////////////////////////////////////////////////
  var s1 = "";
  var s2 = "";
  var t1 = 0;
  /////////////////////////////////////////////
  ///  sort the descending (biggest first)  ///
  /////////////////////////////////////////////
  if (arcount > 1) {
    var t = arcount;
    for (j = 1; j < t - 1; j++) {
      for (i = j + 1; i < t; i++) {
        if (aramount[j] > aramount[i]) {
          s1 = arname[j];
          s2 = arhash[j];
          t1 = aramount[j];
          arname[j] = arname[i];
          arhash[j] = arhash[i];
          aramount[j] = aramount[i];
          arname[i] = s1;
          arhash[i] = s2;
          aramount[i] = t1;
        }
      }
    }
  }
  /////////////////////////////////////////////
  allOthers = arcount - 1;
  others = allOthers;
  if (others > 5) { others = 5; }
  senderPerc = 0.5;
  if (others == 0) { senderPerc = 1; }
  amount0 = aramount[0];
  if (others > 0) { amount1 = aramount[1]; }
  if (others > 1) { amount2 = aramount[2]; }
  if (others > 2) { amount3 = aramount[3]; }
  if (others > 3) { amount4 = aramount[4]; }
  if (others > 4) { amount5 = aramount[5]; }
  hash0 = arhash[0];
  if (others > 0) { hash1 = arhash[1]; }
  if (others > 1) { hash2 = arhash[2]; }
  if (others > 2) { hash3 = arhash[3]; }
  if (others > 3) { hash4 = arhash[4]; }
  if (others > 4) { hash5 = arhash[5]; }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///  Get names others: Because all the "others" are always mutual, this means you must  ///
  ///  have all the ID-Cards of the "others" in your possession and are able to           ///
  ///  find the names by looking at these ID-Cards in your localStorage                   ///
  ///////////////////////////////////////////////////////////////////////////////////////////
  var ss = "";
  if (others > 0) {
    for (i = 0; i < others; i++) {
      ss = arhash[i + 1] + ".txt";
      s2 = window.localStorage.getItem(ss);
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //30 Jun 2022|15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      arname[i + 1] = s2.substring(0, t1); //Gloria Moses
    }
  }
  if (others > 0) { name1 = arname[1]; }
  if (others > 1) { name2 = arname[2]; }
  if (others > 2) { name3 = arname[3]; }
  if (others > 3) { name4 = arname[4]; }
  if (others > 4) { name5 = arname[5]; }
  ////////////////////////////////////////////////////////
  ///  the array of all mutual relations is filled.    ///
  ///  Based on the sorting keys and manual setting    ///
  ///  max 5 are selected and presented. This happens  ///
  ///  after the percentage slider is processed.       ///
  ////////////////////////////////////////////////////////
  document.getElementById("myRange").value = (senderPerc * 100);
  setPercBar();
  calculateOthers();
}


function sortRand() {
  //////////////////////////////////////////////////////////////
  ///  The number of mutual people is known now (arcount-1)  ///
  ///  Number zero is sender so we can                       ///
  ///  correct the originate with the adjustments and        ///
  ///  also sort the others.                                 ///
  ///////////////////////////////////////////////////////////////////////////
  ///  arnamr/arhash/aramount/arcount                                     ///
  ///  var arname = Array(5000).fill("")                                  ///
  ///  var arhash = Array(5000).fill("")                                  ///
  ///  var aramount = Array(5000).fill(0)                                 ///
  ///  var arcount = 0   = all mutual relations including yourself        ///
  ///  var others = 0    = all mutual relations excluding yourself max 5  ///
  ///  var allOthers = 0 = all mutual relations excluding yourself        ///
  ///////////////////////////////////////////////////////////////////////////
  var s1 = "";
  var s2 = "";
  var t1 = 0;
  var tt = 0;
  /////////////////////////////////////////////
  ///  sort the descending (biggest first)  ///
  /////////////////////////////////////////////
  if (arcount > 6) {
    // Returns a random integer from 1 to arcount
    tt = 1 + Math.floor(Math.random() * arcount);
    s1 = arname[1];
    s2 = arhash[1];
    t1 = aramount[1];
    arname[1] = arname[tt];
    arhash[1] = arhash[tt];
    aramount[1] = aramount[tt];
    arname[tt] = s1;
    arhash[tt] = s2;
    aramount[tt] = t1;
    // Returns a random integer from 2 to arcount
    tt = 2 + Math.floor(Math.random() * arcount);
    s1 = arname[2];
    s2 = arhash[2];
    t1 = aramount[2];
    arname[2] = arname[tt];
    arhash[2] = arhash[tt];
    aramount[2] = aramount[tt];
    arname[tt] = s1;
    arhash[tt] = s2;
    aramount[tt] = t1;
    // Returns a random integer from 3 to arcount
    tt = 3 + Math.floor(Math.random() * arcount);
    s1 = arname[3];
    s2 = arhash[3];
    t1 = aramount[3];
    arname[3] = arname[tt];
    arhash[3] = arhash[tt];
    aramount[3] = aramount[tt];
    arname[tt] = s1;
    arhash[tt] = s2;
    aramount[tt] = t1;
    // Returns a random integer from 4 to arcount
    tt = 4 + Math.floor(Math.random() * arcount);
    s1 = arname[4];
    s2 = arhash[4];
    t1 = aramount[4];
    arname[4] = arname[tt];
    arhash[4] = arhash[tt];
    aramount[4] = aramount[tt];
    arname[tt] = s1;
    arhash[tt] = s2;
    aramount[tt] = t1;
    // Returns a random integer from 5 to arcount
    tt = 5 + Math.floor(Math.random() * arcount);
    s1 = arname[5];
    s2 = arhash[5];
    t1 = aramount[5];
    arname[5] = arname[tt];
    arhash[5] = arhash[tt];
    aramount[5] = aramount[tt];
    arname[tt] = s1;
    arhash[tt] = s2;
    aramount[tt] = t1;
  }
  // if (arcount>1)
  // {
  //     var t = arcount
  //     for (j = 1; j < t-1; j++)
  //     {
  //         for (i = j+1; i < t; i++)
  //         {
  //             if (aramount[j] > aramount[i]) 
  //             {
  //                 s1 = arname[j]    
  //                 s2 = arhash[j]
  //                 t1 = aramount[j]
  //                 arname[j] = arname[i]    
  //                 arhash[j] = arhash[i]
  //                 aramount[j] = aramount[i]
  //                 arname[i] = s1    
  //                 arhash[i] = s2
  //                 aramount[i] = t1
  //             }
  //         }
  //     }
  // }
  /////////////////////////////////////////////
  allOthers = arcount - 1;
  others = allOthers;
  if (others > 5) { others = 5; }
  senderPerc = 0.5;
  if (others == 0) { senderPerc = 1; }
  amount0 = aramount[0];
  if (others > 0) { amount1 = aramount[1]; }
  if (others > 1) { amount2 = aramount[2]; }
  if (others > 2) { amount3 = aramount[3]; }
  if (others > 3) { amount4 = aramount[4]; }
  if (others > 4) { amount5 = aramount[5]; }
  hash0 = arhash[0];
  if (others > 0) { hash1 = arhash[1]; }
  if (others > 1) { hash2 = arhash[2]; }
  if (others > 2) { hash3 = arhash[3]; }
  if (others > 3) { hash4 = arhash[4]; }
  if (others > 4) { hash5 = arhash[5]; }
  ///////////////////////////////////////////////////////////////////////////////////////////
  ///  Get names others: Because all the "others" are always mutual, this means you must  ///
  ///  have all the ID-Cards of the "others" in your possession and are able to           ///
  ///  find the names by looking at these ID-Cards in your localStorage                   ///
  ///////////////////////////////////////////////////////////////////////////////////////////
  var ss = "";
  if (others > 0) {
    for (i = 0; i < others; i++) {
      ss = arhash[i + 1] + ".txt";
      s2 = window.localStorage.getItem(ss);
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //30 Jun 2022|15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //15:23:13|Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      s2 = s2.substring(t1 + 1); //Gloria Moses|04 Sep 1998|-204479
      t1 = s2.indexOf("|");
      arname[i + 1] = s2.substring(0, t1); //Gloria Moses
    }
  }
  if (others > 0) { name1 = arname[1]; }
  if (others > 1) { name2 = arname[2]; }
  if (others > 2) { name3 = arname[3]; }
  if (others > 3) { name4 = arname[4]; }
  if (others > 4) { name5 = arname[5]; }
  ////////////////////////////////////////////////////////
  ///  the array of all mutual relations is filled.    ///
  ///  Based on the sorting keys and manual setting    ///
  ///  max 5 are selected and presented. This happens  ///
  ///  after the percentage slider is processed.       ///
  ////////////////////////////////////////////////////////
  document.getElementById("myRange").value = (senderPerc * 100);
  setPercBar();
  calculateOthers();
}


function sortMan() {
  alert("This function is under construction"); //todo
  sortDesc();
}

