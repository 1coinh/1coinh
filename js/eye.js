/**
 * requires setButtons from ./utils.js,
 * this file should be included in the calling html file
 */

/* global setButtons */


/**
 * Eye Color enum
 * 
 * @enum {string}
 */
const Color = {
  prosthesis: "Prosthesis",
  amber: "Amber",
  blue: "Blue",
  brown: "Brown",
  grey: "Grey",
  green: "Green",
  hazel: "Hazel",
  red: "Red",
  bluegrey: "Blue-Grey",
  bluegreen: "Blue-Green",
  greengrey: "Green-Grey"
};


/**
 * The selected side of the face, should be
 * left or right, but can be anything and
 * defaults to Unknown id not set during
 * OnStart().
 * 
 * @type {string}
 */
var selectedSide;


/**
 * 
 * @param {string} side 
 */
function OnStart(side) { // eslint-disable-line no-unused-vars
  selectedSide = side || "Unknown";
  var elements = document.getElementsByName("radio");
  var eye = window.localStorage.getItem("my" + selectedSide + "Eye") || "";

  for (var i=0, len=elements.length; i<len; ++i) {
    if (getColorValue(elements[i].value) === eye) {
      elements[i].checked = true;
    }

    elements[i].onclick = function() {
      colorSelect();
    };
  }
  setButtons(eye, "eyeContinue");
}

/**
 * User selected a color for the eye
 */
function colorSelect() {
  var eye = document.querySelector("input[type='radio'][name='radio']:checked").value;
  console.log(eye, Color[eye]);
  setButtons(eye, "eyeContinue");
}


/**
 * Save the chosen Eye color to localStorage and
 * return to the ID card
 */
function eyeEnter() { // eslint-disable-line no-unused-vars
  
  var eye = getColorValue(document.querySelector("input[type='radio'][name='radio']:checked").value);
  if (eye) { 
    window.localStorage.setItem("my" + selectedSide + "Eye", eye);
  }
  window.location.href = "400_idcard.html";
}


/**
 * return the Color value
 * @param {string} key
 * @return {string}
 */
function getColorValue(key) {
  for (const lookup in Color) {
    if (lookup === key) {
      return Color[key];
    }
  }
}