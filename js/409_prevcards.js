
function OnStart() {
  var inp = document.getElementById("previdcards");
  inp.onpaste = handlePaste;
  var numcards = 0;
  var s1 = window.localStorage.getItem("myPrevidcards");
  if (s1 == null) {s1 = "";}
  numcards = Number(s1);
  if (numcards != numcards) {numcards = 0;} //catch a NaN issue
  /** this is to prove the user has visited this page so the button 
   * on the 400_IDCard page can be changed
   */
  var s1 = String(numcards);
  window.localStorage.setItem("myPrevidcards", s1); 

  // Delete the entire ID-Codev field
  document.getElementById("card01").innerHTML = "";
  document.getElementById("card02").innerHTML = "";
  document.getElementById("card03").innerHTML = "";
  document.getElementById("card04").innerHTML = "";
  document.getElementById("card05").innerHTML = "";
  document.getElementById("card06").innerHTML = "";
  document.getElementById("card07").innerHTML = "";
  document.getElementById("card08").innerHTML = "";
  document.getElementById("card09").innerHTML = "";
  document.getElementById("card10").innerHTML = "";
  document.getElementById("card11").innerHTML = "";
  document.getElementById("card12").innerHTML = "";
  document.getElementById("card13").innerHTML = "";
  document.getElementById("card14").innerHTML = "";
  document.getElementById("card15").innerHTML = "";
  document.getElementById("card16").innerHTML = "";
  document.getElementById("card17").innerHTML = "";
  document.getElementById("card18").innerHTML = "";
  document.getElementById("card19").innerHTML = "";
  document.getElementById("card20").innerHTML = "";
  document.getElementById("card21").innerHTML = "";
  document.getElementById("card22").innerHTML = "";
  document.getElementById("card23").innerHTML = "";
  document.getElementById("card24").innerHTML = "";
  document.getElementById("card25").innerHTML = "";
  document.getElementById("card26").innerHTML = "";
  document.getElementById("card27").innerHTML = "";
  document.getElementById("card28").innerHTML = "";
  document.getElementById("card29").innerHTML = "";
  document.getElementById("card30").innerHTML = "";
  document.getElementById("card31").innerHTML = "";
  document.getElementById("card32").innerHTML = "";
  
  // get all the IDCard hash-codes
  var card01 = window.localStorage.getItem("card01");
  var card02 = window.localStorage.getItem("card02");
  var card03 = window.localStorage.getItem("card03");
  var card04 = window.localStorage.getItem("card04");
  var card05 = window.localStorage.getItem("card05");
  var card06 = window.localStorage.getItem("card06");
  var card07 = window.localStorage.getItem("card07");
  var card08 = window.localStorage.getItem("card08");
  var card09 = window.localStorage.getItem("card09");
  var card10 = window.localStorage.getItem("card10");
  var card11 = window.localStorage.getItem("card11");
  var card12 = window.localStorage.getItem("card12");
  var card13 = window.localStorage.getItem("card13");
  var card14 = window.localStorage.getItem("card14");
  var card15 = window.localStorage.getItem("card15");
  var card16 = window.localStorage.getItem("card16");
  var card17 = window.localStorage.getItem("card17");
  var card18 = window.localStorage.getItem("card18");
  var card19 = window.localStorage.getItem("card19");
  var card20 = window.localStorage.getItem("card20");
  var card21 = window.localStorage.getItem("card21");
  var card22 = window.localStorage.getItem("card22");
  var card23 = window.localStorage.getItem("card23");
  var card24 = window.localStorage.getItem("card24");
  var card25 = window.localStorage.getItem("card25");
  var card26 = window.localStorage.getItem("card26");
  var card27 = window.localStorage.getItem("card27");
  var card28 = window.localStorage.getItem("card28");
  var card29 = window.localStorage.getItem("card29");
  var card30 = window.localStorage.getItem("card30");
  var card31 = window.localStorage.getItem("card31");
  var card32 = window.localStorage.getItem("card32");
  
  // if cards don't exist, make the card__ var ""
  if (card01 == null) {card01 = "";}
  if (card02 == null) {card02 = "";}
  if (card03 == null) {card03 = "";}
  if (card04 == null) {card04 = "";}
  if (card05 == null) {card05 = "";}
  if (card06 == null) {card06 = "";}
  if (card07 == null) {card07 = "";}
  if (card08 == null) {card08 = "";}
  if (card09 == null) {card09 = "";}
  if (card10 == null) {card10 = "";}
  if (card11 == null) {card11 = "";}
  if (card12 == null) {card12 = "";}
  if (card13 == null) {card13 = "";}
  if (card14 == null) {card14 = "";}
  if (card15 == null) {card15 = "";}
  if (card16 == null) {card16 = "";}
  if (card17 == null) {card17 = "";}
  if (card18 == null) {card18 = "";}
  if (card19 == null) {card19 = "";}
  if (card20 == null) {card20 = "";}
  if (card21 == null) {card21 = "";}
  if (card22 == null) {card22 = "";}
  if (card23 == null) {card23 = "";}
  if (card24 == null) {card24 = "";}
  if (card25 == null) {card25 = "";}
  if (card26 == null) {card26 = "";}
  if (card27 == null) {card27 = "";}
  if (card28 == null) {card28 = "";}
  if (card29 == null) {card29 = "";}
  if (card30 == null) {card30 = "";}
  if (card31 == null) {card31 = "";}
  if (card32 == null) {card32 = "";}
  
  // put the shortend hashcodes in the hash field
  if (numcards > 0) {document.getElementById("card01").innerHTML = card01.substring(0,10)+"...";}
  if (numcards > 1) {document.getElementById("card02").innerHTML = card02.substring(0,10)+"...";}
  if (numcards > 2) {document.getElementById("card03").innerHTML = card03.substring(0,10)+"...";}
  if (numcards > 3) {document.getElementById("card04").innerHTML = card04.substring(0,10)+"...";}
  if (numcards > 4) {document.getElementById("card05").innerHTML = card05.substring(0,10)+"...";}
  if (numcards > 5) {document.getElementById("card06").innerHTML = card06.substring(0,10)+"...";}
  if (numcards > 6) {document.getElementById("card07").innerHTML = card07.substring(0,10)+"...";}
  if (numcards > 7) {document.getElementById("card08").innerHTML = card08.substring(0,10)+"...";}
  if (numcards > 8) {document.getElementById("card09").innerHTML = card09.substring(0,10)+"...";}
  if (numcards > 9) {document.getElementById("card10").innerHTML = card10.substring(0,10)+"...";}
  if (numcards >10) {document.getElementById("card11").innerHTML = card11.substring(0,10)+"...";}
  if (numcards >11) {document.getElementById("card12").innerHTML = card12.substring(0,10)+"...";}
  if (numcards >12) {document.getElementById("card13").innerHTML = card13.substring(0,10)+"...";}
  if (numcards >13) {document.getElementById("card14").innerHTML = card14.substring(0,10)+"...";}
  if (numcards >14) {document.getElementById("card15").innerHTML = card15.substring(0,10)+"...";}
  if (numcards >15) {document.getElementById("card16").innerHTML = card16.substring(0,10)+"...";}
  if (numcards >16) {document.getElementById("card17").innerHTML = card17.substring(0,10)+"...";}
  if (numcards >17) {document.getElementById("card18").innerHTML = card18.substring(0,10)+"...";}
  if (numcards >18) {document.getElementById("card19").innerHTML = card19.substring(0,10)+"...";}
  if (numcards >19) {document.getElementById("card20").innerHTML = card20.substring(0,10)+"...";}
  if (numcards >20) {document.getElementById("card21").innerHTML = card21.substring(0,10)+"...";}
  if (numcards >21) {document.getElementById("card22").innerHTML = card22.substring(0,10)+"...";}
  if (numcards >22) {document.getElementById("card23").innerHTML = card23.substring(0,10)+"...";}
  if (numcards >23) {document.getElementById("card24").innerHTML = card24.substring(0,10)+"...";}
  if (numcards >24) {document.getElementById("card25").innerHTML = card25.substring(0,10)+"...";}
  if (numcards >25) {document.getElementById("card26").innerHTML = card26.substring(0,10)+"...";}
  if (numcards >26) {document.getElementById("card27").innerHTML = card27.substring(0,10)+"...";}
  if (numcards >27) {document.getElementById("card28").innerHTML = card28.substring(0,10)+"...";}
  if (numcards >28) {document.getElementById("card29").innerHTML = card29.substring(0,10)+"...";}
  if (numcards >29) {document.getElementById("card30").innerHTML = card30.substring(0,10)+"...";}
  if (numcards >30) {document.getElementById("card31").innerHTML = card31.substring(0,10)+"...";}
  if (numcards >31) {document.getElementById("card32").innerHTML = card32.substring(0,10)+"...";}
  /**
   * get the current IDCard hash-code of the user
   * and put it in the text input box and focus
   */
  var mycurrenthash = window.localStorage.getItem("MyCurrentHash");
  if (mycurrenthash == null) {mycurrenthash = "";}
  /**
   * enable or disable the My Hash button
   * because MyCurrentHash wont change on this page the
   * button only needs to be en/disabled once here
   */
  document.getElementById("pasteCurrentIDCode").classList.remove(document.getElementById("pasteCurrentIDCode").classList.item(0));
  if (mycurrenthash == "") {
    // input text box previdcards stays empty
    document.getElementById("previdcards").value = "";
    document.getElementById("pasteCurrentIDCode").disabled = true;
    document.getElementById("pasteCurrentIDCode").classList.add("disabled_button");
  } else {
    // my hash is pasted in the input text box previdcards
    document.getElementById("previdcards").value = mycurrenthash;
    document.getElementById("pasteCurrentIDCode").disabled = false;
    document.getElementById("pasteCurrentIDCode").classList.add("main_button");
  }
  /**
   * set focus on the input text box
   * (even when the input text box is empty)
   */
  document.getElementById("previdcards").focus();
  /**
   * see how the delete Last ID button should be set
   * it is set here and need be reviewed only after
   * a previous IDCard is deleted. The button only
   * disables when there are no prev. IDCards anymore
   */
  document.getElementById("previdcardsDelete").classList.remove(document.getElementById("previdcardsDelete").classList.item(0));
  if (numcards == 0) {
    document.getElementById("previdcardsDelete").disabled = true;
    document.getElementById("previdcardsDelete").classList.add("disabled_button");
  } else {
    document.getElementById("previdcardsDelete").disabled = false;
    document.getElementById("previdcardsDelete").classList.add("main_button");
  }

  /** the input text box can be empty or filled with My Current Hash
   * so we need to check the text in the input text box to see if
   * it is suitable to be added to the list
   */
  previdcardsTyped();
}


/**
 * 
 * @param {*} event 
 */
function handlePaste(event) 
{
  let pastedText = (event.clipboardData || window.clipboardData).getData("text");
  //alert('Pasted!\n' + pastedText);
  document.getElementById("previdcards").value = pastedText;
  previdcardsTyped();
}

/**
 * here we check - after the text input box is changed - if the text that is
 * in the text input box is suitable to be added as new id-card hash. If the
 * 32 slots are all taken, the +add button should be disabled here
 */
function previdcardsTyped() {
  var err1 = false;  // test for a 64 char hash
  var err2 = false;  // test for wrong chars in the hash
  var err3 = false;  // tests for if hash is already in prev list
  var previdcard = document.getElementById("previdcards").value;
  var ss = "";
  var s2 = "";
  var i = 0;
  var tt = 0;
  var t1 = 0;
  var t2 = 0;
  var t3 = 0;
  var s1 = "";
  var err = false;

  // test if the trimmed text in the input text box is a 64 char hash  ///
  previdcard = previdcard.replace(" ", "");
  previdcard = previdcard.toLowerCase();
  if (previdcard.length > 0) {
    for(i = 0; i < 64; i++) {
      code = previdcard.charCodeAt(i);
      if (((code<47) || ((code > 57) && (code<97)) || (code>102))) {
        // test for illegal characters in input text
        err2 = true;
        ss="";
        if (i>0)
        {
          ss = previdcard.substring(0, i);
        }
        ss = ss + "_";
        if ((i<63) && (i<(previdcard.length-1)))
        {
          ss = ss + previdcard.substr(i+1);
        }
        previdcard = ss;
      }
    }
  }

  // replace the (possibly changed previdcard string in the text input box
  document.getElementById("previdcards").value = previdcard;
  if (previdcard.length != 64) {
    // test for length input text box
    err1 = true;
  }

  // test if hash is already in the previous list
  var numcards = 0;
  var s1 = window.localStorage.getItem("myPrevidcards") || "";

  numcards = Number(s1);
  if (!numcards) {numcards = 0;} //catch NaN issue
  
  // get all the IDCard hash-codes
  var card01 = window.localStorage.getItem("card01");
  var card02 = window.localStorage.getItem("card02");
  var card03 = window.localStorage.getItem("card03");
  var card04 = window.localStorage.getItem("card04");
  var card05 = window.localStorage.getItem("card05");
  var card06 = window.localStorage.getItem("card06");
  var card07 = window.localStorage.getItem("card07");
  var card08 = window.localStorage.getItem("card08");
  var card09 = window.localStorage.getItem("card09");
  var card10 = window.localStorage.getItem("card10");
  var card11 = window.localStorage.getItem("card11");
  var card12 = window.localStorage.getItem("card12");
  var card13 = window.localStorage.getItem("card13");
  var card14 = window.localStorage.getItem("card14");
  var card15 = window.localStorage.getItem("card15");
  var card16 = window.localStorage.getItem("card16");
  var card17 = window.localStorage.getItem("card17");
  var card18 = window.localStorage.getItem("card18");
  var card19 = window.localStorage.getItem("card19");
  var card20 = window.localStorage.getItem("card20");
  var card21 = window.localStorage.getItem("card21");
  var card22 = window.localStorage.getItem("card22");
  var card23 = window.localStorage.getItem("card23");
  var card24 = window.localStorage.getItem("card24");
  var card25 = window.localStorage.getItem("card25");
  var card26 = window.localStorage.getItem("card26");
  var card27 = window.localStorage.getItem("card27");
  var card28 = window.localStorage.getItem("card28");
  var card29 = window.localStorage.getItem("card29");
  var card30 = window.localStorage.getItem("card30");
  var card31 = window.localStorage.getItem("card31");
  var card32 = window.localStorage.getItem("card32");
  
  // if cards don't exist, make the card__ var ""
  if (card01 == null) {card01 = "";}
  if (card02 == null) {card02 = "";}
  if (card03 == null) {card03 = "";}
  if (card04 == null) {card04 = "";}
  if (card05 == null) {card05 = "";}
  if (card06 == null) {card06 = "";}
  if (card07 == null) {card07 = "";}
  if (card08 == null) {card08 = "";}
  if (card09 == null) {card09 = "";}
  if (card10 == null) {card10 = "";}
  if (card11 == null) {card11 = "";}
  if (card12 == null) {card12 = "";}
  if (card13 == null) {card13 = "";}
  if (card14 == null) {card14 = "";}
  if (card15 == null) {card15 = "";}
  if (card16 == null) {card16 = "";}
  if (card17 == null) {card17 = "";}
  if (card18 == null) {card18 = "";}
  if (card19 == null) {card19 = "";}
  if (card20 == null) {card20 = "";}
  if (card21 == null) {card21 = "";}
  if (card22 == null) {card22 = "";}
  if (card23 == null) {card23 = "";}
  if (card24 == null) {card24 = "";}
  if (card25 == null) {card25 = "";}
  if (card26 == null) {card26 = "";}
  if (card27 == null) {card27 = "";}
  if (card28 == null) {card28 = "";}
  if (card29 == null) {card29 = "";}
  if (card30 == null) {card30 = "";}
  if (card31 == null) {card31 = "";}
  if (card32 == null) {card32 = "";}

  // now see if previdcard already exists
  if (numcards> 0) {err3 = (err3 || (card01 == previdcard));}
  if (numcards> 1) {err3 = (err3 || (card02 == previdcard));}
  if (numcards> 2) {err3 = (err3 || (card03 == previdcard));}
  if (numcards> 3) {err3 = (err3 || (card04 == previdcard));}
  if (numcards> 4) {err3 = (err3 || (card05 == previdcard));}
  if (numcards> 5) {err3 = (err3 || (card06 == previdcard));}
  if (numcards> 6) {err3 = (err3 || (card07 == previdcard));}
  if (numcards> 7) {err3 = (err3 || (card08 == previdcard));}
  if (numcards> 8) {err3 = (err3 || (card09 == previdcard));}
  if (numcards> 9) {err3 = (err3 || (card10 == previdcard));}
  if (numcards>10) {err3 = (err3 || (card11 == previdcard));}
  if (numcards>11) {err3 = (err3 || (card12 == previdcard));}
  if (numcards>12) {err3 = (err3 || (card13 == previdcard));}
  if (numcards>13) {err3 = (err3 || (card14 == previdcard));}
  if (numcards>14) {err3 = (err3 || (card15 == previdcard));}
  if (numcards>15) {err3 = (err3 || (card16 == previdcard));}
  if (numcards>16) {err3 = (err3 || (card17 == previdcard));}
  if (numcards>17) {err3 = (err3 || (card18 == previdcard));}
  if (numcards>18) {err3 = (err3 || (card19 == previdcard));}
  if (numcards>19) {err3 = (err3 || (card20 == previdcard));}
  if (numcards>20) {err3 = (err3 || (card21 == previdcard));}
  if (numcards>21) {err3 = (err3 || (card22 == previdcard));}
  if (numcards>22) {err3 = (err3 || (card23 == previdcard));}
  if (numcards>23) {err3 = (err3 || (card24 == previdcard));}
  if (numcards>24) {err3 = (err3 || (card25 == previdcard));}
  if (numcards>25) {err3 = (err3 || (card26 == previdcard));}
  if (numcards>26) {err3 = (err3 || (card27 == previdcard));}
  if (numcards>27) {err3 = (err3 || (card28 == previdcard));}
  if (numcards>28) {err3 = (err3 || (card29 == previdcard));}
  if (numcards>29) {err3 = (err3 || (card30 == previdcard));}
  if (numcards>30) {err3 = (err3 || (card31 == previdcard));}
  if (numcards>31) {err3 = (err3 || (card32 == previdcard));}

  /**
   * now set the text of mySpan01 and mySpan 02
   *   ss: mySpan1 is always shown (good or bad)
   *   s2: mySpan2 is only showed when there is a problem
   */
  tt = previdcard.length;
  ss = "Hash is valid. Can be added as previous IDCard ["+String(tt)+"]";
  s2 = "&nbsp;";
  if (err1) {
    ss = "Hash invalid. Can't be added as previous IDCard ["+String(tt)+"]"; 
  }
  if (err2) {
    ss = "Hash invalid. Can't be added as previous IDCard ["+String(tt)+"]";
    s2 = "Please remove all ' _' characters.";
  }

  if (err3) {
    ss = "Hash already exists. Can't be added anymore ["+String(tt)+"]";
  }

  if (numcards == 32) {
    err3 = true;
    ss = "All 32 hashes used. Can therefore NOT be added ["+String(tt)+"]";
  }

  if ((err1) || (err2) || (err3)) {
    document.getElementById("mySpan1").style.color = "red";
  } else {
    document.getElementById("mySpan1").style.color = "green";
    var mycurrenthash = window.localStorage.getItem("MyCurrentHash");
    if (mycurrenthash == previdcard)
    {
      ss = "This is your current IDCard hash. Add it? ["+String(tt)+"]";
    }
  }

  if (tt == 0) {
    /**
     * The string is empty (for example after addin a hash) so not to scare with a red error
     * we show a white message to enter a hash
     */
    ss = "Enter a new hash here if you want to add one [0]";
    s2 = "&nbsp;";
    document.getElementById("mySpan1").style.color = "white";
  }
  document.getElementById("mySpan1").innerHTML = ss;
  document.getElementById("mySpan2").innerHTML = s2;

  // set the +Add hash button
  document.getElementById("previdcardsAdd").classList.remove(document.getElementById("previdcardsAdd").classList.item(0));
  if ((err1) || (err2) || (err3)) {
    document.getElementById("previdcardsAdd").disabled = true;
    document.getElementById("previdcardsAdd").classList.add("disabled_button");
  } else {
    document.getElementById("previdcardsAdd").disabled = false;
    document.getElementById("previdcardsAdd").classList.add("main_button");
  }
}


/**
 * the text in the input text box is tested and is allowed to be added
 * thats why the button was enabled. So we add the hash, clear the
 * text input box and disable the add button
 */
function previdcardsAdd()
{
  // first we get the hash from the text input box
  var previdcards = document.getElementById("previdcards").value;
  
  // now we find out on which slot it needs to be added
  var numcards = 0;
  var s1 = window.localStorage.getItem("myPrevidcards");
  if (s1 == null) {s1 = "";}
  numcards = Number(s1);
  if (!numcards) {numcards = 0;} //catch NaN issue
  numcards++;
  
  // now add the previdcards hash in the local storage
  // and increase the numcards in the storage
  var ss = String(numcards);
  window.localStorage.setItem("myPrevidcards", ss);
  if (numcards == 1) {window.localStorage.setItem("card01", previdcards);}
  if (numcards == 2) {window.localStorage.setItem("card02", previdcards);}
  if (numcards == 3) {window.localStorage.setItem("card03", previdcards);}
  if (numcards == 4) {window.localStorage.setItem("card04", previdcards);}
  if (numcards == 5) {window.localStorage.setItem("card05", previdcards);}
  if (numcards == 6) {window.localStorage.setItem("card06", previdcards);}
  if (numcards == 7) {window.localStorage.setItem("card07", previdcards);}
  if (numcards == 8) {window.localStorage.setItem("card08", previdcards);}
  if (numcards == 9) {window.localStorage.setItem("card09", previdcards);}
  if (numcards ==10) {window.localStorage.setItem("card10", previdcards);}
  if (numcards ==11) {window.localStorage.setItem("card11", previdcards);}
  if (numcards ==12) {window.localStorage.setItem("card12", previdcards);}
  if (numcards ==13) {window.localStorage.setItem("card13", previdcards);}
  if (numcards ==14) {window.localStorage.setItem("card14", previdcards);}
  if (numcards ==15) {window.localStorage.setItem("card15", previdcards);}
  if (numcards ==16) {window.localStorage.setItem("card16", previdcards);}
  if (numcards ==17) {window.localStorage.setItem("card17", previdcards);}
  if (numcards ==18) {window.localStorage.setItem("card18", previdcards);}
  if (numcards ==19) {window.localStorage.setItem("card19", previdcards);}
  if (numcards ==20) {window.localStorage.setItem("card20", previdcards);}
  if (numcards ==21) {window.localStorage.setItem("card21", previdcards);}
  if (numcards ==22) {window.localStorage.setItem("card22", previdcards);}
  if (numcards ==23) {window.localStorage.setItem("card23", previdcards);}
  if (numcards ==24) {window.localStorage.setItem("card24", previdcards);}
  if (numcards ==25) {window.localStorage.setItem("card25", previdcards);}
  if (numcards ==26) {window.localStorage.setItem("card26", previdcards);}
  if (numcards ==27) {window.localStorage.setItem("card27", previdcards);}
  if (numcards ==28) {window.localStorage.setItem("card28", previdcards);}
  if (numcards ==29) {window.localStorage.setItem("card29", previdcards);}
  if (numcards ==30) {window.localStorage.setItem("card30", previdcards);}
  if (numcards ==31) {window.localStorage.setItem("card31", previdcards);}
  if (numcards ==32) {window.localStorage.setItem("card32", previdcards);}

  // show the new entry in the iDCodes field
  if (numcards == 1) {document.getElementById("card01").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 2) {document.getElementById("card02").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 3) {document.getElementById("card03").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 4) {document.getElementById("card04").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 5) {document.getElementById("card05").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 6) {document.getElementById("card06").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 7) {document.getElementById("card07").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 8) {document.getElementById("card08").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards == 9) {document.getElementById("card09").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==10) {document.getElementById("card10").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==11) {document.getElementById("card11").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==12) {document.getElementById("card12").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==13) {document.getElementById("card13").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==14) {document.getElementById("card14").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==15) {document.getElementById("card15").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==16) {document.getElementById("card16").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==17) {document.getElementById("card17").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==18) {document.getElementById("card18").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==19) {document.getElementById("card19").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==20) {document.getElementById("card20").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==21) {document.getElementById("card21").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==22) {document.getElementById("card22").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==23) {document.getElementById("card23").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==24) {document.getElementById("card24").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==25) {document.getElementById("card25").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==26) {document.getElementById("card26").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==27) {document.getElementById("card27").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==28) {document.getElementById("card28").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==29) {document.getElementById("card29").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==30) {document.getElementById("card30").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==31) {document.getElementById("card31").innerHTML = previdcards.substring(0,10)+"...";}
  if (numcards ==32) {document.getElementById("card32").innerHTML = previdcards.substring(0,10)+"...";}

  // set the delete button and
  // clear the input text box and run previdcardsTyped()
  if (numcards >31) {
    document.getElementById("previdcardsDelete").classList.remove(document.getElementById("previdcardsDelete").classList.item(0));
    document.getElementById("previdcardsDelete").disabled = true;
    document.getElementById("previdcardsDelete").classList.add("disabled_button");
  } else {
    document.getElementById("previdcardsDelete").classList.remove(document.getElementById("previdcardsDelete").classList.item(0));
    document.getElementById("previdcardsDelete").disabled = false;
    document.getElementById("previdcardsDelete").classList.add("main_button");
  }
  document.getElementById("previdcards").value = "";
  previdcardsTyped();
  /**
   * unless other IDCard pages, adding a IDCard code
   * will not bring you back to the 400_idcard.html page
   * simply because you might have more prev cards to add
   */
}

/**
 * 
 */
function pasteMyHash() {
  var mycurrenthash = window.localStorage.getItem("MyCurrentHash");
  document.getElementById("previdcards").value = mycurrenthash;
  previdcardsTyped();
}

/**
 * Get the number of cards present
 */
function previdcardsDelete() {
  var numcards = 0;
  s1 = window.localStorage.getItem("myPrevidcards");
  if (s1 == null) {s1 = "";}
  numcards = Number(s1);
  if (!numcards) {numcards = 0;} //catch NaN issue

  // Arrange the are you sure question
  var ss="";
  if (numcards == 1) {ss = window.localStorage.getItem("card01");}
  if (numcards == 2) {ss = window.localStorage.getItem("card02");}
  if (numcards == 3) {ss = window.localStorage.getItem("card03");}
  if (numcards == 4) {ss = window.localStorage.getItem("card04");}
  if (numcards == 5) {ss = window.localStorage.getItem("card05");}
  if (numcards == 6) {ss = window.localStorage.getItem("card06");}
  if (numcards == 7) {ss = window.localStorage.getItem("card07");}
  if (numcards == 8) {ss = window.localStorage.getItem("card08");}
  if (numcards == 9) {ss = window.localStorage.getItem("card09");}
  if (numcards ==10) {ss = window.localStorage.getItem("card10");}
  if (numcards ==11) {ss = window.localStorage.getItem("card11");}
  if (numcards ==12) {ss = window.localStorage.getItem("card12");}
  if (numcards ==13) {ss = window.localStorage.getItem("card13");}
  if (numcards ==14) {ss = window.localStorage.getItem("card14");}
  if (numcards ==15) {ss = window.localStorage.getItem("card15");}
  if (numcards ==16) {ss = window.localStorage.getItem("card16");}
  if (numcards ==17) {ss = window.localStorage.getItem("card17");}
  if (numcards ==18) {ss = window.localStorage.getItem("card18");}
  if (numcards ==19) {ss = window.localStorage.getItem("card19");}
  if (numcards ==20) {ss = window.localStorage.getItem("card20");}
  if (numcards ==21) {ss = window.localStorage.getItem("card21");}
  if (numcards ==22) {ss = window.localStorage.getItem("card22");}
  if (numcards ==23) {ss = window.localStorage.getItem("card23");}
  if (numcards ==24) {ss = window.localStorage.getItem("card24");}
  if (numcards ==25) {ss = window.localStorage.getItem("card25");}
  if (numcards ==26) {ss = window.localStorage.getItem("card26");}
  if (numcards ==27) {ss = window.localStorage.getItem("card27");}
  if (numcards ==28) {ss = window.localStorage.getItem("card28");}
  if (numcards ==29) {ss = window.localStorage.getItem("card29");}
  if (numcards ==30) {ss = window.localStorage.getItem("card30");}
  if (numcards ==31) {ss = window.localStorage.getItem("card31");}
  if (numcards ==32) {ss = window.localStorage.getItem("card32");}
  var s1 = "Are you sure to remove the IDCard with hash "+ss+" as one of your previous IDCards?";

  if (window.confirm(s1)) {
    // remove the card from the local storage
    if (numcards == 1) {window.localStorage.removeItem("card01");}
    if (numcards == 2) {window.localStorage.removeItem("card02");}
    if (numcards == 3) {window.localStorage.removeItem("card03");}
    if (numcards == 4) {window.localStorage.removeItem("card04");}
    if (numcards == 5) {window.localStorage.removeItem("card05");}
    if (numcards == 6) {window.localStorage.removeItem("card06");}
    if (numcards == 7) {window.localStorage.removeItem("card07");}
    if (numcards == 8) {window.localStorage.removeItem("card08");}
    if (numcards == 9) {window.localStorage.removeItem("card09");}
    if (numcards ==10) {window.localStorage.removeItem("card10");}
    if (numcards ==11) {window.localStorage.removeItem("card11");}
    if (numcards ==12) {window.localStorage.removeItem("card12");}
    if (numcards ==13) {window.localStorage.removeItem("card13");}
    if (numcards ==14) {window.localStorage.removeItem("card14");}
    if (numcards ==15) {window.localStorage.removeItem("card15");}
    if (numcards ==16) {window.localStorage.removeItem("card16");}
    if (numcards ==17) {window.localStorage.removeItem("card17");}
    if (numcards ==18) {window.localStorage.removeItem("card18");}
    if (numcards ==19) {window.localStorage.removeItem("card19");}
    if (numcards ==20) {window.localStorage.removeItem("card20");}
    if (numcards ==21) {window.localStorage.removeItem("card21");}
    if (numcards ==22) {window.localStorage.removeItem("card22");}
    if (numcards ==23) {window.localStorage.removeItem("card23");}
    if (numcards ==24) {window.localStorage.removeItem("card24");}
    if (numcards ==25) {window.localStorage.removeItem("card25");}
    if (numcards ==26) {window.localStorage.removeItem("card26");}
    if (numcards ==27) {window.localStorage.removeItem("card27");}
    if (numcards ==28) {window.localStorage.removeItem("card28");}
    if (numcards ==29) {window.localStorage.removeItem("card29");}
    if (numcards ==30) {window.localStorage.removeItem("card30");}
    if (numcards ==31) {window.localStorage.removeItem("card31");}
    if (numcards ==32) {window.localStorage.removeItem("card32");}

    // remove the card from the IDCard field
    if (numcards == 1) {document.getElementById("card01").innerHTML = "";}
    if (numcards == 2) {document.getElementById("card02").innerHTML = "";}
    if (numcards == 3) {document.getElementById("card03").innerHTML = "";}
    if (numcards == 4) {document.getElementById("card04").innerHTML = "";}
    if (numcards == 5) {document.getElementById("card05").innerHTML = "";}
    if (numcards == 6) {document.getElementById("card06").innerHTML = "";}
    if (numcards == 7) {document.getElementById("card07").innerHTML = "";}
    if (numcards == 8) {document.getElementById("card08").innerHTML = "";}
    if (numcards == 9) {document.getElementById("card09").innerHTML = "";}
    if (numcards ==10) {document.getElementById("card10").innerHTML = "";}
    if (numcards ==11) {document.getElementById("card11").innerHTML = "";}
    if (numcards ==12) {document.getElementById("card12").innerHTML = "";}
    if (numcards ==13) {document.getElementById("card13").innerHTML = "";}
    if (numcards ==14) {document.getElementById("card14").innerHTML = "";}
    if (numcards ==15) {document.getElementById("card15").innerHTML = "";}
    if (numcards ==16) {document.getElementById("card16").innerHTML = "";}
    if (numcards ==17) {document.getElementById("card17").innerHTML = "";}
    if (numcards ==18) {document.getElementById("card18").innerHTML = "";}
    if (numcards ==19) {document.getElementById("card19").innerHTML = "";}
    if (numcards ==20) {document.getElementById("card20").innerHTML = "";}
    if (numcards ==21) {document.getElementById("card21").innerHTML = "";}
    if (numcards ==22) {document.getElementById("card22").innerHTML = "";}
    if (numcards ==23) {document.getElementById("card23").innerHTML = "";}
    if (numcards ==24) {document.getElementById("card24").innerHTML = "";}
    if (numcards ==25) {document.getElementById("card25").innerHTML = "";}
    if (numcards ==26) {document.getElementById("card26").innerHTML = "";}
    if (numcards ==27) {document.getElementById("card27").innerHTML = "";}
    if (numcards ==28) {document.getElementById("card28").innerHTML = "";}
    if (numcards ==29) {document.getElementById("card29").innerHTML = "";}
    if (numcards ==30) {document.getElementById("card30").innerHTML = "";}
    if (numcards ==31) {document.getElementById("card31").innerHTML = "";}
    if (numcards ==32) {document.getElementById("card32").innerHTML = "";}
    
    // disable the button when there are no IDCards left
    numcards--;
    window.localStorage.setItem("myPrevidcards", String(numcards));
    if (numcards < 1) {  
      document.getElementById("previdcardsDelete").classList.remove(document.getElementById("previdcardsDelete").classList.item(0));
      document.getElementById("previdcardsDelete").disabled = true;
      document.getElementById("previdcardsDelete").classList.add("disabled_button");
    }
    s1 = "The IDCard with hash: "+ss+" is removed as one of your previous IDCards.";
    alert(s1);
  }
}  //feebbba78678dacccefbbb768762348792638475628374562eddac5626687685