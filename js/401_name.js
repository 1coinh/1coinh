
function OnStart()
{
  var name = window.localStorage.getItem("myName");
  if (name == null) {name = "";}
  document.getElementById("name").value = name;
  document.getElementById("name").focus();
  nameTyped();
}


/**
 * Name needs to consist of a minumum of 2 words, between 5 and 32 characters and no _
 * 
 */
function nameTyped() {
  var tt = 0;
  var err1 = false; // min 2 words
  var err2 = false; // between 5 and 32 char
  var err3 = false; // no _ characters

  var name = document.getElementById("name").value;
  if (name.length > 0) {
    // Remove faulty characters at start
    while ((name.length>0) && ((name.charCodeAt(0)==32) || (name.charCodeAt(0)==39) || (name.charCodeAt(0)==45))) {
      if (name.length>1) {
        name = name.substring(1, name.length);
      } else {
        name = "";
      }
    }

    // Remove all double or more spaces
    name.replace(/ {2,}/g, ' ');
    var nameArray = name.split(/(\s+)/);

    if (name.trim().length < 5 || name.trim().length > 32) {
      // To short or to long
      err2 = true;
    }

    if (nameArray.length < 2) {
      // Not at least 2 words
      err1 = true;
    } 

    if (!/^[a-zA-ZÆÐƎƏƐƔĲŊŒẞÞǷȜæðǝəɛɣĳŋœĸſßþƿȝĄƁÇĐƊĘĦĮƘŁØƠŞȘŢȚŦŲƯY̨Ƴąɓçđɗęħįƙłøơşșţțŧųưy̨ƴÁÀÂÄǍĂĀÃÅǺĄÆǼǢƁĆĊĈČÇĎḌĐƊÐÉÈĖÊËĚĔĒĘẸƎƏƐĠĜǦĞĢƔáàâäǎăāãåǻąæǽǣɓćċĉčçďḍđɗðéèėêëěĕēęẹǝəɛġĝǧğģɣĤḤĦIÍÌİÎÏǏĬĪĨĮỊĲĴĶƘĹĻŁĽĿʼNŃN̈ŇÑŅŊÓÒÔÖǑŎŌÕŐỌØǾƠŒĥḥħıíìiîïǐĭīĩįịĳĵķƙĸĺļłľŀŉńn̈ňñņŋóòôöǒŏōõőọøǿơœŔŘŖŚŜŠŞȘṢẞŤŢṬŦÞÚÙÛÜǓŬŪŨŰŮŲỤƯẂẀŴẄǷÝỲŶŸȲỸƳŹŻŽẒŕřŗſśŝšşșṣßťţṭŧþúùûüǔŭūũűůųụưẃẁŵẅƿýỳŷÿȳỹƴźżžẓ\s-,.\']+$/.test(name)) {
      // Not valid characters
      err3 = true;
    }
  }
  
  tt = name.length;
  document.getElementById("mySpan2").innerHTML = "between 5 and 32 characters: ["+String(tt)+"]";
  document.getElementById("name").value = name;
  document.getElementById("mySpan1").style.color = "green";
  document.getElementById("mySpan2").style.color = "green";
  document.getElementById("mySpan3").innerHTML = "&nbsp;";
  if (err1) {
    document.getElementById("mySpan1").style.color = "red";
  }
  if (err2) {
    document.getElementById("mySpan2").style.color = "red";
  }
  if (err3) {
    document.getElementById("mySpan3").innerHTML = "Please remove invalid characters";
  }
  document.getElementById("nameContinue").classList.remove(document.getElementById("nameContinue").classList.item(0));
  if ((err1) || (err2) || (err3)) {
    document.getElementById("nameContinue").disabled = true;
    document.getElementById("nameContinue").classList.add("disabled_button");
  } else {
    document.getElementById("nameContinue").disabled = false;
    document.getElementById("nameContinue").classList.add("main_button");
  }
}

/**
 * 
 */
function nameEnter() {
  var name = document.getElementById("name").value;
  window.localStorage.setItem("myName", name);
  window.location.href = "400_idcard.html";
}
