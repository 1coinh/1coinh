/**
 * requires setButtons from ./utils.js,
 * this file should be included in the calling html file
 */

/* global setButtons */

/**
 * Gender enum
 * 
 * @enum {string}
 */
const Gender = {
  male: "Male",
  female: "Female",
  other: "Other"
};


/**
 * 
 */
function OnStart() { // eslint-disable-line no-unused-vars
  var elements = document.getElementsByName("radio");
  var gender = window.localStorage.getItem("myGender");

  for (var i=0, len=elements.length; i<len; ++i) {
    if (getGenderValue(elements[i].value) === gender) {
      elements[i].checked = true;
    }

    elements[i].onclick = function() {
      genderSelect();
    };
  }
  setButtons(gender, "genderContinue");
}


/**
 * User selected a gender
 */
function genderSelect() {
  var gender = document.querySelector("input[type='radio'][name='radio']:checked").value;
  setButtons(gender, "genderContinue");
}


/**
 * Save the chosen Gender to localStorage and
 * return to the ID card
 */
function genderEnter() { // eslint-disable-line no-unused-vars
  var gender = getGenderValue(document.querySelector("input[type='radio'][name='radio']:checked").value);

  if (gender) { 
    window.localStorage.setItem("myGender", gender);
  }
  window.location.href = "400_idcard.html";
}


/**
 * return the Gender value
 * @param {string} key
 * @return {string}
 */
function getGenderValue(key) {
  for (const lookup in Gender) {
    if (lookup === key) {
      return Gender[key];
    }
  }
}