/**
 * requires setIDCardButton from ./utils.js,
 * this file should be included in the calling html file
 */

/* global setIDCardButton */

var IDCardFile = "";

/**
 * 
 */
function OnStart() { // eslint-disable-line no-unused-vars
  setIDCardButton("myName", "Name", "btn_name", "icon-user");
  setIDCardButton("myBirthday", "Birthday", "btn_birthday", "icon-calendar");
  setIDCardButton("myGender", "Gender", "btn_gender", "icon-man-woman");
  setIDCardButton("myLength", "Length", "btn_length", "icon-shift");
  setIDCardButton("myHair", "Hair", "btn_hair", "icon-cloud");
  setIDCardButton("myLeftEye", "Left Eye", "btn_lefteye", "icon-eye");
  setIDCardButton("myRightEye", "Right Eye", "btn_righteye", "icon-eye");
  setIDCardButton("myFeatures", "Features", "btn_features", "icon-hipster");
  setIDCardButton("myPrevidcards", "Previous IDCards", "btn_prevcards", "icon-drawer");
  
  // See if the IDCard can be shown
  var currentHash = "";
  var s1 = window.localStorage.getItem("myCurrentHash") || "";
  if ((s1 != s1) || (s1 == "")) {
    currentHash = "";
    document.getElementById("IDCard2").src = "img/PhotoClear.png";
  } else {
    // There must be a IDCard present
    currentHash = s1;
    var s1 = window.localStorage.getItem("myFiles") || "";
    if (s1 != s1) {
      alert("ERROR: There is no IDCard found, this is strange because the hashcode of your IDCard is found, which should not be possible.");
    } else {
      IDCardFile = s1+".png";
      var IDCardImage = window.localStorage.getItem(IDCardFile);
      if (IDCardImage == null) {IDCardImage = "";}
      if (IDCardImage != IDCardImage) {
        alert("ERROR: There is no IDCard found, this is strange because the filename of your IDCard is found, which should not be possible.");
      } else {
        document.getElementById("IDCard2").src = IDCardImage; // is the Base64 code
      }
    }
  }
}

/**
 * Test if all the buttons are clicked
 */
function IDCardClick() {
  var err = false;
  var name = window.localStorage.getItem("myName");
  err = (err || (name == null));
  var birthday = window.localStorage.getItem("myBirthday");
  err = (err || (birthday == null));
  var gender = window.localStorage.getItem("myGender");
  err = (err || (gender == null));
  var long = window.localStorage.getItem("myLength");
  err = (err || (long == null));
  var hair = window.localStorage.getItem("myHair");
  err = (err || (hair == null));
  var lefteye = window.localStorage.getItem("myLeftEye");
  err = (err || (lefteye == null));
  var righteye = window.localStorage.getItem("myRightEye");
  err = (err || (righteye == null));
  var features = window.localStorage.getItem("myFeatures");
  err = (err || (features == null));
  var prevcards = window.localStorage.getItem("myPrevidcards");
  err = (err || (prevcards == null));
  if (err) {
    alert("Please first fill out all the info under the buttons above.");
  } else {
    window.location.href = "410_picture.html";
  }
}
