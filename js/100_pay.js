/**
 * base64 image
 * 
 * @type {string}
 */
var fileContent = "";


/**
 * blockchain text content
 * 
 * @type {string}
 */
var myBlockchainTXT = "";


/**
 * Function to run when loading a HTML document. The function should be called with:
 *   `<body onload="OnStart();">`
 * 
 * The function will try to load blockchain information from localStorage `myFiles`
 * and will alert the user to create and `IDCard` if no information can be retrieved.
 * 
 * If `myFiles`contains content which basically is the base of a filename,
 * the function will try to retrieve {myFiles}.txt and {myFile}.png item from
 * localStorage, checking if they contain values, warning the user if not or 
 * storing the value(s) in variable(s)
 */
function OnStart() { // eslint-disable-line no-unused-vars
  document.getElementById("loadtext").addEventListener("change", handleFile, false);
  var s1 = window.localStorage.getItem("myFiles") || ""; //to get the filename of your blockchain
  if ((s1 != s1) || (s1 == "")) {
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
  } else {
    var myBlockchain = s1 + ".txt";
    var myIDCard = s1 + ".png"; //base64 code of IDCard
    s1 = window.localStorage.getItem(myBlockchain) || "";
    var s2 = window.localStorage.getItem(myIDCard) || "";
    if ((s1 != s1) || (s1 == "") || (s2 != s2) || (s2 == "")) {
      myBlockchainTXT = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
    } else {
      myBlockchainTXT = s1;
      ss = s2 + ">" + s1;  // so the order is: IDCARD ">" BLOCKCHAIN 
    }
  }
}


/**
 * Function that can be added to the EventListener for the {HTMLElement} with ID `loadtext`
 * that will read a file as text from the given {HTMLElement}
 * 
 * @returns {boolean} always return `false`
 */
function handleFile() {
  var theFile = document.getElementById("loadtext");
  if (theFile.value.indexOf("proposal.txt") > -1) {
    if (typeof (FileReader) != "undefined") {
      //create html5 file reader object
      var myReader = new FileReader();
      // call filereader. onload function
      myReader.onload = function () {
        fileContent = myReader.result;
        processProposal();
      };
      //call file reader onload
      myReader.readAsText(theFile.files[0]);
    } else {
      alert("This browser does not support HTML5.");
    }
  } else {
    alert("Please upload a valid proposal.txt file.");
  }
  return false;
}


/**
 * Simulate a click on the `loadtext` HTMLElement
 *
 * @todo: this function is duplicated a lot, centralize it.
 */
function loadIt() { // eslint-disable-line no-unused-vars
  document.getElementById("loadtext").click();
}


/**
 * Process a proposal (@todo: Define what a proposal is)
 */
function processProposal() {
  var s1 = fileContent;
  var t0 = s1.indexOf("<");
  var ss = s1.substring(0, t0); // proposal part
  s1 = s1.substring(t0 + 1);
  t0 = s1.indexOf("~");
  var myBlockchain = s1.substring(0, t0); // blockchain part

  var s2 = myBlockchain.substring(0, 64);
  var s3 = myBlockchainTXT.substring(0, 64);
  if (s2 == s3) {
    alert("You can't read your own proposal.txt file. This file can only be used by others!");
    window.open("index.html", "_self");
  } else {
    var myIDCard = s1.substring(t0 + 1);
    window.localStorage.setItem("proposal", ss);

    // find the hash of the person between 3rd and 4th "|"
    s1 = ss; //s1 = 0|0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
    t0 = s1.indexOf("|");
    s1 = s1.substring(t0 + 1); //s1 = 0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
    t0 = s1.indexOf("|");
    s1 = s1.substring(t0 + 1); //s1 = Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
    t0 = s1.indexOf("|");
    s1 = s1.substring(t0 + 1, t0 + 65); //s1 = 2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4

    /** 
     * @todo here you could compare possible earlier versions of blockchains you have of the person 
     */
    var fname = s1 + ".txt";
    window.localStorage.setItem(fname, myBlockchain);
    fname = s1 + ".png";
    window.localStorage.setItem(fname, myIDCard);
    window.open("102_pay2.html", "_self");
  }
}


/**
 * Read the blockchain and create the file: "payment_id.txt" where both my IDCard and Blockchain info are included
 * The size of the IDCards is about 200K.
 * 
 * Probably you will do transactions with not more than 1000 people, which makes all IDCards together
 * a maximum of 200 MB, your Blockchain transactions are probably around 1KB per transaction. With
 * 1000 transactions a year, you get to about 10MB in 10 years, but the blockchains of others, you 
 * collect also increases to 10MB in 10 year, which ultimately (after 10 years) is 10GB.
 * 
 * So a 16GB mini-USB-stick should be able to be sufficient to backup your 1CoinH data for the next
 * 15 years.
 */
function downloadPayInfo() { // eslint-disable-line no-unused-vars
  var s1 = window.localStorage.getItem("myFiles") || "";

  if ((!s1) || (s1 === "")) {
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
  } else {
    var myBlockchain = s1 + ".txt";
    var myIDCard = s1 + ".png"; //base64 code of IDCard
    s1 = window.localStorage.getItem(myBlockchain) || "";
    var s2 = window.localStorage.getItem(myIDCard) || "";
    if ((!s1) || (s1 === "") || (!s2) || (s2 === "")) {
      alert("You don't have a personal blockchain and/or IDCard. Go to 'IDCard' to create both.");
    } else {
      var ss = s2 + ">" + s1;  // so the order is: IDCARD ">" BLOCKCHAIN

      /** @todo handle multiple IDCards, they need to be added after this string, each one starting with a "%" character */

      // see: https://stackoverflow.com/questions/3665115/how-to-create-a-file-in-memory-for-user-to-download-but-not-through-server
      const blob = new Blob([ss], { type: "text" });
      if (window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, "payment_id.txt");
      } else {
        const elem = window.document.createElement("a");
        elem.href = window.URL.createObjectURL(blob);
        elem.download = "payment_id.txt";
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
      }
    }
  }
}

