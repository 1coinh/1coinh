
function OnStart()
{
  var hair = window.localStorage.getItem("myHair");
  if (hair == null) {hair = "";}
  if (hair == "White") { document.getElementById("white").checked = true;}
  if (hair == "Blond") { document.getElementById("blond").checked = true;}
  if (hair == "Light-Blond") { document.getElementById("lightblond").checked = true;}
  if (hair == "Medium-Blond") { document.getElementById("medblond").checked = true;}
  if (hair == "Dark-Blond") { document.getElementById("darkblond").checked = true;}
  if (hair == "Golden-Blond") { document.getElementById("goldblond").checked = true;}
  if (hair == "Strawberry-Blond") { document.getElementById("strawberryblond").checked = true;}
  if (hair == "Ginger-Red") { document.getElementById("gingerred").checked = true;}
  if (hair == "Chesnut-Brown") { document.getElementById("chesnutbrown").checked = true;}
  if (hair == "Light-Chesnut") { document.getElementById("lightchesnut").checked = true;}
  if (hair == "Light-Brown") { document.getElementById("lightbrown").checked = true;}
  if (hair == "Medium-Brown") { document.getElementById("medbrown").checked = true;}
  if (hair == "Dark-Brown") { document.getElementById("darkbrown").checked = true;}
  if (hair == "Black") { document.getElementById("black").checked = true;}
  if (hair == "Grey") { document.getElementById("grey").checked = true;}
  if (hair == "Silver") { document.getElementById("silver").checked = true;}
  if (hair == "Titan") { document.getElementById("titan").checked = true;}
  if (hair == "Auburn") { document.getElementById("auburn").checked = true;}
  if (hair == "Copper") { document.getElementById("copper").checked = true;}
  if (hair == "No Hair") { document.getElementById("nohair").checked = true;}
  hairSelect();  
}


function hairSelect()
{
  var hair = "";
  if (document.getElementById("white").checked) {hair = "White";}
  if (document.getElementById("blond").checked) {hair = "Blond";}
  if (document.getElementById("lightblond").checked) {hair = "Light-Blond";}
  if (document.getElementById("medblond").checked) {hair = "Medium-Blond";}
  if (document.getElementById("darkblond").checked) {hair = "Dark-Blond";}
  if (document.getElementById("goldblond").checked) {hair = "Golden-Blond";}
  if (document.getElementById("strawberryblond").checked) {hair = "Strawberry-Blond";}
  if (document.getElementById("gingerred").checked) {hair = "Ginger-Red";}
  if (document.getElementById("chesnutbrown").checked) {hair = "Chesnut-Brown";}
  if (document.getElementById("lightchesnut").checked) {hair = "Light-Chesnut";}
  if (document.getElementById("lightbrown").checked) {hair = "Light-Brown";}
  if (document.getElementById("medbrown").checked) {hair = "Medium-Brown";}
  if (document.getElementById("darkbrown").checked) {hair = "Dark-Brown";}
  if (document.getElementById("black").checked) {hair = "Black";}
  if (document.getElementById("grey").checked) {hair = "Grey";}
  if (document.getElementById("silver").checked) {hair = "Silver";}
  if (document.getElementById("titan").checked) {hair = "Titan";}
  if (document.getElementById("auburn").checked) {hair = "Auburn";}
  if (document.getElementById("copper").checked) {hair = "Copper";}
  if (document.getElementById("nohair").checked) {hair = "No Hair";}
  document.getElementById("hairContinue").classList.remove(document.getElementById("hairContinue").classList.item(0));
  if (hair == "")
  {
    document.getElementById("hairContinue").disabled = true;
    document.getElementById("hairContinue").classList.add("disabled_button");
  }
  else
  {
    document.getElementById("hairContinue").disabled = false;
    document.getElementById("hairContinue").classList.add("main_button");
  }
}


function hairEnter()
{
  var hair = "";
  if (document.getElementById("white").checked) {hair = "White";}
  if (document.getElementById("blond").checked) {hair = "Blond";}
  if (document.getElementById("lightblond").checked) {hair = "Light-Blond";}
  if (document.getElementById("medblond").checked) {hair = "Medium-Blond";}
  if (document.getElementById("darkblond").checked) {hair = "Dark-Blond";}
  if (document.getElementById("goldblond").checked) {hair = "Golden-Blond";}
  if (document.getElementById("strawberryblond").checked) {hair = "Strawberry-Blond";}
  if (document.getElementById("gingerred").checked) {hair = "Ginger-Red";}
  if (document.getElementById("chesnutbrown").checked) {hair = "Chesnut-Brown";}
  if (document.getElementById("lightchesnut").checked) {hair = "Light-Chesnut";}
  if (document.getElementById("lightbrown").checked) {hair = "Light-Brown";}
  if (document.getElementById("medbrown").checked) {hair = "Medium-Brown";}
  if (document.getElementById("darkbrown").checked) {hair = "Dark-Brown";}
  if (document.getElementById("black").checked) {hair = "Black";}
  if (document.getElementById("grey").checked) {hair = "Grey";}
  if (document.getElementById("silver").checked) {hair = "Silver";}
  if (document.getElementById("titan").checked) {hair = "Titan";}
  if (document.getElementById("auburn").checked) {hair = "Auburn";}
  if (document.getElementById("copper").checked) {hair = "Copper";}
  if (document.getElementById("nohair").checked) {hair = "No Hair";}
  window.localStorage.setItem("myHair", hair);
  window.location.href = "400_idcard.html";
}