
var currencychar = "";

function OnStart()
{
  document.getElementById("loadtext").addEventListener("change", handleFile, false);
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  // //////////////////////////////
  // ///  read your blockchain  ///
  // //////////////////////////////
  // var myFiles = ""  //to get the filename of your blockchain
  // var read = "" //to read your blockchain
  // var s1 = window.localStorage.getItem('myFiles')
  // if (s1 == null) {s1 = ""}
  // if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  // {
  //   read = ""
  //   alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
  // }
  // else
  // {
  //   var myBlockchain = s1 + ".txt"
  //   var s1 = window.localStorage.getItem(myBlockchain)
  //   if (s1 == null) {s1 = ""}
  //   if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  //   {
  //     read = ""
  //     alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
  //   }
  //   else
  //   {
  //     read = s1;
  //     document.getElementById("bc").value = myBlockchain + "***" + read
  //   }
  // }
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  var s1 = window.localStorage.getItem("myCurrencyChar");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    document.getElementById("default").checked = true;
    window.localStorage.setItem("myCurrencyChar","ᕫ");
    currencyChar = "ᕫ";
  }
  else
  {
    if (s1 == "ᕫ")
    {
      document.getElementById("default").checked = true;
      currencyChar = "ᕫ";
    }
    else
    {
      document.getElementById("alternative").checked = true;
      currencyChar = "ᕫ";
    }
  }
}


function defaultSelect()
{
  currencyChar = "ᕫ";
  window.localStorage.setItem("myCurrencyChar","ᕫ");
}


function alternativeSelect()
{
  currencyChar = "U";
  window.localStorage.setItem("myCurrencyChar","U");
}



function clearStorage()
{
  if (window.confirm("Are you sure to erase all your personal 1CoinH local data?")) 
  {
    localStorage.clear();
    alert("All your personal 1CoinH local data is erased!");
  }
}


function backup()
{
  //////////////////////////////
  ///  read your blockchain  ///
  //////////////////////////////
  var myFiles = "";  //to get the filename of your blockchain
  var ss = "";
  var s1 = "";
  var s2 = "";
  s1 = window.localStorage.getItem("myFiles");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    read = "";
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
  }
  else
  {
    if (window.confirm("Are you sure to backup your personal 1CoinH local data?")) 
    {
      myFiles = s1;
      var err = false;
      var myBlockchain = s1 + ".txt";
      var myIDCard = s1 + ".png"; //base64 code of IDCard
      var sbc = window.localStorage.getItem(myBlockchain);
      if ((sbc != sbc) || (sbc == null)) {err = true;}
      var sid = window.localStorage.getItem(myIDCard);
      if ((sid != sid) || (sid == null)) {err = true;}
      var shair = window.localStorage.getItem("myHair");
      if ((shair != shair) || (shair == null)) {err = true;}
      var sfeatures = window.localStorage.getItem("myFeatures");
      if ((sfeatures != sfeatures) || (sfeatures == null)) {err = true;}
      var sname = window.localStorage.getItem("myName");
      if ((sname != sname) || (sname == null)) {err = true;}
      var sgender = window.localStorage.getItem("myGender");
      if ((sgender != sgender) || (sgender == null)) {err = true;}
      var sreye = window.localStorage.getItem("myRightEye");
      if ((sreye != sreye) || (sreye == null)) {err = true;}
      var sleye = window.localStorage.getItem("myLeftEye");
      if ((sleye != sleye) || (sleye == null)) {err = true;}
      var sbirthday = window.localStorage.getItem("myBirthday");
      if ((sbirthday != sbirthday) || (sbirthday == null)) {err = true;}
      var scurchar = window.localStorage.getItem("myCurrencyChar");
      if ((scurchar != scurchar) || (scurchar == null)) {err = true;}
      var slen = window.localStorage.getItem("myLength");
      if ((slen != slen) || (slen == null)) {err = true;}
      var sprid = window.localStorage.getItem("myPrevidcards");
      if ((sprid != sprid) || (sprid == null)) {err = true;}
      var scard01 = "----------------------------------------------------------------";
      var scard02 = "----------------------------------------------------------------";
      var scard03 = "----------------------------------------------------------------";
      var scard04 = "----------------------------------------------------------------";
      var scard05 = "----------------------------------------------------------------";
      var scard06 = "----------------------------------------------------------------";
      var scard07 = "----------------------------------------------------------------";
      var scard08 = "----------------------------------------------------------------";
      var scard09 = "----------------------------------------------------------------";
      var scard10 = "----------------------------------------------------------------";
      var scard11 = "----------------------------------------------------------------";
      var scard12 = "----------------------------------------------------------------";
      var scard13 = "----------------------------------------------------------------";
      var scard14 = "----------------------------------------------------------------";
      var scard15 = "----------------------------------------------------------------";
      var scard16 = "----------------------------------------------------------------";
      var scard17 = "----------------------------------------------------------------";
      var scard18 = "----------------------------------------------------------------";
      var scard19 = "----------------------------------------------------------------";
      var scard20 = "----------------------------------------------------------------";
      var scard21 = "----------------------------------------------------------------";
      var scard22 = "----------------------------------------------------------------";
      var scard23 = "----------------------------------------------------------------";
      var scard24 = "----------------------------------------------------------------";
      var scard25 = "----------------------------------------------------------------";
      var scard26 = "----------------------------------------------------------------";
      var scard27 = "----------------------------------------------------------------";
      var scard28 = "----------------------------------------------------------------";
      var scard29 = "----------------------------------------------------------------";
      var scard30 = "----------------------------------------------------------------";
      var scard31 = "----------------------------------------------------------------";
      var scard32 = "----------------------------------------------------------------";
      if ((err == false) && (sprid != "0"))
      {
        var tt = Number(sprid);
        var ssi = "";
        var i = 0;
        if (tt>0)
        {
          for(i = 0; i < tt; i++) 
          {
            ssi = String(i);
            if (i<10) {ss1 = "0" + ss1;}
            ssi = "card" + ssi;
            if (i ==  1) {scard01 = ss1;}
            if (i ==  2) {scard02 = ss1;}
            if (i ==  3) {scard03 = ss1;}
            if (i ==  4) {scard04 = ss1;}
            if (i ==  5) {scard05 = ss1;}
            if (i ==  6) {scard06 = ss1;}
            if (i ==  7) {scard07 = ss1;}
            if (i ==  8) {scard08 = ss1;}
            if (i ==  9) {scard09 = ss1;}
            if (i == 10) {scard10 = ss1;}
            if (i == 11) {scard11 = ss1;}
            if (i == 12) {scard12 = ss1;}
            if (i == 13) {scard13 = ss1;}
            if (i == 14) {scard14 = ss1;}
            if (i == 15) {scard15 = ss1;}
            if (i == 16) {scard16 = ss1;}
            if (i == 17) {scard17 = ss1;}
            if (i == 18) {scard18 = ss1;}
            if (i == 19) {scard19 = ss1;}
            if (i == 20) {scard20 = ss1;}
            if (i == 21) {scard21 = ss1;}
            if (i == 22) {scard22 = ss1;}
            if (i == 23) {scard23 = ss1;}
            if (i == 24) {scard24 = ss1;}
            if (i == 25) {scard25 = ss1;}
            if (i == 26) {scard26 = ss1;}
            if (i == 27) {scard27 = ss1;}
            if (i == 28) {scard28 = ss1;}
            if (i == 29) {scard29 = ss1;}
            if (i == 30) {scard30 = ss1;}
            if (i == 31) {scard31 = ss1;}
            if (i == 32) {scard32 = ss1;}
          }
        }
      }
      var scurh = window.localStorage.getItem("myCurrentHash");
      if ((scurh != scurh) || (scurh == null)) {err = true;}
      var sidpic = window.localStorage.getItem("IDCardPicture");
      if ((sidpic != sidpic) || (sidpic == null)) {err = true;}
      if (err) //catch a NaN issue
      {
        alert("You don't have a personal blockchain and/or IDCard. Go to 'IDCard' to create both.");
      }
      else
      {
        ////////////////////////////////
        ///  create the backup file  ///
        ////////////////////////////////
        ss = "<<myFiles>>" + myFiles;
        ss = ss + "<<myBlockchain>>" + myBlockchain + "<<>>" + sbc;  
        ss = ss + "<<myIDCard>>" + myIDCard + "<<>>" + sid;
        ss = ss + "<<myName>>" + sname;   
        ss = ss + "<<myBirthday>>" + sbirthday;   
        ss = ss + "<<myGender>>" + sgender;   
        ss = ss + "<<myLength>>" + slen;   
        ss = ss + "<<myHair>>" + shair;   
        ss = ss + "<<myLeftEye>>" + sleye;   
        ss = ss + "<<myRightEye>>" + sreye;   
        ss = ss + "<<myFeatures>>" + sfeatures;   
        ss = ss + "<<myCurrencyChar>>" + scurchar;   
        ss = ss + "<<myPrevidcards>>" +  sprid;  
        ss = ss + "<<card01>>" + scard01;
        ss = ss + "<<card02>>" + scard02;
        ss = ss + "<<card03>>" + scard03;
        ss = ss + "<<card04>>" + scard04;
        ss = ss + "<<card05>>" + scard05;
        ss = ss + "<<card06>>" + scard06;
        ss = ss + "<<card07>>" + scard07;
        ss = ss + "<<card08>>" + scard08;
        ss = ss + "<<card09>>" + scard09;
        ss = ss + "<<card10>>" + scard10;
        ss = ss + "<<card11>>" + scard11;
        ss = ss + "<<card12>>" + scard12;
        ss = ss + "<<card13>>" + scard13;
        ss = ss + "<<card14>>" + scard14;
        ss = ss + "<<card15>>" + scard15;
        ss = ss + "<<card16>>" + scard16;
        ss = ss + "<<card17>>" + scard17;
        ss = ss + "<<card18>>" + scard18;
        ss = ss + "<<card19>>" + scard19;
        ss = ss + "<<card20>>" + scard20;
        ss = ss + "<<card21>>" + scard21;
        ss = ss + "<<card22>>" + scard22;
        ss = ss + "<<card23>>" + scard23;
        ss = ss + "<<card24>>" + scard24;
        ss = ss + "<<card25>>" + scard25;
        ss = ss + "<<card26>>" + scard26;
        ss = ss + "<<card27>>" + scard27;
        ss = ss + "<<card28>>" + scard28;
        ss = ss + "<<card29>>" + scard29;
        ss = ss + "<<card30>>" + scard30;
        ss = ss + "<<card31>>" + scard31;
        ss = ss + "<<card32>>" + scard32;
        ss = ss + "<<myCurrentHash>>" + scurh;   
        ss = ss + "<<IDCardPicture>>" + sidpic;
        ///////////////////////////////////////////////////////////////////////////////////
        ///  Now we read all different hashes (transactionpartners) from the blokchain  ///
        ///////////////////////////////////////////////////////////////////////////////////

        var relations = ""; //collects unique IDCode hashes
        var read = sbc; //blockchain
        //////////////////////////////
        /// remove the first part  ///
        //////////////////////////////
        var t1 = read.indexOf("<") + 1;
        read = read.substring(t1);
        //////////////////////////////////////////
        ///  count the number of transactions  ///
        //////////////////////////////////////////
        var t2 = (read.match(/</g) || []).length;
        var readx = "";
        var s0 = "";
        var s2 = "";
        var s3 = "";
        var s4 = "";
        var sss = "";
        var ss1 = "";
        var ss2 = "";
        var ss3 = "";
        var ss4 = "";
        var ss5 = "";
        var ssr = "";
        var xr0 = 0;
        var xr1 = 0;
        var xr2 = 0;
        var xr3 = 0;
        var xr4 = 0;
        var xr5 = 0;
        var tx = 0;
        var t0 = 0;
        var delta = 0;
        var fact = 0;
        var others2 = 0;
        var endhash = "";
        if (t2 > 0)  //there is at least 1 transaction
        {
          for (var i = 0; i < t2; i++)
          {
            t1 = read.indexOf("<");
            readx = read.substring(0,t1+1); //including "<"
            //////////////////////////////////////////
            ///  get remaining part of blockchain  ///
            ///  to be used in the next run        ///
            //////////////////////////////////////////
            if (read.length>(t1+3)) //string is not finished
            {
              read = read.substring(t1+1); //to be used in next for loop cycle
            }
            //////////////////////////////////////////
            readx = readx.trim();
            s0 = readx.substring(0,1);     //can be "R" be "S"
            s1 = readx.substring(1,65);  //must be person that receives the coin
            if (relations.length > 10)
            {
              if (relations.indexOf(s1) == -1)
              {
                relations = relations + s1;
              }
            }
            else
            {
              relations = s1;
            }
            sss = readx.substring(66); 
            t0 = sss.indexOf("|");
            s2 = sss.substring(0,t0);          //timestamp
            sss = sss.substring(t0+1);      
            t0 = sss.indexOf("|");
            s3 = sss.substring(0,t0);          //securityhash
            sss = sss.substring(t0+1);      
            t0 = sss.indexOf("|");
            ssr = sss.substring(0,t0);          //rr0
            xr0 = Number(ssr);
            others2 = 0;
            sss = sss.substring(t0+1);      
            t0 = sss.indexOf("|");
            endhash = "";
            ssr = "";
            if (t0 > -1)
            {
              others2 = 1;
              ss1 = sss.substring(0,t0); //hash1
              if (relations.indexOf(ss1) > -1)
              {
                relations = relations + ss1;
              }
              sss = sss.substring(t0+1);      
              t0 = sss.indexOf("|");
              ssr = sss.substring(0,t0); //rr1
              xr1 = Number(ssr);
              sss = sss.substring(t0+1); 
              t0 = sss.indexOf("|");
              if (t0 > -1)
              {
                others2 = 2;
                ss2 = sss.substring(0,t0); //hash2
                if (relations.indexOf(ss2) > -1)
                {
                  relations = relations + ss2;
                }
                sss = sss.substring(t0+1);      
                t0 = sss.indexOf("|");
                ssr = sss.substring(0,t0); //rr2
                xr2 = Number(ssr);
                sss = sss.substring(t0+1); 
                t0 = sss.indexOf("|");
                if (t0 > -1)
                {
                  others2 = 3;
                  var ss3 = sss.substring(0,t0); //hash3
                  if (relations.indexOf(ss3) > -1)
                  {
                    relations = relations + ss3;
                  }
                  sss = sss.substring(t0+1);      
                  t0 = sss.indexOf("|");
                  ssr = sss.substring(0,t0); //rr3
                  xr3 = Number(ssr);
                  sss = sss.substring(t0+1); 
                  t0 = sss.indexOf("|");
                  if (t0 > -1)
                  {
                    others2 = 4;
                    var ss4 = sss.substring(0,t0); //hash4
                    if (relations.indexOf(ss4) > -1)
                    {
                      relations = relations + ss4;
                    }
                    sss = sss.substring(t0+1);      
                    t0 = sss.indexOf("|");
                    ssr = sss.substring(0,t0); //rr4
                    xr4 = Number(ssr);
                    sss = sss.substring(t0+1); 
                    t0 = sss.indexOf("|");
                    if (t0 > -1)
                    {
                      others2 = 5;
                      var ss5 = sss.substring(0,t0); //hash5
                      if (relations.indexOf(ss5) > -1)
                      {
                        relations = relations + ss5;
                      }
                      sss = sss.substring(t0+1);      
                      t0 = sss.indexOf("|");
                      ssr = sss.substring(0,t0); //rr5
                      xr5 = Number(ssr);
                      sss = sss.substring(t0+1); 
                      t0 = sss.indexOf("|");
                    }
                  }
                }
              }
            }
            //////////////////////
            ///  end for loop  ///
            //////////////////////
          }
          t0 = Math.round((relations.length)/64);
          for (var i = 0; i < t0; i++)
          {
            ss1 = relations.substring((64*i),(64*(i+1)));
            ss2 = ss1 + ".txt";
            ss3 = ss1 + ".png";
            ss4 = window.localStorage.getItem(ss2);
            if ((ss4 != ss4) || (ss4 == null)) 
            {
              err = true;
            }
            else
            {
              ss = ss + "<<"+ss2+">>" + ss4;
            } 
            ss5 = window.localStorage.getItem(ss3);
            if ((ss5 != ss5) || (ss5 == null)) 
            {
              err = true;
            }
            else
            {
              ss = ss + "<<"+ss3+">>" + ss5;
            } 
          }
        }
        // OLD!!
        // var element = document.createElement('a');
        // element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(ss));
        // element.setAttribute('download', "backup.txt");
        // element.style.display = 'none';
        // document.body.appendChild(element);
        // element.click();
        // document.body.removeChild(element);
        //////////////////////////////////////////////////
        //https://stackoverflow.com/questions/3665115/how-to-create-a-file-in-memory-for-user-to-download-but-not-through-server
        //////////////////////////////////////////////////
        const blob = new Blob([ss], {type: "text"});  /// /csv
        if(window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveBlob(blob, "backup.txt");
        }
        else{
          const elem = window.document.createElement("a");
          elem.href = window.URL.createObjectURL(blob);
          elem.download = "backup.txt";        
          document.body.appendChild(elem);
          elem.click();        
          document.body.removeChild(elem);
        }
      }
    }
  }
}


function loadIt()
{
  document.getElementById("loadtext").click();
}


function handleFile(e) 
{
  var fileSize = 0;
  var theFile = document.getElementById("loadtext"); 
  if (typeof (FileReader) != "undefined") 
  {
    //get table element
    //var table = document.getElementById("myTable");
    var headerLine = "";
    //create html5 file reader object
    var myReader = new FileReader();
    // call filereader. onload function
    myReader.onload = function(e) 
    {
      fileContent = myReader.result;
      restore();
    };
    //call file reader onload
    myReader.readAsText(theFile.files[0]);
  }
  else 
  {
    alert("This browser does not support HTML5.");
  }
  return false;
}


function restore()
{
  var ss = fileContent;
  var sm = "Are you sure to restore your personal 1CoinH local 'backup.txt' data? "+
          "Remember that your backup MUST be fully up-to-date! If it is not FULLY "+
          "up-to-date, others will view your blockchain as fraudulent and might not "+
          "accept your coins. If you don't have a recent backup, you need to get your blockchain "+
          "from the last person you did a transaction with, and you need to find the "+
          "blockchain and IDCards of all persons you did transactions with since your "+
          "last backup before you proceed! Do you want to proceed?";
  if (window.confirm(sm)) 
  {
    var s1 = "";
    var t0 = 0;
    var t1 = 0;
    /////////////////////////
    t0 = ss.indexOf("<<myFiles>>") + 11;
    if (t0>-1)
    {
      t1 = ss.indexOf("<<myBlockchain>>");
      var myFiles = ss.substring(t0,t1);
      window.localStorage.setItem("myFiles",myFiles);
      //alert("myFiles: "+myFiles)
      /////////////////////////
      ss = ss.substring(t1+16);
      t1 = ss.indexOf("<<>>");
      var myBlockchain =  ss.substring(0,t1);
      //alert("myBlockchain: "+myBlockchain)
      /////////////////////////
      ss = ss.substring(t1+4);
      t1 = ss.indexOf("<<myIDCard>>");
      var myBlockchain2 =  ss.substring(0,t1);
      window.localStorage.setItem(myBlockchain,myBlockchain2);
      //alert("myBlockchain2: "+myBlockchain2)
      /////////////////////////
      ss = ss.substring(t1+12);
      t1 = ss.indexOf("<<>>");
      var myIDCard =  ss.substring(0,t1);
      //alert("myIDCard: "+myIDCard)
      /////////////////////////
      ss = ss.substring(t1+4);
      t1 = ss.indexOf("<<myName>>");
      var myIDCard2 =  ss.substring(0,t1);
      window.localStorage.setItem(myIDCard,myIDCard2);
      //alert("myIDCard2: "+myIDCard2)
      /////////////////////////
      ss = ss.substring(t1+10);
      t1 = ss.indexOf("<<myBirthday>>");
      var myName =  ss.substring(0,t1);
      window.localStorage.setItem("myName",myName);
      //alert("myName: "+myName)
      /////////////////////////
      ss = ss.substring(t1+14);
      t1 = ss.indexOf("<<myGender>>");
      var myBirthday =  ss.substring(0,t1);
      window.localStorage.setItem("myBirthday",myBirthday);
      //alert("myBirthday: "+myBirthday)
      /////////////////////////
      ss = ss.substring(t1+12);
      t1 = ss.indexOf("<<myLength>>");
      var myGender =  ss.substring(0,t1);
      window.localStorage.setItem("myGender",myGender);
      //alert("myGender: "+myGender)
      /////////////////////////
      ss = ss.substring(t1+12);
      t1 = ss.indexOf("<<myHair>>");
      var myLength =  ss.substring(0,t1);
      window.localStorage.setItem("myLength",myLength);
      //alert("myLength: "+myLength)
      /////////////////////////
      ss = ss.substring(t1+10);
      t1 = ss.indexOf("<<myLeftEye>>");
      var myHair =  ss.substring(0,t1);
      window.localStorage.setItem("myHair",myHair);
      //alert("myHair: "+myHair)
      /////////////////////////
      ss = ss.substring(t1+13);
      t1 = ss.indexOf("<<myRightEye>>");
      var myLeftEye =  ss.substring(0,t1);
      window.localStorage.setItem("myLeftEye",myLeftEye);
      //alert("myLeftEye: "+myLeftEye)
      /////////////////////////
      ss = ss.substring(t1+14);
      t1 = ss.indexOf("<<myFeatures>>");
      var myRightEye =  ss.substring(0,t1);
      window.localStorage.setItem("myRightEye",myRightEye);
      //alert("myRightEye: "+myRightEye)
      /////////////////////////
      ss = ss.substring(t1+14);
      t1 = ss.indexOf("<<myCurrencyChar>>");
      var myFeatures =  ss.substring(0,t1);
      window.localStorage.setItem("myFeatures",myFeatures);
      //alert("myFeatures: "+myFeatures)
      /////////////////////////
      ss = ss.substring(t1+18);
      t1 = ss.indexOf("<<myPrevidcards>>");
      var myCurrencyChar =  ss.substring(0,t1);
      window.localStorage.setItem("myCurrencyChar",myCurrencyChar);
      //alert("myCurrencyChar: "+myCurrencyChar)
      /////////////////////////
      ss = ss.substring(t1+17);
      t1 = ss.indexOf("<<card01>>");
      var myPrevidcards =  ss.substring(0,t1);
      window.localStorage.setItem("myPrevidcards",myPrevidcards);
      //alert("myPrevidcards: "+myPrevidcards)
      /////////////////////////
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card02>>");var card01 =  ss.substring(0,t1);//;alert("card01: "+card01)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card03>>");var card02 =  ss.substring(0,t1);//;alert("card02: "+card02)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card04>>");var card03 =  ss.substring(0,t1);//;alert("card03: "+card03)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card05>>");var card04 =  ss.substring(0,t1);//;alert("card04: "+card04)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card06>>");var card05 =  ss.substring(0,t1);//;alert("card05: "+card05)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card07>>");var card06 =  ss.substring(0,t1);//;alert("card06: "+card06)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card08>>");var card07 =  ss.substring(0,t1);//;alert("card07: "+card07)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card09>>");var card08 =  ss.substring(0,t1);//;alert("card08: "+card08)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card10>>");var card09 =  ss.substring(0,t1);//;alert("card09: "+card09)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card11>>");var card10 =  ss.substring(0,t1);//;alert("card10: "+card10)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card12>>");var card11 =  ss.substring(0,t1);//;alert("card11: "+card11)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card13>>");var card12 =  ss.substring(0,t1);//;alert("card12: "+card12)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card14>>");var card13 =  ss.substring(0,t1);//;alert("card13: "+card13)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card15>>");var card14 =  ss.substring(0,t1);//;alert("card14: "+card14)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card16>>");var card15 =  ss.substring(0,t1);//;alert("card15: "+card15)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card17>>");var card16 =  ss.substring(0,t1);//;alert("card16: "+card16)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card18>>");var card17 =  ss.substring(0,t1);//;alert("card17: "+card17)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card19>>");var card18 =  ss.substring(0,t1);//;alert("card18: "+card18)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card20>>");var card19 =  ss.substring(0,t1);//;alert("card19: "+card19)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card21>>");var card20 =  ss.substring(0,t1);//;alert("card20: "+card20)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card22>>");var card21 =  ss.substring(0,t1);//;alert("card21: "+card21)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card23>>");var card22 =  ss.substring(0,t1);//;alert("card22: "+card22)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card24>>");var card23 =  ss.substring(0,t1);//;alert("card23: "+card23)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card25>>");var card24 =  ss.substring(0,t1);//;alert("card24: "+card24)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card26>>");var card25 =  ss.substring(0,t1);//;alert("card25: "+card25)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card27>>");var card26 =  ss.substring(0,t1);//;alert("card26: "+card26)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card28>>");var card27 =  ss.substring(0,t1);//;alert("card27: "+card27)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card29>>");var card28 =  ss.substring(0,t1);//;alert("card28: "+card28)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card30>>");var card29 =  ss.substring(0,t1);//;alert("card29: "+card29)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card31>>");var card30 =  ss.substring(0,t1);//;alert("card30: "+card30)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<card32>>");var card31 =  ss.substring(0,t1);//;alert("card31: "+card31)
      ss = ss.substring(t1+10);t1 = ss.indexOf("<<myCurrentHash>>");var card32 =  ss.substring(0,t1);//;alert("card32: "+card32)
      /////////////////////////
      var tx = Number(myPrevidcards);
      if (tx >  0) {window.localStorage.setItem("card01",card01);}
      if (tx >  1) {window.localStorage.setItem("card02",card02);}
      if (tx >  2) {window.localStorage.setItem("card03",card03);}
      if (tx >  3) {window.localStorage.setItem("card04",card04);}
      if (tx >  4) {window.localStorage.setItem("card05",card05);}
      if (tx >  5) {window.localStorage.setItem("card06",card06);}
      if (tx >  6) {window.localStorage.setItem("card07",card07);}
      if (tx >  7) {window.localStorage.setItem("card08",card08);}
      if (tx >  8) {window.localStorage.setItem("card09",card09);}
      if (tx >  9) {window.localStorage.setItem("card10",card10);}
      if (tx > 10) {window.localStorage.setItem("card11",card11);}
      if (tx > 11) {window.localStorage.setItem("card12",card12);}
      if (tx > 12) {window.localStorage.setItem("card13",card13);}
      if (tx > 13) {window.localStorage.setItem("card14",card14);}
      if (tx > 14) {window.localStorage.setItem("card15",card15);}
      if (tx > 15) {window.localStorage.setItem("card16",card16);}
      if (tx > 16) {window.localStorage.setItem("card17",card17);}
      if (tx > 17) {window.localStorage.setItem("card18",card18);}
      if (tx > 18) {window.localStorage.setItem("card19",card19);}
      if (tx > 19) {window.localStorage.setItem("card20",card20);}
      if (tx > 20) {window.localStorage.setItem("card21",card21);}
      if (tx > 21) {window.localStorage.setItem("card22",card22);}
      if (tx > 22) {window.localStorage.setItem("card23",card23);}
      if (tx > 23) {window.localStorage.setItem("card24",card24);}
      if (tx > 24) {window.localStorage.setItem("card25",card25);}
      if (tx > 25) {window.localStorage.setItem("card26",card26);}
      if (tx > 26) {window.localStorage.setItem("card27",card27);}
      if (tx > 27) {window.localStorage.setItem("card28",card28);}
      if (tx > 28) {window.localStorage.setItem("card29",card29);}
      if (tx > 29) {window.localStorage.setItem("card30",card30);}
      if (tx > 30) {window.localStorage.setItem("card31",card31);}
      if (tx > 31) {window.localStorage.setItem("card32",card32);}
      /////////////////////////
      ss = ss.substring(t1+17);
      t1 = ss.indexOf("<<IDCardPicture>>");
      var myCurrentHash =  ss.substring(0,t1);
      window.localStorage.setItem("myCurrentHash",myCurrentHash);
      //alert("myCurrentHash: "+myCurrentHash)
      /////////////////////////
      ss = ss.substring(t1+17);
      t1 = ss.indexOf("<<");
      if (t1>-1)
      {
        var IDCardPicture =  ss.substring(0,t1);
      }
      else
      {
        var IDCardPicture =  ss;
      }
      window.localStorage.setItem("IDCardPicture",IDCardPicture);
      //alert("IDCardPicture: "+IDCardPicture)
      /////////////////////////
      if (t1>-1)
      {
        var ss1 = "";
        var ss2 = "";
        var ss3 = "";
        var ss4 = "";
        var t2 = t1;
        while (t1>-1)
        {
          ss = ss.substring(t2+2);  //ss = hash.txt>>......
          t2 = ss.indexOf(">>");
          ss1 =  ss.substring(0,t2);  
          ss = ss.substring(t2+2);  //ss = blockchain< <<hash.png>>......
          t2 = ss.indexOf("<<<");
          ss2 =  ss.substring(0,t2); //ss = hash.png>>...
          ss = ss.substring(t2+3);
          t2 = ss.indexOf(">>");
          ss3 =  ss.substring(0,t2); //ss = idcard<<next hash.txt>>...
          ss = ss.substring(t2+2);
          t2 = ss.indexOf("<<");
          if (t2>-1)
          {
            ss4 =  ss.substring(0,t2);
          }
          else
          {
            ss4 = ss;
          }
          window.localStorage.setItem(ss1,ss2);
          window.localStorage.setItem(ss3,ss4);
          //alert(ss1+": "+ss2)
          //alert(ss3+": "+ss4)
          t1 = t2;
        }
      }
      alert("Your data is restored!");
    }
    else
    {
      alert("There is no blockchain found! The file is wrong others2 corrupt or you need to create an IDCard first.");
    }
  }

}