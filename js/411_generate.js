/**
 * requires SHA256 from ./hash.js,
 * this file should be included in the calling html file
 */

/* global SHA256 */
/////////////////////////
///  Set global vars  ///
/////////////////////////
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var imageWidth = 0;
var imageHeight = 0;
var imb1 = false;
var image1 = new Image;
//image1.crossOrigin="anonymous"
var image = ctx.createImageData(1, 1); // pixel image
var lg = "";
var data = image.data;
var colors = [
  { r: 0, g: 0, b: 0, a: 255 }, // black   0
  { r: 0, g: 255, b: 0, a: 255 }, // lime    1
  { r: 0, g: 0, b: 255, a: 255 }, // blue    2
  { r: 0, g: 255, b: 255, a: 255 }, // aqua    3
  { r: 255, g: 0, b: 0, a: 255 }, // red     4
  { r: 255, g: 255, b: 0, a: 255 }, // yellow  5
  { r: 255, g: 0, b: 255, a: 255 }, // magenta 6
  { r: 255, g: 255, b: 255, a: 255 }, // white   7
  { r: 0, g: 127, b: 0, a: 255 }, // green   8
  { r: 0, g: 0, b: 127, a: 255 }, // navy    9
  { r: 0, g: 127, b: 127, a: 255 }, // teal   10
  { r: 127, g: 0, b: 0, a: 255 }, // maroon 11
  { r: 127, g: 127, b: 0, a: 255 }, // olive  12
  { r: 127, g: 0, b: 127, a: 255 }, // purple 13
  { r: 127, g: 127, b: 127, a: 255 }, // gray   14
  { r: 255, g: 127, b: 127, a: 255 }, // pink?  15
];
var img4 = new Image();
var hc = 0;   //hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time). The counter does not need to look at daylight saving time or time zones
var hcd = 13; //day
var hcm = 2; //month
var hcy = 2035; //year
var hch = 11; //hour
var hcmin = 0; //minutes
var hcsec = 0; //seconds
var hcmsec = 0; //milliseconds
var bdd = 13;   //birthday day
var bmm = 2;    //birthday month
var byy = 1965; //birthday year
var bhh = 0;    //birthday hour
var MyBhh = 0;  //my birthday hour
var timer = ""; //string to represent the progress in time in the DATASET

function getBirthdayHour() {
  ///////////////////////////////////////////////////////  
  // based on the vars bdd,bmm and byy                 //
  // the hour count in bhh is calculated from 1900     // 
  // and then altered to 1-1-2022                      //
  // the exact hour of birth is not important and =0   //
  ///////////////////////////////////////////////////////

  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((byy - 1897) / 4);
  if (((Math.trunc((byy - 1900) / 4)) == ((byy - 1900) / 4)) && (bmm > 2)) { t1++; }
  t1 = bdd + t1 + (365 * (byy - 1900));  //23755
  if (bmm > 1) { t1 = t1 + 31; }
  if (bmm > 2) { t1 = t1 + 28; }
  if (bmm > 3) { t1 = t1 + 31; }
  if (bmm > 4) { t1 = t1 + 30; }
  if (bmm > 5) { t1 = t1 + 31; }
  if (bmm > 6) { t1 = t1 + 30; }
  if (bmm > 7) { t1 = t1 + 31; }
  if (bmm > 8) { t1 = t1 + 31; }
  if (bmm > 9) { t1 = t1 + 30; }
  if (bmm > 10) { t1 = t1 + 31; }
  if (bmm > 11) { t1 = t1 + 30; }
  bhh = (24 * (t1 - 1)) + 1; //01-01-1900 =1  01-01-2022=1069464   
  bhh = bhh - 1069464;   //43830+365+366 days or 1069464 hours between 01-01-1900 and 01-01-2022
}


function use() {
  /////////////////////////////////////
  ///  USE! (save IDCard and exit)  ///
  ///////////////////////////////////////////
  ///  First we get the needed variables  ///
  ///////////////////////////////////////////
  var s1 = window.localStorage.getItem("myPrevidcards");
  if (s1 == null) { s1 = ""; }
  numcards = Number(s1);
  if (numcards != numcards) { numcards = 0; } //catch a NaN issue
  var name = window.localStorage.getItem("myName");
  var birthday = window.localStorage.getItem("myBirthday");
  var timedate = Date.now();
  var tempBase64 = "Temp" + String(timedate);
  //////////////////////////////////
  ///  Then we hash the IDCard.  ///
  //////////////////////////////////  
  var canvas1 = document.createElement("canvas");
  canvas1.width = 250;
  canvas1.height = 375;
  canvas1.getContext("2d").drawImage(canvas, 0, 0, 250, 375, 0, 0, 250, 375);
  var dataurl = canvas1.toDataURL();
  var sha = SHA256(dataurl); //sha is the 64 char hash string of the Base64 code of your IDCard
  ////////////////////////////////////////////////////////////////////
  ///  and store the hash as your currenhash in the local storage  ///
  ////////////////////////////////////////////////////////////////////
  localStorage.setItem("myCurrentHash", sha);
  /////////////////////////////////////////////////////////////////////////////////////////////
  ///  then we create the name of the local storage where to store your IDCard Base64 data  ///
  ///  The format is this: Teun_van_Sambeek-01,4ca2a4859b705a78ce07.png                     ///
  ///         name with _ + "-" + cardnr + "," + first 20char of hash of the IDCard + .png  ///
  /////////////////////////////////////////////////////////////////////////////////////////////
  ss = name.replace(/\s/g, "_"); //replace spaces in my Name
  s4 = String(numcards + 1);
  if ((numcards + 1) < 10) { s4 = "0" + s4; }
  ss = ss + "-" + s4 + "," + sha.substring(0, 20);
  localStorage.setItem("myFiles", ss);
  s1 = ss + ".png";
  s2 = ss + ".txt";
  localStorage.setItem(s1, dataurl);
  //console.log(dataurl); 
  /////////////////////////////////////////////////////
  ///  If you don't have any previous blockchains,  ///
  ///  a new personal blockchain is created         ///
  /////////////////////////////////////////////////////
  if (numcards == 0) {
    ///////////////////////////////////////////////////////////////////////
    ///  there are no previous IDCards so create your empty blockchain  ///
    ///////////////////////////////////////////////////////////////////////
    var today = new Date();
    hcd = Number(String(today.getDate()));
    hcm = Number(String(today.getMonth() + 1)); //January is 0!
    hcy = Number(today.getFullYear());
    hch = Number(String(today.getHours()));
    hcmin = Number(String(today.getMinutes()));
    hcsec = Number(String(today.getSeconds()));
    var sdate = String(hcd);
    if (hcm == 1) { sdate = sdate + " Jan "; }
    if (hcm == 2) { sdate = sdate + " Feb "; }
    if (hcm == 3) { sdate = sdate + " Mar "; }
    if (hcm == 4) { sdate = sdate + " Apr "; }
    if (hcm == 5) { sdate = sdate + " May "; }
    if (hcm == 6) { sdate = sdate + " Jun "; }
    if (hcm == 7) { sdate = sdate + " Jul "; }
    if (hcm == 8) { sdate = sdate + " Aug "; }
    if (hcm == 9) { sdate = sdate + " Sep "; }
    if (hcm == 10) { sdate = sdate + " Okt "; }
    if (hcm == 11) { sdate = sdate + " Nov "; }
    if (hcm == 12) { sdate = sdate + " Dec "; }
    var ss1 = String(hch);
    if (hch < 10) { ss1 = "0" + ss1; }
    var ss2 = String(hcmin);
    if (hcmin < 10) { ss2 = "0" + ss2; }
    var ss3 = String(hcsec);
    if (hcsec < 10) { ss3 = "0" + ss3; }
    bdd = Number(birthday.substring(0, 2));
    bmm = Number(birthday.substring(3, 5));
    byy = Number(birthday.substring(6));
    getBirthdayHour();
    mms = "";
    if (bmm == 1) { mms = " Jan "; }
    if (bmm == 2) { mms = " Feb "; }
    if (bmm == 3) { mms = " Mar "; }
    if (bmm == 4) { mms = " Apr "; }
    if (bmm == 5) { mms = " May "; }
    if (bmm == 6) { mms = " Jun "; }
    if (bmm == 7) { mms = " Jul "; }
    if (bmm == 8) { mms = " Aug "; }
    if (bmm == 9) { mms = " Sep "; }
    if (bmm == 10) { mms = " Okt "; }
    if (bmm == 11) { mms = " Nov "; }
    if (bmm == 12) { mms = " Dec "; }
    var st = sha + "|" + sdate + String(hcy) + "|" +
      ss1 + ":" + ss2 + ":" + ss3 + "|" + name + "|" +
      birthday.substring(0, 2) + mms + birthday.substring(6) + "|" + String(bhh) + "<";
    //console.log(st);    //remove todo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    localStorage.setItem(s2, st);
    ss = "Your new blockchain is stored in your browser ('" + s2 + "'')";
    alert(ss);
  }
  window.open("400_idcard.html", "_self");
}


function drawPixel(x, y, color) {
  data[0] = color.r;
  data[1] = color.g;
  data[2] = color.b;
  data[3] = color.a;
  ctx.putImageData(image, x, y);
}


function generateCard() {
  if (imb1) {
    var name = window.localStorage.getItem("myName");
    var birthday = window.localStorage.getItem("myBirthday");
    var gender = window.localStorage.getItem("myGender");
    var long = window.localStorage.getItem("myLength");
    var hair = window.localStorage.getItem("myHair");
    var lefteye = window.localStorage.getItem("myLeftEye");
    var righteye = window.localStorage.getItem("myRightEye");
    var features = window.localStorage.getItem("myFeatures");
    var prevcards = window.localStorage.getItem("myPrevidcards");
    var d = Number(birthday.substring(0, 2));
    var m = Number(birthday.substring(3, 5));
    switch (m) {
    case 1: { var mnd = "Jan"; } break;
    case 2: { var mnd = "Feb"; } break;
    case 3: { var mnd = "Mar"; } break;
    case 4: { var mnd = "Apr"; } break;
    case 5: { var mnd = "May"; } break;
    case 6: { var mnd = "Jun"; } break;
    case 7: { var mnd = "Jul"; } break;
    case 8: { var mnd = "Aug"; } break;
    case 9: { var mnd = "Sep"; } break;
    case 10: { var mnd = "Oct"; } break;
    case 11: { var mnd = "Nov"; } break;
    case 12: { var mnd = "Dec"; } break;
    }
    birthday = mnd + " " + String(d) + ", " + birthday.substring(6, 10);
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, 250, 375);
    ctx.drawImage(image1, 0, 0, 250, 250);
    ctx.fillStyle = "white";
    if (!img4.complete) {
      setTimeout(function () { generateCard(); }, 50); return;
    }
    var s1 = "";
    var s2 = "";
    var s3 = "";
    var ss = "";
    ////////////////////
    /// 1CoinH logo  ///
    ////////////////////
    var x = 175;
    var y = 252;
    ctx.drawImage(img4, x, y);
    //////////////
    ///  name  ///
    //////////////
    ctx.font = "bold 16px Arial";
    var x = 3;
    var y = 263;
    //name = "Gggjbb van GgppLgggLL"
    ctx.fillStyle = "black";
    ctx.fillText(name, x - 1, y - 1);
    ctx.fillText(name, x + 1, y - 1);
    ctx.fillText(name, x - 1, y + 1);
    ctx.fillText(name, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(name, x, y);
    //////////////////
    ///  birthday  ///
    //////////////////
    ctx.font = "normal 14px Arial";
    var x = 3;
    var y = 278;
    ctx.fillStyle = "black";
    ctx.fillText(birthday, x - 1, y - 1);
    ctx.fillText(birthday, x + 1, y - 1);
    ctx.fillText(birthday, x - 1, y + 1);
    ctx.fillText(birthday, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(birthday, x, y);
    ////////////////
    ///  gender  ///
    ////////////////
    var x = 107;
    var y = 278;
    ctx.fillStyle = "black";
    ctx.fillText(gender, x - 1, y - 1);
    ctx.fillText(gender, x + 1, y - 1);
    ctx.fillText(gender, x - 1, y + 1);
    ctx.fillText(gender, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(gender, x, y);
    ////////////////
    ///  length  ///
    ////////////////
    var x = 3;
    var y = 293;
    //long = "6ft 11in"
    ctx.fillStyle = "black";
    ctx.fillText(long, x - 1, y - 1);
    ctx.fillText(long, x + 1, y - 1);
    ctx.fillText(long, x - 1, y + 1);
    ctx.fillText(long, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(long, x, y);
    //////////////
    ///  Hair  ///
    //////////////
    var x = 64;
    var y = 293;
    //hair = "Strawberry-Blond"
    hair = hair + " Hair";
    ctx.fillStyle = "black";
    ctx.fillText(hair, x - 1, y - 1);
    ctx.fillText(hair, x + 1, y - 1);
    ctx.fillText(hair, x - 1, y + 1);
    ctx.fillText(hair, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(hair, x, y);
    //////////////
    ///  Eyes  ///
    //////////////
    var x = 3;
    var y = 308;
    s2 = lefteye;
    s3 = righteye;
    //s3="Prothesis"
    if (s2 == s3) {
      s1 = "Eyes: " + s2;
    }
    else {
      s1 = "Eyes L:" + s2 + "  R:" + s3;
    }
    ctx.fillStyle = "black";
    ctx.fillText(s1, x - 1, y - 1);
    ctx.fillText(s1, x + 1, y - 1);
    ctx.fillText(s1, x - 1, y + 1);
    ctx.fillText(s1, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(s1, x, y);
    //////////////////
    ///  features  ///
    //////////////////
    var x = 3;
    var y = 323;
    //long = "6ft 11in"
    ctx.fillStyle = "black";
    ctx.fillText(features, x - 1, y - 1);
    ctx.fillText(features, x + 1, y - 1);
    ctx.fillText(features, x - 1, y + 1);
    ctx.fillText(features, x + 1, y + 1);
    ctx.fillStyle = "white";
    ctx.fillText(features, x, y);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///  Draw the hash squares of possible 32 previous ID-Cards                                            ///
    ///  add hash to filename ID-Card to avoid confusion once ID-Cards are collected                       ///
    ///  the hash is 64 char of 16 numbers 2x2x2x2 = 16 2x2x2x2x2x2 =64 so we need 64 x 4 bits for a hash  ///
    ///  we make a 8x8 square where each pixel can be 16 colors.                                           ///
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    var numcards = 0;
    s1 = window.localStorage.getItem("myPrevidcards");
    if (s1 == null) { s1 = ""; }
    numcards = Number(s1);
    if (numcards != numcards) { numcards = 0; } //catch a NaN issue
    var card01 = window.localStorage.getItem("card01");
    var card02 = window.localStorage.getItem("card02");
    var card03 = window.localStorage.getItem("card03");
    var card04 = window.localStorage.getItem("card04");
    var card05 = window.localStorage.getItem("card05");
    var card06 = window.localStorage.getItem("card06");
    var card07 = window.localStorage.getItem("card07");
    var card08 = window.localStorage.getItem("card08");
    var card09 = window.localStorage.getItem("card09");
    var card10 = window.localStorage.getItem("card10");
    var card11 = window.localStorage.getItem("card11");
    var card12 = window.localStorage.getItem("card12");
    var card13 = window.localStorage.getItem("card13");
    var card14 = window.localStorage.getItem("card14");
    var card15 = window.localStorage.getItem("card15");
    var card16 = window.localStorage.getItem("card16");
    var card17 = window.localStorage.getItem("card17");
    var card18 = window.localStorage.getItem("card18");
    var card19 = window.localStorage.getItem("card19");
    var card20 = window.localStorage.getItem("card20");
    var card21 = window.localStorage.getItem("card21");
    var card22 = window.localStorage.getItem("card22");
    var card23 = window.localStorage.getItem("card23");
    var card24 = window.localStorage.getItem("card24");
    var card25 = window.localStorage.getItem("card25");
    var card26 = window.localStorage.getItem("card26");
    var card27 = window.localStorage.getItem("card27");
    var card28 = window.localStorage.getItem("card28");
    var card29 = window.localStorage.getItem("card29");
    var card30 = window.localStorage.getItem("card30");
    var card31 = window.localStorage.getItem("card31");
    var card32 = window.localStorage.getItem("card32");
    ctx.lineWidth = 1;
    ctx.beginPath();
    var j = 0;
    for (j = 0; j < 16; j++) {
      xx = 6 + (15 * j);
      yy = 327;
      //ctx.rect(xx,yy, xx+9, yy+9); 
      ctx.fillStyle = "#AD8F00";
      ctx.fillRect(xx, yy, 12, 12);
      ctx.fillStyle = "#000000";
      ctx.fillRect(xx + 1, yy + 1, 10, 10);
    }
    for (j = 0; j < 16; j++) {
      xx = 6 + (15 * j);
      yy = 342;
      //ctx.rect(xx,yy, xx+9, yy+9);
      ctx.fillStyle = "#AD8F00";
      ctx.fillRect(xx, yy, 12, 12);
      ctx.fillStyle = "#000000";
      ctx.fillRect(xx + 1, yy + 1, 10, 10);
    }
    //////////////////////////////////////////
    ///  Fill the Squares with hash-color  ///
    //////////////////////////////////////////
    s1 = "";
    var t1 = 0;
    var t2 = 0;
    var t5 = numcards; //number of previous ID-Cards
    var t3 = Math.trunc(t5 / 16); // is 0 or 1
    var t4 = t5 - (t3 * 16); //max 16
    var t7 = t4;
    if (t3 == 1) { t7 = 16; }
    t6 = 0;
    var xx = 0;
    var yy = 0;
    if (t5 > 0) {
      for (var j = 0; j < t7; j++) //column 8x8 square  moet t7 zijn 
      {
        if (t6 == 0) hashnr = card01;
        if (t6 == 1) hashnr = card02;
        if (t6 == 2) hashnr = card03;
        if (t6 == 3) hashnr = card04;
        if (t6 == 4) hashnr = card05;
        if (t6 == 5) hashnr = card06;
        if (t6 == 6) hashnr = card07;
        if (t6 == 7) hashnr = card08;
        if (t6 == 8) hashnr = card09;
        if (t6 == 9) hashnr = card10;
        if (t6 == 10) hashnr = card11;
        if (t6 == 11) hashnr = card12;
        if (t6 == 12) hashnr = card13;
        if (t6 == 13) hashnr = card14;
        if (t6 == 14) hashnr = card15;
        if (t6 == 15) hashnr = card16;
        for (var i = 0; i < 64; i++) {
          s1 = hashnr.substring(i, i + 1);
          if (s1 == "0") { color = colors[0]; } //black
          if (s1 == "1") { color = colors[1]; } //Lime
          if (s1 == "2") { color = colors[2]; } //Blue
          if (s1 == "3") { color = colors[3]; } //Aqua
          if (s1 == "4") { color = colors[4]; } //Red
          if (s1 == "5") { color = colors[5]; } //Yellow
          if (s1 == "6") { color = colors[6]; } //Magenta
          if (s1 == "7") { color = colors[7]; } //White
          if (s1 == "8") { color = colors[8]; } //Green
          if (s1 == "9") { color = colors[9]; } //Navy
          if (s1 == "a") { color = colors[10]; } //Teal
          if (s1 == "b") { color = colors[11]; } //Maroon
          if (s1 == "c") { color = colors[12]; } //Olive
          if (s1 == "d") { color = colors[13]; } //Purple
          if (s1 == "e") { color = colors[14]; } //Grey
          if (s1 == "f") { color = colors[15]; } //Pink?
          t1 = Math.trunc(i / 8); //row    0-7
          t2 = i - (8 * t1);      //column 0-7
          xx = 8 + (15 * j) + t1;
          yy = 329 + t2;
          drawPixel(xx, yy, color);
        }
        t6++;
      }
    }
    if (t3 > 0) {
      for (var j = 0; j < t4; j++) //column 8x8 square 
      {
        if (t6 == 16) hashnr = card17;
        if (t6 == 17) hashnr = card18;
        if (t6 == 18) hashnr = card19;
        if (t6 == 19) hashnr = card20;
        if (t6 == 20) hashnr = card21;
        if (t6 == 21) hashnr = card22;
        if (t6 == 22) hashnr = card23;
        if (t6 == 23) hashnr = card24;
        if (t6 == 24) hashnr = card25;
        if (t6 == 25) hashnr = card26;
        if (t6 == 26) hashnr = card27;
        if (t6 == 27) hashnr = card28;
        if (t6 == 28) hashnr = card29;
        if (t6 == 29) hashnr = card30;
        if (t6 == 30) hashnr = card31;
        if (t6 == 31) hashnr = card32;
        for (var i = 0; i < 64; i++) {
          s1 = hashnr.substring(i, i + 1);
          if (s1 == "0") { color = colors[0]; } //black
          if (s1 == "1") { color = colors[1]; } //Lime
          if (s1 == "2") { color = colors[2]; } //Blue
          if (s1 == "3") { color = colors[3]; } //Aqua
          if (s1 == "4") { color = colors[4]; } //Red
          if (s1 == "5") { color = colors[5]; } //Yellow
          if (s1 == "6") { color = colors[6]; } //Magenta
          if (s1 == "7") { color = colors[7]; } //White
          if (s1 == "8") { color = colors[8]; } //Green
          if (s1 == "9") { color = colors[9]; } //Navy
          if (s1 == "a") { color = colors[10]; } //Teal
          if (s1 == "b") { color = colors[11]; } //Maroon
          if (s1 == "c") { color = colors[12]; } //Olive
          if (s1 == "d") { color = colors[13]; } //Purple
          if (s1 == "e") { color = colors[14]; } //Grey
          if (s1 == "f") { color = colors[15]; } //Pink?
          t1 = Math.trunc(i / 8); //row
          t2 = i - (8 * t1);      //column
          xx = 8 + (15 * j) + t1;
          yy = 344 + t2;
          drawPixel(xx, yy, color);
        }
        t6++;
      }
    }
    /////////////////////////////////////
    ///  Add ID-Card number and date  ///
    /////////////////////////////////////
    var today = new Date();
    var dddd = String(today.getDate());
    var mmmm = String(today.getMonth() + 1); //January is 0!
    var yyyy = today.getFullYear();
    t1 = Number(dddd);
    s1 = String(t1);
    //if (t1 < 10) {s1 = "0"+s1}
    t2 = Number(mmmm);
    t3 = Number(yyyy);
    s2 = "";
    if (t2 == 1) { s2 = "Jan"; }
    if (t2 == 2) { s2 = "Feb"; }
    if (t2 == 3) { s2 = "Mar"; }
    if (t2 == 4) { s2 = "Apr"; }
    if (t2 == 5) { s2 = "May"; }
    if (t2 == 6) { s2 = "Jun"; }
    if (t2 == 7) { s2 = "Jul"; }
    if (t2 == 8) { s2 = "Aug"; }
    if (t2 == 9) { s2 = "Sep"; }
    if (t2 == 10) { s2 = "Oct"; }
    if (t2 == 11) { s2 = "Nov"; }
    if (t2 == 12) { s2 = "Dec"; }
    s3 = "";
    t2 = t6 + 1;
    s3 = String(t2);
    if (t2 < 10) { s3 = "0" + s3; }
    ss = "1CoinH IDCard" + s3;
    ctx.fillStyle = "#AD8F00";
    ctx.font = "bold 14px Arial";
    ctx.fillText(ss, 20, 369);
    ss = s2 + " " + s1 + ", " + yyyy;
    ctx.fillText(ss, 150, 369);
  }
}


///////////////////////////////////////////////////////
// Main application - run only once at load screen  ///
///////////////////////////////////////////////////////
function OnStart() {
  img4.src = lg;
  /////////////////////////////
  ///  load your new image  ///
  /////////////////////////////
  var picture1 = localStorage.getItem("IDCardPicture");
  imb1 = (!(picture1 == null));
  if (imb1) {
    image1 = document.createElement("img");
    image1.src = picture1;
  }
  /////////////////////////////
  ///  generate the IDCard  ///
  /////////////////////////////
  generateCard();
}


///////////////////////////////////////////////////////
///  this is the Base64 code of the coin logo.      ///
///  this is to prevent a Cors Tainted image error  ///
///////////////////////////////////////////////////////
lg = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAACXBIWXMAAAsTAAALEwEAmpwYAAAADXRFWHRDYW1lcmEAQ2FtZXJhaP/v6QAAABh0RVh0RGF0ZQAyMDIyLzA0LzA5IDIzOjI0O";
lg = lg + "jA3LhqOVwAAADN6VFh0RmlsZQAACJnTz8jPTdUvSS3N03fJTy7NTc0rKdY3dM7PzPOAUvEGxnpJOal5KQBQeA783M+pfgAAAAl0RVh0RnJhbWUAMDAwtlYltAAAABN0RVh0UmVuZGVyVGltZQAwMDoyNC42M5rGbEoAAAALdE";
lg = lg + "VYdFNjZW5lAFNjZW5l5SFdlgAAABB0RVh0VGltZQAwMDowMDowMDowMJvEFlQAAAAldEVYdGN5Y2xlcy5WaWV3TGF5ZXIucmVuZGVyX3RpbWUAMDA6MjIuMjeHRgRjAAAAHHRFWHRjeWNsZXMuVmlld0xheWVyLnNhbXBsZXM";
lg = lg + "AMTI4D+0H8AAAAC50RVh0Y3ljbGVzLlZpZXdMYXllci5zeW5jaHJvbml6YXRpb25fdGltZQAwMDowMC40M1/MJGoAAAAkdEVYdGN5Y2xlcy5WaWV3TGF5ZXIudG90YWxfdGltZQAwMDoyMi43MJqyzeEAACAASURBVHicxbx5";
lg = lg + "lGVXfd/72Xuf4c5D3Zq7hp5bre5GSN2SWmg2wkQGB0wibBYOiU1sx4lxzHKM8x7LT8Zey1nJM8TBLMc4jxASMCyIABMIBkkIhLolJHVL3WpJPVUP1TVPd77nnmnv98e5dbsVMBaTfda6XVVdt+45+7u/v+n7+50j+Hs8HvvCg";
lg = lg + "yU3fGwy5TRHUH7JsUIbwFKpSOXsRuhlV8vlO66M3fTvVv++rlH8XZ3oqU++p7BW/fo9JqrfHsbRzZk0+1Npf8hyDEKAZUuUJZFKYjkKKSVaK6SyCQMaoRe+aFnWs9l8/qidu+2x7Yf/v+W/i+v+iQL0+Y/8XCVqPPf2KGo94D";
lg = lg + "jeHbYV2W5KERub9WaWVneA0AwQ6ByxTmO7GQQCqWJcK0DRIJOqk8+3GR+LyWctohhM6OnQ946VB4sPOcXrP71l/+dmf1Jr+IkA9Jl/f9Nhvz33m5ZsvU3JyFW2w8LGEM14G209hbYrXLw0S6PR4ODBm3j22DGGKiXe8MY3Ua9";
lg = lg + "u8LGPf5zhoSHe9o/+MXEccfbMGTKZNFOjOXLWRXLOS2ybbpFyA/yOH8eheKSyZfLD04ee+KoQwvw412L9OD/sYw/ecIcI5/8gbJ66x7GlqHaHWfGuo6P2kMqWsCybkuOAgFbb46+/9jVuvPFGsukUh193FwsLCwxXihht2Lv3";
lg = lg + "OsrlMr7f5X8+9BBvfvObufXwbRSLd5JKpbi0eIXqmW9TdI+q6/dU31hbOffG1c9PnLz05OEPbL3tqc//uNb0YwHoP//bw1tFPPMhvJffaqcssertYLV1kIuLITqO2Lo9jUBgWxaWZSGkZNfu3ezbt49PfOITvPe970UIQbfrs";
lg = lg + "bzs0+50mJiYwhjDiRMnGBwc5J577sEYaLfbBEFAuTLCwOADwAMcv3ACml9k1+SLr6mtnn7oO58dfzJdnHzPa974nWM/6trkj/LHDz74oPzo/73tt2Vw4pRrNX6uYabFsys/y+n6HZyfSxiidYSUEiElCDCAEAIhBAcOHKDr+8";
lg = lg + "zNzWGMQRjD0vIKSkpGRkaIoojnnz/Brp27MAaCICCOY+I4ptPp0O126XoeW7a9hvEDv8cLy/+WYy/txc0Et1WXTn/n4b+Y/NBnP/tA+u8FoD993+3jI82PPCKCuT8mVcieqv4U357Zz/nZOkEQUiyVGR8fZ27uClEUEUURWhu";
lg = lg + "01kRhSKfdZnFxgUOHDvJXX/oSURQRxxGzV+YYHRtDSMnGxgZXrlxh3/79AGit6Xa7+L5PGIZ4nofX7dLpdPB9H+WWOHb5tXz75V+m7o+obL7+3vTCN4799Z/fcsMPu071w/zRh3/n+jtlcOZRS3X3L3o7+M7lG9hoSmbOn2fL";
lg = lg + "lnGEtLBtmzAMuXTxIvv2H8CyrF7o1ug45MgT32L37r1kUpoz5y6hI59MJkUQhnQ6HsVigce+8Rj1WpV77roNIW201iiliOMYrXWfTVGUsPTpp5/mk5/6FKnsIAOTP8/Sss/W8bmhjaXFd739H+ya/ezDqyd/0LX+wFHsP/7Wz";
lg = lg + "nfK8MrHbNdxTy7tZbZa5ulnjjE8PIwUsG//AVzXxXVd6vU6/+tLX+Ld73432VyuD9L62ipBGLJ1S5FKoYurmtiqS6ttyA9MknWqSKeMURXW6y4r6yHFYgnbtrEsC6WSPEkIgZQSKSXnz5/nz/7sz3jTm97EXXfdRRRFGGNYm3";
lg = lg + "uGfSNfpLW+Ybr+6B++6/cvPfgTA+g//Pq29yg9/yepQl4+MXMdK3UX20529vjx5/C8Dvfd9wZKpWQxtm3zyU9+kt27dnH3PfeQtkLKzmVS8Sxp5shlWziuQAiQEqrhHuzsBMOpb/X9lNZgxDC+2YrI7qHNATpdB22SaK6Uola";
lg = lg + "r8cEPfpBDhw7xlre8BWN6ptwz7VZtgR3FLxDXz+P7w3/2i78/+xuvNh141QD9u1+d/g0TzH/YLZTFN8/upNGxkUr1WWHbNidPPM/S8gqHDx9m6/QUXa9Dq9XmNTscKtYZCtZlUilDKiWJtctqM0ejU0AbSaxjRHo36cIw3sq3";
lg = lg + "e0zRVEoddm0PyWd9/FAQ+oJYHaDr3spSfSunTh7nq197jNGxMd71rncB9AEKw5AwDLEsC69dY8L9EqpzgkZr+D//8z+68i9fzbpflQ/6f35p6ztEuPBfsgMV+fUXt9HoqFfQOwHIotv1OXjwIE89+SSplMue8Zi95ecZsk+Rd";
lg = lg + "+uEFLi8PslGq0InylDKBUwPr7BlcJ3J4RrNaAelgTw3b3+CbRN1JkeauCmby4tZTs+UuLJYRMmQidEFRPspXP8EWyZ3kC3vY6BcZHRsvBcMrvqnOI4xxqAsh0awk2xqAxXO3PyGWybsrxypf+NHBuj3/um+20Uw+/lsKW8/+v";
lg = lg + "JWNpoCAQgpkUIge/7AljFLK6sMlAe4fucwN285x7DzEoVsSD0YY82bxrIlU8VZRgdqDBY6OLKJlBohBVLCUvdWAgbYkn8mMTthsKTHYKHJxHCVqZEaTS/P8y8PsrpusXuXx5nZSTJuxN49w3SCHFrHfScehiFa6z6jbCdFK9p";
lg = lg + "FKbeEbl+88/7bt1788pHqiR8aoAf/5T2junXqkVTGKj8zv5OFtV4OIwUgEpBkkimsr6+hlM3uoSrXFZ+jkO4QWqPMNyYpuDUG07OkZB0dG2zbUF0LSaUEUoFB0OkYlvzDBLHFVOEEEAGCOAapQEqBjg2ubDFaWieXMTzx4m20";
lg = lg + "xW4q1rcZNP8bS4Q0o0miWPfShhjTA8gYg1IK20lR7WxltHRB+I3F+3/mzt3/+ytPrC79TRh8vzxIhLWT/11Z8fiF5jYuLfb+Uwh6/hEpRHJyrSmVity+fYmtuZfI5FzmO9fRbGmG7Zew9QZBYMBIolgglUs2bxPGhjiGKDTEk";
lg = lg + "WHtwrco8QxR0CUKIIoMWkPQNXhtjd+9+n5HNIm7q9j+BdY2XNziIHkepdz6L4i4ThgEhGFI3DO3TZDiOMbNVrjUfitD407KBHOf/eyDD+R+YAb9ztsn/5UJVt8TZyZ5ZqaE0bpvTkKInnMWSfVNwP7yCSrpKnZhhNn1EQbUed";
lg = lg + "KqiRCClCNwHEUYaSzbEIQRShksSyTZtQapBF09QqZQoOzOYgyEIYSBAQRaG8KAJKppiI3DUnWIbPuvObBzlbNXJpBOmZHyZYLVF/DkdgLtXmVCj+1mM/q5Zao1ycTgmYGFlbXKV59qfvlVA/T+d922xQQzX8gUc+5TF7dTrda";
lg = lg + "wHRcpJUqpfh4CAltGvLZygnK2gyhOs7AEWwuXcF2BNJDPSCINUajJZhVhaLAUGJP4HaWuhvkV/xCRrFBxXkTrBIygx644hjgGjKAZT/PM3N0MOy+wa7uPlIaBzBq1psv8+gg7ppYJ107hqZ2EPZCUurpUAVgyotYtk1arOMze";
lg = lg + "9Ppbb3jsq0cXv0s2+Z4mFgfn/l8hovyLi2PERtH1un1wNr9iDBLN/vIp8qkOqridteU2k/kFUhmLWAvyWYkXGNCQy9l4XYE0iYkaA7GGOErqMx1DbfkcGblCHBl0nAAkhSDWKTyzjTVzJ89v/AJPL/w0a800pUJEHBm6bUOrK";
lg = lg + "SnZSxTtdU5c2MnklM9A+zPIuPEK85JSMnPhAh//xKf51F9+mir/kNHJrAzaFz5sHnzwu/D4Lga975/ceCj2Zv9Ep4bExfo2LCnwwxDnGgYBYAx7SmeppKukh3ewMNdgML1KGAmyGQh8iGODHxiMFhQKgk47wnEkhs0kLwFL68";
lg = lg + "S3XZpXFIuSvLWCNokpAbw8N835zhtZqhUwWlCb/Q6hLDGamyPntmk1DUE3xrINBB1sWzNf287WsTk6q1cI0zdw5coss1fm+MxnPsOxY8e4bs8e7nvDfYTaJgoUg4Uzo19fvnTx68+8Mqp9t9zRvfz7jivlmfpWXnrpJcIwAmB";
lg = lg + "8fJx0Oo1Simajwa7hFmO5NbKDo8xc9BjPrWIMRDGEsSLWEVGkSbmSWMPKGthKEkQapZLIpA1YCoSAkDzl4S0srsRMDGwh9uaQMgFubmYGa1uX1sLTVN0t5IdeQ211nY1WjsH0MkqB49g4tgAT4ch1As/l4uIUw6VL4D3Gy6fh";
lg = lg + "woWL3HHHHRw4cIBUKoUQAse28aM7yaefxZ9Z/b3HHnzwU/d+4APR92TQ7/6zgwe6jdkPaXdYnF+rkE275LIZXNdlaWmJer3B7OVLbJ8sc3D8PLlihgsrFYbtyyAScxkZVNQbEWlXoCxBN9AgSPIdYXppQsIeJa+m8meW9uLHB";
lg = lg + "exUhSvLacaKVzAaAl0iyN9L1xQwdoVsYZBU5znWWxZSpZkuz2FbktjEhKHGdgRhCK5qsN4eo1yMwTvP+Pa7qAzvYM91e7FUsiuWZbG8ssypUy9TKY9Rzr48cHL+0ulvHq+e+p4MCpsX3+M4iHPrQ3heF1A4KYdUJjGtQrGE3/";
lg = lg + "W4fugyShlWupOUxTm0gUJKkc5K4siQTSe+Rwj69VQUCQJrAmlclIaKvIgUm87XkOoeY2VjiInpIVz/JcJA0Y3znAveigk9Uo1vU/en6TQVdmYfVqpJV6Sw7BxR0CaKQMjEtIWURGHMWOY0F1Zv4TXTz9Ge+TqDQ+/k0oVzhDG";
lg = lg + "cP3+el156iXa7zc4d27n5pl9kdPCbXLxQ+03g09/FoAcffCDnzZ/6b9ItOi8tDyKEhF45sel31tfW2Dos2FWZx8qNUl1rkrPbGA3ptGS9GmFCTagNupcrxTE04i0sibvY4Dpq8QQtPcJ46kV6OSY6vZ98ukU9GCZT2sJ4fpam";
lg = lg + "2c5CcDO+D97Fz9HN3opxKzTmnsLODjMcPE4hOEbajVDSJldKo5RNbT3C8zS5nML3NY70WOtMMTIwSxSWeeirxzn14osMDw9z55138sADD3Dr4VvRKDrVNfLupS133nT75x95+vLKKxi0fuLJt4oozK0HQ8geOMaYflVt2w5uy";
lg = lg + "mV3ZQ7HtVio5Sm75zBAGEMmZyEwSNui2QyINXh6kFVzAF+NII1EaI3RhpRbBQzGCLq+4fLsOmr0TdREQOwPU49dLp2/yMDWCrnGV9i9VXImHqBAneEtPhdCw2Cpy7YJG2Og0w7wPD/JupFYtqG+ESNsgyXWWe9Os3U0jb30BK";
lg = lg + "//qbdSKg1Qrgz1UxZjwLIsWvIgU+VjYnnt7C8Cv/sKgNK294CwHV5eKaGUxMArAEqlUgy4LUrpNoEaxwqWEA5IAekUXLrcxXUESgW0wwIr+no6chwpFbKXnG1mtA7rvehlUBJUPMfquk/UXqMVNtGpMSYqp2i2rzBZXqFq9qD";
lg = lg + "iFnbtK8j8ODKMGRsOaTZihAAnlfgyoZJ0wbYUvjHEUYyUMOie5czsTsaHXmCXX6eb3tF3K3EU92pBiVvYStAuIY33jzYBkgAf/vB73NBvv943RSLsfsa8+RJCEEURU8VlhJSsNzPknWbPxyQnchxBN04z272Bc8FdNPUoumdn";
lg = lg + "iR+6WmGnRA1DkgcZAwPlFKmUS1HOkBLrRJ0VqtEkRTWLkILVcA9u+zkmp8dY8Hczml1BEOO4YKUH0NYUsZoi1hniOKbrR8SUwJ1GW9MIo+n6NtmsjWg8i5Siv2Ha6H5BaxAs1fYwONjZ8Ue/cc91fQZdOPrYbToOsp4p95I4k";
lg = lg + "9RZfdFKY4mY0UIN6eSJ6g1EOnGKm6VCENmc8e9GI7GUIKOaODKgzShGa+Jrquqs3YBe/mMM1MxOCuI8vmWRyktWzr+MLt3IVvciq94k0rtIlNnBMysV7JTNkHu6X3LMx3fSFlMopRgOHiJDG4Al+Qa0NQ3ARPgJ8tY8lxbGKe";
lg = lg + "YusRGtEcjKK1RJrTVSSiJnFzZH6SyffT1wWgJof/V2xxFUOwUgyV43HfSmiZWdGgJNI6ww4K733tdzsgZCMhStVaZTL7I39Qhb1aPkRFLhbhaMulddZ1QjyZ61IYoFK8FuCvIKqBSRNqTMQnJuHbDqjVM0L9CJc2SzGaIoImt";
lg = lg + "ViSPwfYPPYHIRxqCi1eRaIgjEQLJoodH+Ola0SCcaw7GB1ssJa66p9PsvdxzbsomD7h19E0s7wSGETa3rIq6xG9H7KqVkKFtDSoHXlSgZ9yOQ6bEgLetszxxjOHWZlOVhWeDpXN+0tNZEcUza8hAE0Cs1GuEokVbYep1AO4TW";
lg = lg + "KNmxgwgpmO3ehBNeYdW+BytVZnV1DdeKcWQHrQ3d0CUwbrKwqIXQHkaDH+XQIqnBHBpEUdgzqYBU2iJunk2uqwcQ15Qilpuh1ipgOfrmPkASvT/QaYRy+ox5hSguBEO5JkK4BL7fd3Cmx54ERIhCUEISRglw3TiH7nUdNkHK2";
lg = lg + "S3k5h4YaISD2DQJAo0XplhaCVi9fIqoOUdIlhXxWpTKMDc3T6FQIGs10dpgDPimTBwnPsSK19A9s/Wp9DdYRavQq/0y1gbr9RKOmQcdEkVRn+Gb+rXRmoY3hCW72z772Q+l5WOPPWZFYbA1NCmWFucxvQJIiEQ5VEqhREza7t";
lg = lg + "LVWTJWCyGSaBEESZ4jRXJhpaKNm5JkXUkcJwyKrpE+hRDk3TZCJBeMAM+LaPd0Ho8BYpkl6/hIO09KeshgncbqBYrFIkIIXKponZy3awauyhfxGvRKnUBUerqVwfjL0NsQRy+x3hwmm43Q3hLfePRRZmZmmLsyx9rqKq1WizN";
lg = lg + "nTtONy7h2KJ//2pd2WEf+10fHjPGtmBT5QhHP65DOZBPnHMdcunSRPdNllIRumCZrr2A0ZFxFoaRYXg4TKpjE3mq1mGxaEmqXyCQdDwDRKyoyqo4Qm8AKXL1MJ9xKXVVQdpZUKkcpu5+5ThFTe5JBFSC33EO1ayUAiXqfKYGo";
lg = lg + "YIwhiiJUlDAIA6E92P9/GSYMSqjSITIZLEvg6AZnzpzhxIkTdH2fKIoQQjA1OcnP33+AwIMwXpiyWhuzo5YCjYtl2eTyRc6dPUOr3aFWrWI7Nq/dmUUIiIyDLUO0BssRLCwGpG1BYEBZYKQklzGgIBCFqzYOSJU4razV6C/QA";
lg = lg + "Bm1xlS2yUJ9Dzq8Qix2MO+P44oNtg1usLru0/K6CJlOADJVdK886cpyP7ey9HpPQjEEdhkThomJxavIXr0gxFWfEAc13vzm+5mc2tZPYwLfZ2bmPEY5SfCJukOWNK0CQhAbi/PnZ1hfX2O4UqBULLH/+usIohhHbdCr7xLnrA";
lg = lg + "S5goXfiRmezDN/pZHoNkGMHxtEDJ0412fPpm5tK3BlEx3T14TyWUFn/Shjw/ex4U/T7nTZs3WQ9vy3SKVtSoWAhsqjpMJSCiuuoSUEoSB0i8RRhFISK66iBUSxJFYl0BpJhNJ1tOSq3yPxX0Qdut2gL+xLIXBdl73X78OvzRI";
lg = lg + "EhiiMipYOGraSYJCMDA1w/d49uOksqVSKhYX5ZCJDxj2nqvtAVdcCsjlBdb3L8GiOucstHFsmq5bQ1fl+boExaGNI2R0gThjUA8ixBeVCzMr8V5HFN6JEiua5v8SxYnQkUHYOjYOlFMq0MdrHiCQAhLqnSsZ1MCEGgW9KaBKn";
lg = lg + "aOl1RPJTwnoFcawTyTbqcuLkC3zzW48zMDDA6OgoQ0ND1Os1dk0OYGsIg8ixLKWQvV0e2zKOEEn0CoKA8fEtvHDyJNvKm6KERgqINGSzgk5XIwjxOzBcsUllFcuLXYQUtMPs1V66TiJNOtO8mhpoesKZIJcVCCFp5xwCUgx5p";
lg = lg + "u+1PF1EqCRZtdlIriKGrin1QneMNOtAIr6FYgAdJxKLrdeTZJarskoUaeI4ycEmt4yyfft21tbWuHzpEs8++yxdz2PrO99KnJDfWNJ1Q8tPKGjM1dIBYwiCgD179oC42KOoQQD5tMRJWSwu+4wOScJuhMawvhFSLCo6vqETpa";
lg = lg + "8OK/R8Ud5p9U3LbFK9F+4tBWb5y+RdUGnRqwWha8q9yzFYppqAYsAIBytcQeuItJzHJPU1PmViHQNg6XWkvZmGJFEtjOJE2xYWU9PbKBSL7N69u189XLp4AR1HRCFI5USWsNy6isCWETMXZnBsGykVjuuilKRRq3Lj7gyIpIP";
lg = lg + "hOhahjqktBwzkk11udw3DwzaCBCg/TmOk3Q+1QohET3I7PXDot47olTYAtg2uI9C9jdIGfJPtlyhCN9EStDFk9QxZPZOokybhSBwbfDXS78un7dVkBzbnknrsi+ME4GPHjjExOcmWLRM4jkNsDJPTW2ktnyU2YNlW3Roa3Lva";
lg = lg + "WnwB1wpwbUWr0yEMQ3w/IAgCut0ue7ftRwjIpWL8SOG6MDgo8boxkdaAYW0txHGAGDxd6MkI5hUJZ9Zu9ZO25KoNRiSISQm5TNLhMLr/a8DqMzAw6WTGyIBSoGSvodjTrlt6FN+uEAcBaSsgq1Yw9EqizSaBNkQRhKRZW7vM2";
lg = lg + "XPn0VozsWULU9PTGK2ZLIe9iKiWrcM/d/fCY3/xV3EuFaodO65H2an+rsdxTKfdphN1kyQv3WWtmmJQNPGimC3Tea5capFyBX6webEG3xT6kWuTRWlHIHUDc824hOn/c9VPvJJZ4CqPRi80t+U2itGLKBEmixai5+wN7TDPmv";
lg = lg + "U6wl54H3PPIHofJnpmGUQuQneINYSywOvvez0pN8XS8hKXLl3ihZPPUyyWGUulEALs0vCcPHTo10LbsS8X0z62k+pPa2wurlAo4GbK+HGarNOhHWYwGIQwXL7QZLDiUCkrCjmLKEouKBAlLCsZotpUI9OqhZTmFQBsuk5jrgK";
lg = lg + "16Zc231a0lrHtxJH42mVF3UsoKgQRhKHBDyw69n6W7PvxdVJLDmdbFMypXtuo97ka6kGRYqpGEEg6Oo/juCAEo6NjHD58G//4gZ9n73V7ULqBxtbX3fbTMxaAZalTacvfThwgbacvcWyahmUMzXCAnJ7Hsu1EB+rxttUIMMIQ";
lg = lg + "dDcdvCCkhFKqz0KAjKy/Ytbm2uGcTcedtH+4WooAIlhgbKDN5XUbjKHNAKHzM5TLIIWmG6eoN73e9cJgPmJMf+PqGXqfpQ3UOnm2D1xmrVUmti1kr0DdJAPA6NgYcv4JkOnLv/ZrH+hIgFinnpWiRTkbXJ3g2ixYgfWNDerRE";
lg = lg + "Do2OHZMrBWCxG/YaQvbkuQLCtsWIBQ+uT7I/QiW8r4nOsaQCO5aMFi2++rA5ksKSNW/zmAuSgKFEIRhxGo9Zq0pabS66DjZzKlyg7HoywjTTiJyL7JpDTo2hJFEm5jA2tJ35FonedImQK1GjZRqA9Yz0BPM3FzlqNKrlJw1zs";
lg = lg + "54ZNIurXabVqtNs9mmWq2ScfaxM6/YOtLm9IUCW0pVhIBWM0xuJbB6YVbnMX3Tuaqz5J32K1jTzyZIQFCWYK0aooRAG9Ov1xBg4ib5+hcx8noaYjsR6T47LakZSK8xaJ3H7lxGWld5uumctYFat0DObhCG0FUTye+v6RZvRsq";
lg = lg + "4s4KlYpSdOtoHaM+unzty+eyHvPHyejoMXR57+mnS6TS5XJ5MJsPU9DRLq00a20cZ8BeImASqSJn0thC9PArwKfZ92ObOOrZNSmyA6ac9r/hGSiiWHKobPuUBF4RmcTHAtgRCCaJIo2REIT5BNjgJKksk0igiHNGkmBGEfoSy";
lg = lg + "eiVxL00QMhmt0dqw1h5k1/AsDa/ESsemPGD1W+ibQUkKgRMuYKSgNLLzUZhLALr3lz7Q/W/vH3x0rFx/89jQHu57/eupDA4RxzFBEACGZm2d5W6BAXeBscGYtWqawZzXF8566QZBD6A4jnvJoCGXMqDbPWW9914DvWhOrA2hy";
lg = lg + "WDbARsbiTNLpySOLbBsgdfZFNYFlgVKthGijdcx5IsWaduAlj1dOTF9QaIWRJGhG9joKEIITdfZzcypi9xYTAIJPWCkEHidNoOZFcIwe+n9H/7mKbhmeCGdq3zB6C5bh+rk83mUlEg09doGM+fPc+LUyzx31icQA0wPb9CMxx";
lg = lg + "ECggjanqHREtTaadpxpU/XTSeTcTWhLtHxBJ5nEkHtGjMbGCrRrNV6oTspA4yGIIhZWw1JuapvMuKaVy6bjAR3PI3TG6URvY1KMufEj12pj7NrbBkvSDNbH2J5eZmHH36YhYWFfmNCWRbd+iJZp4Pjph/axKXf9slnD/3PanX";
lg = lg + "hw7tHl7PHj7icPXOaZtsjDCNKpSIjIyPoWDPX2Ucp8wTbxjqcWbmVSJaJcdFYGJKRPK2jvu+RSrHaVFStnyKbSeHaMcr4DMrTZPVptBbEUUQUaNIlm/qSJpOW2I7EdSVCGDw/Adv3DSMjDu1WiOsocgVF4MVkMpIgNGSUoFrT";
lg = lg + "lMo2tqVpdTR1L400MbYV4aVvISeHePOb38TJEyd58skn2bZtGzfeeCPdrocbnCNWmJGJ3Z+CZOis31n91Fdf8B+4b2BXpdh67UZnG/MrHW6++RYOHjrE9u07GBsbZ2S4wsxsg21jkrHyEsveNF2GiPTmzKIkihPpcnP7Nsf0B";
lg = lg + "BBFMd1A0/QMI6l5LFMjn89SX2+RzSm6XoxjSbqBJo50Uov5htgY4tiQcRUGTbloo9w0BomUikzORiiFEQripM7L5RR+13B2ZZobts/jRSWW9OuwLBelLKamphgZHuLUi6d48uhRdkyNsz1/krbnPvv7//XMH34XgwCKlW0f7j";
lg = lg + "ZP/tMdlXOie/h2coUCvu8T+B5zV+a4fHmWWr3O1onD3LZlmcPbX+Abz1zm1qmlfv2klCQMdd8nSWmItUhmfnQidGkjUERoLKI4wLYFjiPxuhpbSaRM/j6Or2bCtiVIZwROStLtGlqdFq6T6OBCS9rtBNCR0RSeF9Jpx5xfHWf";
lg = lg + "XyCpBYGilb+Pci7NMTkxQLJUQQrBlYpIHHniA7xx9Art7ESsXMjA0/hGo9TF5xXTHp74yt/T2+wZvnxht7Tg7X2FuYZ1LF85x7PgJ5hcWKZfL7Nt3PdWaT3FghKH0JfI5i7OzFqPlDlJqTBST32SDo7GUQQqNlEkLRghD7Mek";
lg = lg + "HUmhaNOsBigbul2NkhDFSahrtwz5rKRcsqjVYyxLEISGKDRE2qBUYnJpVzIwYON5hvKgQ7MZIIHLa0VsKRkdrBNmD7HU2cWTR4/wu01yiwAACvdJREFU3PMnWF9bxVaSykARy07h2pLtuWfwuvblsdve9Ktf/vIx/T0BAvj1f";
lg = lg + "7bvXKe+/O60HYjTc3lmryywf/8BbrrxRkbHxrAdh0w2x1ozx2BRM1FZJhRlFlcMA/mAOBJoYpRIJlQ36yslRH+mEQR+rIlCnYzqKYllSRxX4qYUtq1wLYkXJIWwQKB7TlpK0WMqmBiEJYhijetIun6UjPI1s9TbZa7ftowvtj";
lg = lg + "If3o6yXTKZDNu378D3A54/cZLHH/82ge9TkVcYKazgZob+zb9+8NFnr8XjuwD6xOfn537hp4f2jw7Vr1/eGGRsah9DI6NIAc1Gg9XVVRYXF6k36nTYznipw86pDaqdAdY2NBOjmjgSFEo2hUqGVjPAxAZNMm0hYnBTCjclEQb";
lg = lg + "8UBOEpldXGYJAEwTJqFw+r4hDgxYSrU1/oEr2AHJTikxKYNuKMBZIaVhr5lhtjnDTnnna4SDV9M/iRxZKWZTLZS7MnOeuu+/mpptuIggDpscq7B89Qb3hnLru/v/0Lz73uc+Z7wsQwPv+9Z1Pry/O/cqWwabzzMtZTp54jqWV";
lg = lg + "VcIoplAosLq6SrfbZXrrDppmB0W1xp5tG9Q6BS7MCcYHvaRH7oWk0xY6Tm4/aGxEuFlBFBt8Xyf3W4irNZi55tLiWNNqa/xwM5Gj31OXCiyVfB/F4HU1AsNSo0SzO8CNO6/Q8kvUc/+QRsdBWVa/jRVHAdpALpdjZGgQt/EoG";
lg = lg + "bttBke3v+OXfusjF/5PLL4nQB/9y3P1X33nrq4yq2+UWuNWbuH6ffsZGhpiZXmZldVVbr/9drLZLL5vWPUmyMt1JoeWsewUJy8UGSp0wCT6r+cZarWYfMFipSnIpgyt0CIMDbbrsFbXLK4bjBAoS7LaEATaQilBzZMoKVhvCT";
lg = lg + "o+FDKJDqRUMqkmBWgjuLixBddSXD+9hBcNspF5C7V2CnFN+1wqxUClwosnj4FQtBaeZd/UFfyw+PHf/tPz/+l7YfE3DpLfe+Ftf6Lsyrded+M6ec4TRwFLi4ucOXuWW2+5mZGRERzHQUhJswOPzxxk3T/AQKbGP7g35uz6NhY";
lg = lg + "2cigLhkccchmB5RjOzcY0A4vZVTBK0okkcxuC5bpAWpKzy4qVmuHyUoxlW1xZijHA4rphuRdcNotQIaDWyXBubSdTlQaTgyu0zQRL9s9SbVh9RWHzSApxi3yxhFe9wg1TZ6htqIs3Hrj3vX8TDuJv+gXA3Il3Tpx4/OFjSDP8";
lg = lg + "V08d5NEjZ5iamuSOO+9GCIHf7RJGEX63y/FjRxgb38ZdN3SYyj5DviC4sDDA2QuCifIq5WwXg2BmCaYGNIsNxWA2JhQOtY0AXytKeUHLS5JLHWnslEu35WE5SWhHCIbSAZYl6EYOc/Ux8umIyfJSEv1KB7niHSTWqq9DbbLHc";
lg = lg + "RxU7+6kxSszTKqHEXEjGB7Zdtcv/+HJ7/xQAAGc/Pob7r700omvR8Jyvvz0zQyO76JYShp23W6XTrvNE088wUa1ytTUFIOVAabHBDeMv0CaOWzH5uLyAKs1BytaZ7TY7LWqk5Ijjk2/T7bZCtp8bR7yms5EPSjSDCsUsjGTpS";
lg = lg + "WCICSkSNV+HdVgC+l0Cq2vKqKu6/YHwCzLYmVxjkLnYYZLdSNk+V+854Pn/uL7rf9vBQjg6S/e/UvrC6c+1uxmxVMX3sD41r14nke1usEjDz9CGEUcOnSIVCpFFHocPfoke/Zcx+H9mjH7OQiqDAxnafhFzs+7tOohQncYyHi";
lg = lg + "klNcbQ+Fqx/UadTE0GZphnk6QJp2RVDJNRoo1PC8miNI0rP1cqk1y4dI8N9xwA7Zl9Tu6Sqn+XYqpVIq1lUVSta8zPd6g28n88W9/5PLv/G1rf1UAARz93F3/plU99x8aXkYcOXsHc8stvvWtbzI2PsHdd9+NMYau53H0ySeJ";
lg = lg + "ooiDr92PtNNIoZkoLjJkn6PoroExOCkbN19hYd2l1lAox8LoRLxPZidEb0JEY4s2BadJ2u4QhjGYZHqjqnez7E2ihU3X83j4kUc5cOAA+/fvv9pJ6d3o57ou1fUVSv6jjA/Vadbcj//uR+ffzSuFzR8NIIDH/8f+3+k0F/99p";
lg = lg + "IriS0f2cuL0Crcevg3Hceh2uxw5coRms8ntr3sdbipFFEUsLy0xc+ECe/fuJed4jGRXKDur5O0qkoCwX4JclV1f0eYGtLHpmgE8MUYtGsfTZSzL6oltglq9xuzsFS5fvsyBA/u57rq9vW4KOLbD+vIsO/PfYWigTX3D/fh7//";
lg = lg + "SX/7kQH9Dfd7E/DEAARz976N2N9St/brm29ZUj25GlQ8RhwLefOMpGtcrtt99OsVgkDAJeevll5ufmEFJy6NAhwjDst4OUFOQcn5RqYek2SgRIkoRIWQ5GuIQmTVfn6Jps776xxPG6rvsKsaterxPFMb7v8/TTT3P4lpvZsWs";
lg = lg + "3URRRWzzNzVtfxLF80/Vyf/yeD1343R/k8RU/8H3zr3v7sx8bGNt1f7cdrj/w0+cZVo/QaNQIo4i9e/dSKBTwOh2OP/ccIyMj5HI5BisVlJQ4joNt2ziODQg8nePEOY/PfG2GZy/lWJOvZUPdyPNXSrwwn6VuJvBFCSFtrF6H";
lg = lg + "xLKsRKuSvTlupZAq+XlyYoLr9uzh6JNHWV5aoDv/Te7a9R2Uafmp9OCv/OZ/vPi+H/TZHj/UoylueesTj7z82FsPnT/x1F8ePrB62572U2StHWhnmtOnT7O+scHBgwexbZvjx48zNj6ePHkhjvsMMioxjzDWpNJZtk5vRcoEg";
lg = lg + "BdOnaJSqbBly8TVQahrwtrmd5shXCD6TcqJiS1sHU2zI/stRidqNOvOzOCWXe/4xf/rmWd+mLX+0E9e2HvvFy/lbvi1u8Ju/v1uXOv+k585y/XDT7B7W4UbbngN6XSaYrHI8vIyhUL+FSPFm10EISVzc3OMj4/3OxYzMzNcv3";
lg = lg + "cvMzMzuK7bB+Lao68h9z6v0+mwsDBPu77ChHOMu3ccoZyp6yAq//m2G9924w8Lzo8EEMC9934guu9Xzv7RQOWGG5Zng6/u2TLPL9xzhNdOnsRvzPLc8WPk83ksy+7POkp59ZRSStbX1xkfH+9V/YZqtcrOXbtQSrG+vt4HZ5M";
lg = lg + "t1x5JrhORczX37qtz59TXmC5dplqzjucGdt31rz44/+tvefd/bf4oa/yBnfT3Ox772I33NesLf+CmvNvS+QynL+Y4Oz/O3MYAbjqZOLNtizBM+lHV6gbHjh3n/vvvB+DChRlGRkYZGBjgyJEjVCoV9uzZ059E3Rw9UUoShwEZ";
lg = lg + "scCgPcO24VWCIMTrps+mswN/MGu989Mf+MCri1J/2/FjBWjzeOzjh+5pVOd/E93+WdCWnSlwabnIQn2EhdUs7SCN5wcsLydP2dqxYwdRGPDc8ye46667yOdynDjxHFGk2XPddUm3JPCRcYuB9BoD6WWGMmsQd4gjjLRzj+eLg";
lg = lg + "3/6jvf//Bdebfh+tcdPBKDN4/GH3ja2cfH4L3he5wFB5xZBrLLFPG3PYa1u0woyeEEao7K4mSJhKNAmBhMT+R7EHWzRxjFNCqkWGbdL0I0BYYI486JyMg+Vhrd/6p3ve/zcT2oNP1GArj2+9NF3DHprx38qCpt3xnF4MI6j/X";
lg = lg + "HYyUexIQp7t4Bv1mMxfSlDSIFlpbrSsl9Wyjqu7PSR4sj+R9/+W1/+iT237Nrj7wyg73U89NFfHWstHp/stOvDURyVwiB0w1BjOW4old2w7MxqeuT6uXf/9qeuCCF+rKbzao//H6hRPy/3GXbzAAAAAElFTkSuQmCC";