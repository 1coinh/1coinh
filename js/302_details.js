/**
 * requires SHA256 from ./hash.js,
 * this file should be included in the calling html file
 */

/* global SHA256 */

/**
 * @type {string}
 */
var showtrans = "";

/**
 * hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time).
 * The counter does not need to look at daylight saving time or
 * time zones.
 * 
 * @type {number}
 */
var hc = 0;

/**
 * day
 * 
 * @type {number}
 */
var hcd = 0;

/**
 * month
 * 
 * @type {number}
 */
var hcm = 0;

/**
 * year
 * 
 * @type {number}
 */
var hcy = 0;

/**
 * hour
 * 
 * @type {number}
 */
var hch = 0;

/**
 * minutes
 * 
 * @type {number}
 */
var hcmin = 0;

/**
 * seconds
 * 
 * @type {number}
 */
var hcsec = 0;

/**
 * milliseconds
 * 
 * @type {number}
 */
var hcmsec = 0;

/**
 * birthday day
 * 
 * @type {number}
 */
var bdd = 0;

/**
 * birthday month
 * 
 * @type {number}
 */
var bmm = 0;

/**
 * birthday year
 * 
 * @type {number}
 */
var byy = 0;

/**
 * birthday hour
 * 
 * @type {number}
 */
var bhh = 0;

/**
 * my birthday hour
 * 
 * @type {number}
 */
var MyBhh = 0;

/**
 * string to represent the progress in time in the DATASET
 * 
 * @type {string}
 */
var timer = "";

/**
 * Currency character
 * 
 * @type {string}
 */
var currencyChar = "ᕫ";

var delta = 0;
var fact = 0;

var xr0 = 0;
var xr1 = 0;
var xr2 = 0;
var xr3 = 0;
var xr4 = 0;
var xr5 = 0;
var gname = "";
var ssname0 = "";
var ssname1 = "";
var ssname2 = "";
var ssname3 = "";
var ssname4 = "";
var ssname5 = "";
var endhash = "";
var others = 0;
var hash0 = "";
var hash1 = "";
var hash2 = "";
var hash3 = "";
var hash4 = "";
var hash5 = "";
var gprevid = "";
var previd0 = "";
var previd1 = "";
var previd2 = "";
var previd3 = "";
var previd4 = "";
var previd5 = "";
var receivesend = "";


/**
 * @todo duplicate! should be migrated.
 * 
 * @param {*} num 
 * @param {*} decimals 
 * @returns {*}
 */
const format = (num, decimals) => num.toLocaleString("en-US", {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});


/**
 * based on hc, the vars hcd,hcm,hcy and hch are set
 * the date in hc_date is also set
 * 
 * @todo duplicate! should be migrated.
 * 
 */
function setDate() {
  var t1 = 0;
  var t5 = 0;
  hcd = Math.trunc(hc / 24); // hcd = daynumber since 01-01-2022 where 01-01-2022 is day 0 here
  hch = hc - (24 * hcd); // hch = number of hours on day t1
  t1 = Math.trunc(hcd / 1461); // t1 = number of clusters of 4 years (=1461 days) that are past
  hcd = 1 + (hcd - (1461 * t1)); // hcd = remaining number of days in remaining cluster, add one day to make 01-01-2022 day 1
  t5 = 0; // t5 = number of years that has past in the remaining cluster 
  if (hcd > 365) { t5++; hcd = hcd - 365; } // t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 365) && (t5 == 1)) { t5++; hcd = hcd - 365; } // t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 366) && (t5 == 2)) { t5++; hcd = hcd - 366; } // t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  hcy = 2022 + t5 + (4 * t1);
  // hcd is now the day number in year hcy
  hcm = 1;
  if (hcd > 31) { hcm = 2; hcd = hcd - 31; }
  if (t5 == 2) {
    if ((hcd > 29) && (hcm > 1)) { hcm = 3; hcd = hcd - 29; }
  } else {
    if ((hcd > 28) && (hcm > 1)) { hcm = 3; hcd = hcd - 28; }
  }
  if ((hcd > 31) && (hcm > 2)) { hcm = 4; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 3)) { hcm = 5; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 4)) { hcm = 6; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 5)) { hcm = 7; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 6)) { hcm = 8; hcd = hcd - 31; }
  if ((hcd > 31) && (hcm > 7)) { hcm = 9; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 8)) { hcm = 10; hcd = hcd - 30; }
  if ((hcd > 31) && (hcm > 9)) { hcm = 11; hcd = hcd - 31; }
  if ((hcd > 30) && (hcm > 10)) { hcm = 12; hcd = hcd - 30; }
  var mnd = "Jan";

  switch (hcm) {
  case 1:{ mnd = "Jan"; } break;
  case 2: { mnd = "Feb"; } break;
  case 3: { mnd = "Mar"; } break;
  case 4: { mnd = "Apr"; } break;
  case 5: { mnd = "May"; } break;
  case 6: { mnd = "Jun"; } break;
  case 7: { mnd = "Jul"; } break;
  case 8: { mnd = "Aug"; } break;
  case 9: { mnd = "Sep"; } break;
  case 10: { mnd = "Oct"; } break;
  case 11: { mnd = "Nov"; } break;
  case 12: { mnd = "Dec"; } break;
  }

  var day = (hcd < 10 ? "0" : "") + hcd;
  var hour = (hch < 10 ? "0" : "") + hch;
  timer = day + mnd + hcy + "_" + hour + "u";
}


/**
 * based on the vars hcd,hcm,hcy and hch
 * the hour count in hc is calculated
 *
 * @todo duplicate! should be migrated.
 *
 */
function getDate() {
  // first count all extra jump-year days
  var t1 = 0;
  t1 = Math.trunc((hcy - 2021) / 4); // in the year 2025 we need to start with one jump day
  if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2)) { t1++; } // if feb has pars in a jump year we also need to add one jump day
  t1 = hcd + t1 + (365 * (hcy - 2022)); // then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
  if (hcm > 1) { t1 = t1 + 31; } // then we add the days of all past months in this year vvv
  if (hcm > 2) { t1 = t1 + 28; }
  if (hcm > 3) { t1 = t1 + 31; }
  if (hcm > 4) { t1 = t1 + 30; }
  if (hcm > 5) { t1 = t1 + 31; }
  if (hcm > 6) { t1 = t1 + 30; }
  if (hcm > 7) { t1 = t1 + 31; }
  if (hcm > 8) { t1 = t1 + 31; }
  if (hcm > 9) { t1 = t1 + 30; }
  if (hcm > 10) { t1 = t1 + 31; }
  if (hcm > 11) { t1 = t1 + 30; }
  hc = (24 * (t1 - 1)) + hch; // because the day has not passed we reduce one day, multiply with 24 and add the current hour.
}


/**
 * We need to find the name of the person with hash:gname
 */
function GetName() {
  var ssl = gname + ".txt";
  var s1 = window.localStorage.getItem(ssl) || "";
  if ((s1 != s1) || (s1 == "")) {
    alert("Blockchain: " + ssl + " not found!");
    gname = "Name not found";
  } else {
    var previdnum = (s1.match(/%/g) || []).length + 1;
    gprevid = String(previdnum);
    if (previdnum < 10) { gprevid = "0" + gprevid; }
    var t1 = s1.indexOf("|");
    s1 = s1.substring(t1 + 1);
    t1 = s1.indexOf("|");
    s1 = s1.substring(t1 + 1);
    t1 = s1.indexOf("|");
    s1 = s1.substring(t1 + 1);
    t1 = s1.indexOf("|");
    gname = s1.substring(0, t1);
  }
}

/**
 * Function to run when loading a HTML document. The function should be called with:
 *   `<body onload="OnStart();">`
 *
 */
function OnStart() { // eslint-disable-line no-unused-vars
  var ssr = "";
  var sss = "";
  var s0 = "";
  var s1 = "";
  var s2 = "";
  var s3 = "";
  var s4 = "";
  var tx = 0;
  var t0 = 0;
  var readx = "";
  xr0 = 0;
  xr1 = 0;
  xr2 = 0;
  xr3 = 0;
  xr4 = 0;
  xr5 = 0;
  // arrange currency character
  s1 = window.localStorage.getItem("myCurrencyChar") || "";
  if ((s1 != s1) || (s1 == "")) {
    currencyChar = "ᕫ";
  } else {
    currencyChar = s1;
  }

  showtrans = window.localStorage.getItem("details");
  var today = new Date();
  hcd = Number(String(today.getDate()));
  hcm = Number(String(today.getMonth() + 1)); //January is 0!
  hcy = Number(today.getFullYear());
  hch = Number(String(today.getHours()));
  getDate();
  var hcend = hc;
  readx = showtrans;
  s0 = readx.substring(0, 1); // can be "R" be "S"
  receivesend = s0;
  hash0 = readx.substring(1, 65); // hash person that paid to me or received the coin from me
  gname = hash0;
  GetName();
  previd0 = gprevid;
  ssname0 = gname;
  sss = readx.substring(66);
  t0 = sss.indexOf("|");
  s2 = sss.substring(0, t0); // timestamp
  sss = sss.substring(t0 + 1);
  t0 = s2.indexOf(":");
  hc = Number(s2.substring(0, t0));
  setDate();
  var st1 = timer.substring(0, 2) + "-" + timer.substring(2, 5) + "-" + timer.substring(5, 9) + ", " + timer.substring(10, 12) + ":00";
  t0 = sss.indexOf("|");
  s3 = sss.substring(0, t0); // securityhash
  sss = sss.substring(t0 + 1);
  t0 = sss.indexOf("|");
  ssr = sss.substring(0, t0); // rr0
  xr0 = Number(ssr);
  others = 0;
  sss = sss.substring(t0 + 1);
  t0 = sss.indexOf("|");
  endhash = "";
  ssr = "";
  if (t0 > -1) {
    others = 1;
    hash1 = sss.substring(0, t0); // hash1
    gname = hash1;
    GetName();
    previd1 = gprevid;
    ssname1 = gname;
    sss = sss.substring(t0 + 1);
    t0 = sss.indexOf("|");
    ssr = sss.substring(0, t0); // rr1
    xr1 = Number(ssr);
    sss = sss.substring(t0 + 1);
    t0 = sss.indexOf("|");
    if (t0 > -1) {
      others = 2;
      hash2 = sss.substring(0, t0); // hash2
      gname = hash2;
      GetName();
      previd2 = gprevid;
      ssname2 = gname;
      sss = sss.substring(t0 + 1);
      t0 = sss.indexOf("|");
      ssr = sss.substring(0, t0); // rr2
      xr2 = Number(ssr);
      sss = sss.substring(t0 + 1);
      t0 = sss.indexOf("|");
      if (t0 > -1) {
        others = 3;
        hash3 = sss.substring(0, t0); // hash3
        gname = hash3;
        GetName();
        previd3 = gprevid;
        ssname3 = gname;
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        ssr = sss.substring(0, t0); // rr3
        xr3 = Number(ssr);
        sss = sss.substring(t0 + 1);
        t0 = sss.indexOf("|");
        if (t0 > -1) {
          others = 4;
          hash4 = sss.substring(0, t0); // hash4
          gname = hash4;
          GetName();
          previd4 = gprevid;
          ssname4 = gname;
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          ssr = sss.substring(0, t0); // rr4
          xr4 = Number(ssr);
          sss = sss.substring(t0 + 1);
          t0 = sss.indexOf("|");
          if (t0 > -1) {
            others = 5;
            hash5 = sss.substring(0, t0); // hash5
            gname = hash5;
            GetName();
            previd5 = gprevid;
            ssname5 = gname;
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
            ssr = sss.substring(0, t0); // rr5
            xr5 = Number(ssr);
            sss = sss.substring(t0 + 1);
            t0 = sss.indexOf("|");
          }
        }
      }
    }
  }
  t0 = sss.indexOf("<");
  endhash = sss.substring(t0 - 64, t0); // must be endhash

  // Process data
  t0 = s2.indexOf(":");
  tx = Number(s2.substring(0, t0)); // hour of payment
  delta = (hcend - tx);
  fact = Math.pow(0.6, (delta / 8766));

  var fname = hash0 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash0;

  document.getElementById("button1").classList.remove(document.getElementById("button1").classList.item(0));
  document.getElementById("button2").classList.remove(document.getElementById("button2").classList.item(0));
  document.getElementById("button3").classList.remove(document.getElementById("button3").classList.item(0));
  document.getElementById("button4").classList.remove(document.getElementById("button4").classList.item(0));
  document.getElementById("button5").classList.remove(document.getElementById("button5").classList.item(0));
  document.getElementById("button0").style.color = "#F7E273";

  /** @todo replace by for loop */
  if (others > 0) {
    document.getElementById("button1").disabled = false;
    document.getElementById("button1").classList.add("main_button");
    document.getElementById("button1").style.color = "#AD8F00";
  } else {
    document.getElementById("button1").disabled = true;
    document.getElementById("button1").classList.add("disabled_button");
    document.getElementById("button1").style.color = "#777777";
  }

  if (others > 1) {
    document.getElementById("button2").disabled = false;
    document.getElementById("button2").classList.add("main_button");
    document.getElementById("button2").style.color = "#AD8F00";
  } else {
    document.getElementById("button2").disabled = true;
    document.getElementById("button2").classList.add("disabled_button");
    document.getElementById("button2").style.color = "#777777";
  }

  if (others > 2) {
    document.getElementById("button3").disabled = false;
    document.getElementById("button3").classList.add("main_button");
    document.getElementById("button3").style.color = "#AD8F00";
  } else {
    document.getElementById("button3").disabled = true;
    document.getElementById("button3").classList.add("disabled_button");
    document.getElementById("button3").style.color = "#777777";
  }

  if (others > 3) {
    document.getElementById("button4").disabled = false;
    document.getElementById("button4").classList.add("main_button");
    document.getElementById("button4").style.color = "#AD8F00";
  } else {
    document.getElementById("button4").disabled = true;
    document.getElementById("button4").classList.add("disabled_button");
    document.getElementById("button4").style.color = "#777777";
  }

  if (others > 4) {
    document.getElementById("button5").disabled = false;
    document.getElementById("button5").classList.add("main_button");
    document.getElementById("button5").style.color = "#AD8F00";
  } else {
    document.getElementById("button5").disabled = true;
    document.getElementById("button5").classList.add("disabled_button");
    document.getElementById("button5").style.color = "#777777";
  }

  select0();
}

/**
 * 
 */
function select0() {
  var s1;
  var col;
  var rr0 = (xr0 + xr1 + xr2 + xr3 + xr4 + xr5);
  var rr1 = xr0 * fact;
  if (receivesend == "R") {
    s1 = "<small>Paid " + format(rr0) + " " + currencyChar + ", Self: " + format(xr0) + " " + currencyChar + " ("
      + format(rr1) + " " + currencyChar + ")</small>";
    col = "#cccccc";
  } else {
    s1 = "<small>Received " + format(rr0) + " " + currencyChar + ", Mine: " + format(xr0) + " " + currencyChar + " ("
      + format(rr1) + " " + currencyChar + ")</small>";
    col = "#ff0000";
  }
  var fname = hash0 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash0;
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = "#ff0000";
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  } else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname0 + " | IDCard" + previd0;
  }

  document.getElementById("button0").style.color = "#F7E273";
  if (others > 0) { document.getElementById("button1").style.color = "#AD8F00"; }
  else { document.getElementById("button1").style.color = "#777777"; }
  if (others > 1) { document.getElementById("button2").style.color = "#AD8F00"; }
  else { document.getElementById("button2").style.color = "#777777"; }
  if (others > 2) { document.getElementById("button3").style.color = "#AD8F00"; }
  else { document.getElementById("button3").style.color = "#777777"; }
  if (others > 3) { document.getElementById("button4").style.color = "#AD8F00"; }
  else { document.getElementById("button4").style.color = "#777777"; }
  if (others > 4) { document.getElementById("button5").style.color = "#AD8F00"; }
  else { document.getElementById("button5").style.color = "#777777"; }
}

/**
 * Function is initiated from html
 */
function select1() { // eslint-disable-line no-unused-vars
  var rr1 = xr1 * fact;
  var col;
  var s1 = "<small>" + format(xr1) + " " + currencyChar + " (" + format(rr1) + " " + currencyChar + ")</small>";
  if (receivesend == "R") {
    col = "#cccccc";
  } else {
    col = "#ff0000";
  }
  var fname = hash1 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash1;
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = "#ff0000";
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  } else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname1 + " | IDCard" + previd1;
  }
  document.getElementById("button0").style.color = "#AD8F00";
  document.getElementById("button1").style.color = "#F7E273";
  if (others > 1) { document.getElementById("button2").style.color = "#AD8F00"; }
  else { document.getElementById("button2").style.color = "#777777"; }
  if (others > 2) { document.getElementById("button3").style.color = "#AD8F00"; }
  else { document.getElementById("button3").style.color = "#777777"; }
  if (others > 3) { document.getElementById("button4").style.color = "#AD8F00"; }
  else { document.getElementById("button4").style.color = "#777777"; }
  if (others > 4) { document.getElementById("button5").style.color = "#AD8F00"; }
  else { document.getElementById("button5").style.color = "#777777"; }
}


/**
 * Function is initiated from html
 */
function select2() {  // eslint-disable-line no-unused-vars
  var rr1 = xr2 * fact;
  var col;
  var s1 = "<small>" + format(xr2) + " " + currencyChar + " (" + format(rr1) + " " + currencyChar + ")</small>";
  if (receivesend == "R") {
    col = "#cccccc";
  } else {
    col = "#ff0000";
  }
  var fname = hash2 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash2;
  if (sha != sha2) {
    //hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  } else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = "#cccccc";
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname2 + " | IDCard" + previd2;
  }
  document.getElementById("button0").style.color = "#AD8F00";
  document.getElementById("button1").style.color = "#AD8F00";
  document.getElementById("button2").style.color = "#F7E273";
  if (others > 2) {
    document.getElementById("button3").style.color = "#AD8F00"; 
  } else { 
    document.getElementById("button3").style.color = "#777777";
  }
  if (others > 3) {
    document.getElementById("button4").style.color = "#AD8F00";
  } else {
    document.getElementById("button4").style.color = "#777777";
  }
  if (others > 4) {
    document.getElementById("button5").style.color = "#AD8F00";
  } else {
    document.getElementById("button5").style.color = "#777777";
  }
}


/**
 * Function is initiated from html
 */
function select3() { // eslint-disable-line no-unused-vars
  var rr1 = xr3 * fact;
  var col;
  var s1 = "<small>" + format(xr3) + " " + currencyChar + " (" + format(rr1) + " " + currencyChar + ")</small>";
  if (receivesend == "R") {
    col = "#cccccc";
  } else {
    col = "#ff0000";
  }
  var fname = hash3 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash3;
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  } else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname3 + " | IDCard" + previd3;
  }
  document.getElementById("button0").style.color = "#AD8F00";
  document.getElementById("button1").style.color = "#AD8F00";
  document.getElementById("button2").style.color = "#AD8F00";
  document.getElementById("button3").style.color = "#F7E273";
  if (others > 3) { document.getElementById("button4").style.color = "#AD8F00"; }
  else { document.getElementById("button4").style.color = "#777777"; }
  if (others > 4) { document.getElementById("button5").style.color = "#AD8F00"; }
  else { document.getElementById("button5").style.color = "#777777"; }
}


/**
 * Function is initiated from html
 */
function select4() { // eslint-disable-line no-unused-vars
  var rr1 = xr4 * fact;
  var col;
  var s1 = "<small>" + format(xr4) + " " + currencyChar + " (" + format(rr1) + " " + currencyChar + ")</small>";
  if (receivesend == "R") {
    col = "#cccccc";
  } else {
    col = "#ff0000";
  }
  var fname = hash4 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash4;
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  }
  else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = col;
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname4 + " | IDCard" + previd4;
  }
  document.getElementById("button0").style.color = "#AD8F00";
  document.getElementById("button1").style.color = "#AD8F00";
  document.getElementById("button2").style.color = "#AD8F00";
  document.getElementById("button3").style.color = "#AD8F00";
  document.getElementById("button4").style.color = "#F7E273";
  if (others > 4) { document.getElementById("button5").style.color = "#AD8F00"; }
  else { document.getElementById("button5").style.color = "#777777"; }
}


/**
 * Function is initiated from html 
 */
function select5() { // eslint-disable-line no-unused-vars
  var rr1 = xr5 * fact;
  var s1 = "<small>" + format(xr5) + " " + currencyChar + " (" + format(rr1) + " " + currencyChar + ")</small>";
  var fname = hash5 + ".png";
  var IDCardBase64 = window.localStorage.getItem(fname);
  var sha = SHA256(IDCardBase64);
  var sha2 = hash5;
  if (sha != sha2) {
    // hash is not okay
    document.getElementById("description").innerHTML = "Hash IDCard is wrong!";
    document.getElementById("description").style.color = "#ff0000";
    document.getElementById("description").style.display = "none";
    document.getElementById("checkName").style.display = "none";
    document.getElementById("IDCard").src = "img/PhotoClear.png";
  }
  else {
    document.getElementById("description").innerHTML = s1;
    document.getElementById("description").style.color = "#cccccc";
    document.getElementById("description").style.display = "block";
    document.getElementById("checkName").style.display = "block";
    document.getElementById("IDCard").src = IDCardBase64;
    document.getElementById("checkName").innerHTML = ssname5 + " | IDCard" + previd5;
  }
  document.getElementById("button0").style.color = "#AD8F00";
  document.getElementById("button1").style.color = "#AD8F00";
  document.getElementById("button2").style.color = "#AD8F00";
  document.getElementById("button3").style.color = "#AD8F00";
  document.getElementById("button4").style.color = "#AD8F00";
  document.getElementById("button5").style.color = "#F7E273";
}


/**
 * Function is initiated from html
 */
function returnTrans() { // eslint-disable-line no-unused-vars
  window.open("301_transactions.html", "_self");
}
