//used
var currentHash = "";
var hcd = 13; //day
var hcm = 2; //month
var hcy = 2035; //year
var hch = 11; //hour
var hc = 0;   //hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time). The counter does not need to look at daylight saving time or time zones
var currencyChar = "";
var bdd = 0; //birthday day
var bmm = 0; //birthday month
var byy = 0; //birthday year
var bhh = 0; //birthday hour
var timer = ""; //string to represent the progress in time in the DATASET


const format = (num, decimals) => num.toLocaleString("en-US", {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});



function getBirthdayHour()
{
  ///////////////////////////////////////////////////////  
  // based on the vars bdd,bmm and byy                 //
  // the hour count in bhh is calculated from 1900     // 
  // and then altered to 1-1-2022                      //
  // the exact hour of birth is not important and =0   //
  ///////////////////////////////////////////////////////
    
  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((byy - 1897) / 4); 
  if (((Math.trunc((byy - 1900) / 4)) == ((byy - 1900) / 4)) && (bmm > 2))  {t1++;}
  t1 = bdd + t1 + (365*(byy-1900));  //23755
  if (bmm > 1) {t1 = t1+31;}
  if (bmm > 2) {t1 = t1+28;}
  if (bmm > 3) {t1 = t1+31;}
  if (bmm > 4) {t1 = t1+30;}
  if (bmm > 5) {t1 = t1+31;}
  if (bmm > 6) {t1 = t1+30;}
  if (bmm > 7) {t1 = t1+31;}
  if (bmm > 8) {t1 = t1+31;}
  if (bmm > 9) {t1 = t1+30;}
  if (bmm > 10) {t1 = t1+31;}
  if (bmm > 11) {t1 = t1+30;}
  bhh = (24*(t1-1)) + 1; //01-01-1900 =1  01-01-2022=1069464   
  bhh = bhh - 1069464;   //43830+365+366 days or 1069464 hours between 01-01-1900 and 01-01-2022
}


function setDate()
{
  ///////////////////////////////////////////////////////
  // based on hc, the vars hcd,hcm,hcy and hch are set //
  // the date in hc_date is also set                   //
  ///////////////////////////////////////////////////////
  var t1 = 0;
  var t5 = 0;
  hcd = Math.trunc(hc/24);                          //hcd = daynumber since 01-01-2022 where 01-01-2022 is day 0 here
  hch = hc - (24*hcd);                              //hch = number of hours on day t1
  t1 = Math.trunc(hcd / 1461);                      //t1 = number of clusters of 4 years (=1461 days) that are past
  hcd = 1 + (hcd - (1461 * t1));                    //hcd = remaining number of days in remaining cluster, add one day to make 01-01-2022 day 1
  t5 = 0;                                           //t5 = number of years that has past in the remaining cluster 
  if (hcd > 365) {t5++;hcd=hcd-365;}               //t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 365) && (t5 == 1)) {t5++;hcd=hcd-365;}//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  if ((hcd > 366)  && (t5 == 2)) {t5++;hcd=hcd-366;}//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
  hcy = 2022 + t5 + (4*t1);
  //hcd is now the day number in year hcy
  hcm = 1;
  if (hcd > 31) {hcm=2;hcd=hcd-31;}
  if (t5 == 2)
  {
    if ((hcd > 29) && (hcm>1)) {hcm=3;hcd=hcd-29;}
  } else
  {    
    if ((hcd > 28) && (hcm>1)) {hcm=3;hcd=hcd-28;}
  }
  if ((hcd > 31) && (hcm>2)) {hcm=4;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>3)) {hcm=5;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>4)) {hcm=6;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>5)) {hcm=7;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>6)) {hcm=8;hcd=hcd-31;}
  if ((hcd > 31) && (hcm>7)) {hcm=9;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>8)) {hcm=10;hcd=hcd-30;}
  if ((hcd > 31) && (hcm>9)) {hcm=11;hcd=hcd-31;}
  if ((hcd > 30) && (hcm>10)) {hcm=12;hcd=hcd-30;}
  var mnd = "Jan";
  switch(hcm) 
  {
  case 1:  {var mnd = "Jan";} break;        
  case 2:  {var mnd = "Feb";} break;        
  case 3:  {var mnd = "Mar";} break;        
  case 4:  {var mnd = "Apr";} break;        
  case 5:  {var mnd = "May";} break;        
  case 6:  {var mnd = "Jun";} break;        
  case 7:  {var mnd = "Jul";} break;        
  case 8:  {var mnd = "Aug";} break;        
  case 9:  {var mnd = "Sep";} break;        
  case 10:  {var mnd = "Oct";} break;        
  case 11:  {var mnd = "Nov";} break;        
  case 12:  {var mnd = "Dec";} break;        
  }
  day = (hcd < 10 ? "0" : "") + hcd;
  hour = (hch < 10 ? "0" : "") + hch;
  timer = day + mnd + hcy+"_"+hour+"u";
}


function getDate()
{
  ///////////////////////////////////////////////////////
  // based on the vars hcd,hcm,hcy and hch             //
  // the hour count in hc is calculated                //
  ///////////////////////////////////////////////////////
    
  ///////////////////////////////////////////
  /// first count all extra jump-year days///
  ///////////////////////////////////////////
  var t1 = 0;
  t1 = Math.trunc((hcy - 2021) / 4);    //in the year 2025 we need to start with one jump day
  if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2))  {t1++;} //if feb has pars in a jump year we also need to add one jump day
  t1 = hcd + t1 + (365*(hcy-2022)); //then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
  if (hcm > 1) {t1 = t1+31;} //then we add the days of all past months in this year vvv
  if (hcm > 2) {t1 = t1+28;}
  if (hcm > 3) {t1 = t1+31;}
  if (hcm > 4) {t1 = t1+30;}
  if (hcm > 5) {t1 = t1+31;}
  if (hcm > 6) {t1 = t1+30;}
  if (hcm > 7) {t1 = t1+31;}
  if (hcm > 8) {t1 = t1+31;}
  if (hcm > 9) {t1 = t1+30;}
  if (hcm > 10) {t1 = t1+31;}
  if (hcm > 11) {t1 = t1+30;}
  hc = (24*(t1-1)) + hch; //because the day has not passed we reduce one day, multiply with 24 and add the current hour.
  //var ss = String(hc);
  //document.getElementById("ta1").innerHTML ="Hour Count: " + ss;
}


function OnStart()
{
  var numtransactions = 0;
  //////////////////////////////
  ///  Arrange the ᕫ symbol  ///
  //////////////////////////////
  currencyChar = "";
  var s1 = "";
  s1 = window.localStorage.getItem("myCurrencyChar");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    currencyChar = "ᕫ";
    window.localStorage.setItem("myCurrencyChar","ᕫ");
    alert("If the currency character 'ᕫ' doesn't show properly, go to 'Settings' to choose an alternative.");
  }
  else
  {
    currencyChar = s1;
  }
  //////////////////////////////
  ///  get the birthdayhour  ///
  //////////////////////////////
  var birthday = "";
  var s1 = window.localStorage.getItem("myBirthday");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    birthday = "";
    alert("You don't have set your birthday. Go to 'IDCard' to set your birthday.");
  }
  else
  {
    birthday = s1;
    bdd = Number(birthday.substring(0, 2));
    bmm = Number(birthday.substring(3, 5));
    byy = Number(birthday.substring(6));
    getBirthdayHour(); //this sets the bhh
  }
  //////////////////////////////
  ///  read your blockchain  ///
  //////////////////////////////
  var myFiles = "";  //to get the filename of your blockchain
  var read = ""; //to read your blockchain
  var s1 = window.localStorage.getItem("myFiles");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    read = "";
    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
  }
  else
  {
    var myBlockchain = s1 + ".txt";
    var s1 = window.localStorage.getItem(myBlockchain);
    if (s1 == null) {s1 = "";}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
      read = "";
      alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.");
    }
    else
    {
      read = s1;
    }
  }
  ///////////////////////////
  ///  Set the date text  ///
  ///////////////////////////
  var today = new Date();
  hcd = Number(String(today.getDate()));
  hcm = Number(String(today.getMonth() + 1)); //January is 0!
  hcy = Number(today.getFullYear());
  hch = Number(String(today.getHours()));
  var sdate = "Your coins per "+String(hcd);
  if (hcm ==  1) {sdate = sdate + " Jan ";} 
  if (hcm ==  2) {sdate = sdate + " Feb ";} 
  if (hcm ==  3) {sdate = sdate + " Mar ";} 
  if (hcm ==  4) {sdate = sdate + " Apr ";} 
  if (hcm ==  5) {sdate = sdate + " May ";} 
  if (hcm ==  6) {sdate = sdate + " Jun ";} 
  if (hcm ==  7) {sdate = sdate + " Jul ";} 
  if (hcm ==  8) {sdate = sdate + " Aug ";} 
  if (hcm ==  9) {sdate = sdate + " Sep ";} 
  if (hcm == 10) {sdate = sdate + " Okt ";} 
  if (hcm == 11) {sdate = sdate + " Nov ";} 
  if (hcm == 12) {sdate = sdate + " Dec ";}
  s1 = String(hch);
  if (hch < 10) {s1 = "0"+s1;}
  sdate = sdate + String(hcy) + ", " + s1 + ":00h";
  document.getElementById("timetitle").innerHTML = sdate;
  //////////////////////////////////////////////
  ///  See if you already have an IDCard     ///
  ///  without it you can't do transactions  ///
  //////////////////////////////////////////////
  currentHash = "";
  s1 = window.localStorage.getItem("myCurrentHash");
  if (s1 == null) {s1 = "";}
  if ((s1 != s1) || (s1 == "") || (birthday == "") || (read == "")) //catch a NaN issue
  {
    currentHash = "";
    document.getElementById("fullcoins").innerHTML = "0.00 "+currencyChar;
    document.getElementById("mycoins").innerHTML = "0.00 "+currencyChar;
    document.getElementById("myspentcoins").innerHTML = "0.00 "+currencyChar;
    document.getElementById("othercoins").innerHTML = "0.00 "+currencyChar;
    document.getElementById("otherspentcoins").innerHTML = "0.00 "+currencyChar;
    document.getElementById("availablecoins").innerHTML = "0.00 "+currencyChar;
    alert("Without an IDCard you won't receive 1CoinH coins!");
  }
  else
  {
    currentHash = s1;
    //////////////////////////////////////////////////
    ///  count the coins you have self originated  ///
    //////////////////////////////////////////////////
    getDate();  //defines hc which is the current hour we live in
    var hcend = hc;
    var ss = hcd+"-"+hcm+"-"+hcy;
    var originated = 0;
    var coins = 0;
    for(hc = 1; hc < hcend; hc++)
    {
      setDate();
      if (bhh < hc) //person must be born
      {
        originated=(originated*0.999941728183863)+1;
        coins = coins + 1;
      }
    }
    var spentcoins = 0;
    var coinsothers = 0;
    var spentothers = 0;
    /////////////////////////////////////////////////////////////////////////////////////////
    ///  now we need to find the last full transaction line (between the "<" characters)  ///
    ///  in the new blockchain of the sender and check all the items in this line         ///
    /////////////////////////////////////////////////////////////////////////////////////////
    var t1 = read.indexOf("<") + 1;
    read = read.substring(t1);
    var t2 = (read.match(/</g) || []).length;
    var readx = "";
    var s0 = "";
    var s2 = "";
    var s3 = "";
    var s4 = "";
    var sss = "";
    var ss1 = "";
    var ss2 = "";
    var ss3 = "";
    var ss4 = "";
    var ss5 = "";
    var ssr = "";
    var xr0 = 0;
    var xr1 = 0;
    var xr2 = 0;
    var xr3 = 0;
    var xr4 = 0;
    var xr5 = 0;
    var tx = 0;
    var t0 = 0;
    var delta = 0;
    var fact = 0;
    var others2 = 0;
    var endhash = "";
    //app.ShowPopup(t2)
    document.getElementById("transactions").classList.remove(document.getElementById("transactions").classList.item(0));
    if (t2 > 0)  //there is at least 1 transaction
    {
      document.getElementById("transactions").disabled = false;
      document.getElementById("transactions").classList.add("main_button");
      for (var i = 0; i < t2; i++)
      {
        t1 = read.indexOf("<");
        readx = read.substring(0,t1+1); //including "<"
        //////////////////////////////////////////
        ///  get remaining part of blockchain  ///
        ///  to be used in the next run        ///
        //////////////////////////////////////////
        if (read.length>(t1+3)) //string is not finished
        {
          read = read.substring(t1+1); //to be used in next for loop cycle
        }
        //////////////////////////////////////////
        readx = readx.trim();
        s0 = readx.substring(0,1);     //can be "R" be "S"
        s1 = readx.substring(1,65);  //must be person that receives the coin
        sss = readx.substring(66); 
        t0 = sss.indexOf("|");
        s2 = sss.substring(0,t0);          //timestamp
        sss = sss.substring(t0+1);      
        t0 = sss.indexOf("|");
        s3 = sss.substring(0,t0);          //securityhash
        sss = sss.substring(t0+1);      
        t0 = sss.indexOf("|");
        ssr = sss.substring(0,t0);          //rr0
        xr0 = Number(ssr);
        others2 = 0;
        sss = sss.substring(t0+1);      
        t0 = sss.indexOf("|");
        endhash = "";
        ssr = "";
        if (t0 > -1)
        {
          others2 = 1;
          ss1 = sss.substring(0,t0); //hash1
          sss = sss.substring(t0+1);      
          t0 = sss.indexOf("|");
          ssr = sss.substring(0,t0); //rr1
          xr1 = Number(ssr);
          sss = sss.substring(t0+1); 
          t0 = sss.indexOf("|");
          if (t0 > -1)
          {
            others2 = 2;
            ss2 = sss.substring(0,t0); //hash2
            sss = sss.substring(t0+1);      
            t0 = sss.indexOf("|");
            ssr = sss.substring(0,t0); //rr2
            xr2 = Number(ssr);
            sss = sss.substring(t0+1); 
            t0 = sss.indexOf("|");
            if (t0 > -1)
            {
              others2 = 3;
              var ss3 = sss.substring(0,t0); //hash3
              sss = sss.substring(t0+1);      
              t0 = sss.indexOf("|");
              ssr = sss.substring(0,t0); //rr3
              xr3 = Number(ssr);
              sss = sss.substring(t0+1); 
              t0 = sss.indexOf("|");
              if (t0 > -1)
              {
                others2 = 4;
                var ss4 = sss.substring(0,t0); //hash4
                sss = sss.substring(t0+1);      
                t0 = sss.indexOf("|");
                ssr = sss.substring(0,t0); //rr4
                xr4 = Number(ssr);
                sss = sss.substring(t0+1); 
                t0 = sss.indexOf("|");
                if (t0 > -1)
                {
                  others2 = 5;
                  var ss5 = sss.substring(0,t0); //hash5
                  sss = sss.substring(t0+1);      
                  t0 = sss.indexOf("|");
                  ssr = sss.substring(0,t0); //rr5
                  xr5 = Number(ssr);
                  sss = sss.substring(t0+1); 
                  t0 = sss.indexOf("|");
                }
              }
            }
          }
        }
        t0 = sss.indexOf("<");
        endhash = sss.substring(t0-64,t0);          //must be endhash
        //////////////////////
        ///  Process data  ///
        //////////////////////
        // var spentcoins = 0
        // var coinsothers = 0
        // var spentothers = 0
        t0 = s2.indexOf(":");
        tx = Number(s2.substring(0,t0)); //hour of payment
        delta = (hcend - tx);
        fact = Math.pow(0.6, (delta/8766));
        if (s0 == "R")
        {
          // received coins
          if (currentHash == ss1) {originated = originated + (fact * xr1); xr1 = 0;} // I receive my own coins back
          if (currentHash == ss2) {originated = originated + (fact * xr2); xr2 = 0;} // I receive my own coins back
          if (currentHash == ss3) {originated = originated + (fact * xr3); xr3 = 0;} // I receive my own coins back
          if (currentHash == ss4) {originated = originated + (fact * xr4); xr4 = 0;} // I receive my own coins back
          if (currentHash == ss5) {originated = originated + (fact * xr5); xr5 = 0;} // I receive my own coins back
          coinsothers = coinsothers + (fact * (xr0+xr1+xr2+xr3+xr4+xr5));
        }
        else
        {
          // spent coins
          spentcoins = spentcoins + (fact * (xr0));
          spentothers = spentothers + (fact * (xr1+xr2+xr3+xr4+xr5));
        }
        //////////////////////
        ///  end for loop  ///
        //////////////////////
      }
    }
    else
    { //there are no transactions 
      document.getElementById("transactions").disabled = true;
      document.getElementById("transactions").classList.add("disabled_button");
    }

    ///////////////////////////////////////
    ///  Past value self created coins  ///
    ///////////////////////////////////////
    ss = format(Math.round(coins*100)/100);
    document.getElementById("fullcoins").innerHTML = ss + " "+currencyChar;
    //////////////////////////////////////////
    ///  Present value self created coins  ///
    //////////////////////////////////////////
    ss = format(Math.round(originated*100)/100);
    document.getElementById("mycoins").innerHTML = ss + " "+currencyChar;
    ////////////////////////////////////////////////
    ///  Present value spent self created coins  ///
    ////////////////////////////////////////////////
    ss = format(Math.round(spentcoins*100)/100);
    document.getElementById("myspentcoins").innerHTML = ss + " "+currencyChar;
    /////////////////////////////////////////
    ///  Present value coins from others  ///
    /////////////////////////////////////////
    ss = format(Math.round(coinsothers*100)/100);
    document.getElementById("othercoins").innerHTML = ss + " "+currencyChar;
    ///////////////////////////////////////////////
    ///  Present value spent coins from others  ///
    ///////////////////////////////////////////////
    ss = format(Math.round(spentothers*100)/100);
    document.getElementById("otherspentcoins").innerHTML = ss + " "+currencyChar;
    ///////////////////////////////////////
    ///  Present value available coins  ///
    ///////////////////////////////////////
    var availablecoins = (originated - spentcoins) + (coinsothers - spentothers); 
    ss = format(Math.round(availablecoins*100)/100);
    document.getElementById("availablecoins").innerHTML = ss + " "+currencyChar;
    ///////////////////////////////////////
  }
}


function transactions()
{
  window.localStorage.setItem("transaction","-1");
  window.open("301_transactions.html","_self");
}

